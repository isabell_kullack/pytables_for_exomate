.. . documentation master file, created by
   sphinx-quickstart on Wed Oct 28 14:38:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyTables for Exomate's documentation!
================================================

Contents:

.. toctree::
   :maxdepth: 4

   functioninterface
   functioninterface_calls
   functioninterface_annotations
   functioninterface_samples
   functioninterface_variants
   indexcreator
   interface
   indexHandler
   profiling
   queries
   view


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

