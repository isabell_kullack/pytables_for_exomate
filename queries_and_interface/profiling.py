import cProfile, pstats, io, time
start_time = 0.0

def enable_profiling():
    """enables total profiling"""
    profiling_object = cProfile.Profile()
    profiling_object.enable()
    global start_time
    start_time = time.time()
    return profiling_object

def disable_profiling(profiling_object):
    """disables and exports total profiling"""
    profiling_object.disable()
    s = io.StringIO()
    global start_time
    processing_time = time.time() - start_time
    print("Processing Time: {0}".format(processing_time))
    sortby = 'ncalls'#'time' 
    with open('Profiling/profiling_output.txt', 'w') as f:
        pstats.Stats(profiling_object, stream=f).strip_dirs().sort_stats(sortby).print_stats()


def profile_method(func):
    """profiling decorator class for single method profiling export"""
    def profiled_func(*args, **kwargs):
        profile = cProfile.Profile()
        profile.enable()
        global start_time
        start_time = time.time()
        result = func(*args, **kwargs)
        processing_time = time.time() - start_time
        print("Processing Time: {0}".format(processing_time))
        profile.disable()
        s = io.StringIO()
        sortby = 'ncalls'#'time'
        function_name = func.__name__
        with open('Profiling/profiling_' + function_name + '.txt', 'w') as f:
            pstats.Stats(profile, stream=f).strip_dirs().sort_stats(sortby).print_stats()
        return result
    return profiled_func


def get_time(func):
    """decorator time measuring function"""
    def timed(*args, **kw):
        start_time = time.time()
        result = func(*args, **kw)
        end_time = time.time()
        operation_time = end_time - start_time
        print('%r (%r, %r) %2.2f sec' % (func.__name__, args, kw, operation_time))
        return result
    return timed
