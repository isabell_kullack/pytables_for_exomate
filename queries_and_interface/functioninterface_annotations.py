from tables import *
from filehandler import *
from profiling import *
from datastructures import *
import time
import sys
import argparse
from interface import get_prepare


__author__ = "Isabell Kullack"

COLUMN_NAMES = dict({ 0: 'annotation_id', 1: 'variant_id', 2: 'variant_table_id', 3: 'transcript_id', 4: 'allele', 5: 'annotation', 6: 'annotation2', 7: 'annotation3', 8: 'annotation4', 9: 'annotation5', 10: 'annotation6', 11: 'annotation7', 12: 'putative_impact', 13: 'gene_name', 14: 'gene_id', 15: 'feature_type', 16: 'feature_id', 17: 'transcript_biotype', 18: 'rank', 19: 'total', 20: 'hgvsc', 21: 'hgvsp', 22: 'cdna_position', 23: 'cdna_len', 24: 'cds_position', 25: 'cds_len', 26: 'protein_position', 27: 'protein_len', 28: 'distance_to_feature', 29: 'info'
})


#@profile_method
def get_annotation_from_params_dict(annotations_table, searched_annotation_params, is_search_in_kernel = True, output_range = None, outsingle = None, searched_annotation_compare = None):
    """ funtional interface of annotation table which creates querys of dictionary searched_annotation_params and creates outsingle for each given column by number or as range of columns by output_range """

    
    passvalues_annotations = []
    
    if searched_annotation_compare:
        is_search_in_kernel = True
    
    if is_search_in_kernel:
        query = """("""
        for key, value in searched_annotation_params.items():
            if value is None:
                continue
            if isinstance(value, str):
                value = value.encode()
            if isinstance(value, list):
                value = value[0]
            #print(key, value)
            if key in {'annotation', 'annotation2', 'annotation3', 'annotation4', 'annotation5', 'annotation6', 'annotation7'}:
                query += """((annotation == {0}) | (annotation2 == {1}) | (annotation3 == {2}) | (annotation4 == {3}) | (annotation5 == {4}) | (annotation6 == {5}) | (annotation7 == {6})) & """.format(value, value, value, value, value, value, value)
            if searched_annotation_compare is not None:
                compare = "=="
                try:
                    if searched_annotation_compare[key]:
                        compare = searched_annotation_compare[key]
                except KeyError:
                    pass
                query += """({0} {1} {2}) & """.format(key, compare, value) 
            else:
                query += """({0} == {1}) & """.format(key, value) 
            
        query += """ True )"""
        print(query)
        annotations_table_row = annotations_table.row
        
        if outsingle:
            passvalues_annotations += [[annotations_table_row[COLUMN_NAMES[single_nr]] for single_nr in outsingle] for annotations_table_row in annotations_table.where(query)]
        elif output_range:
            # edit min column count if change!
            passvalues_annotations += [(annotations_table_row[max(0,output_range[0]):min(output_range[1],30)]) for annotations_table_row in annotations_table.where(query)]
        else:
            passvalues_annotations += [(annotations_table_row[:]) for annotations_table_row in annotations_table.where(query)]
    else:
        annotations_table_row = annotations_table.row
        """for annotations_table_row in annotations_table.iterrows():
            for x in searched_annotation_params:
                if (searched_annotation_params[x] is not None) and isinstance(searched_annotation_params[x][0], int): 
                    print(annotations_table_row[x])
                    print(searched_annotation_params[x][0])"""
        if outsingle:
            passvalues_annotations += [[annotations_table_row[COLUMN_NAMES[single_nr]] for single_nr in outsingle] for annotations_table_row in annotations_table.iterrows() 
                                if all( 
                                        ((searched_annotation_params[x] is None)  or 
                                        (isinstance(searched_annotation_params[x][0], int) and (annotations_table_row[x] == searched_annotation_params[x][0])) or
                                        (isinstance(searched_annotation_params[x], float) and annotations_table_row[x] == searched_annotation_params[x]) or 
                                        (isinstance(searched_annotation_params[x], str) and annotations_table_row[x] == searched_annotation_params[x].encode()) or
                                        ('annotation' in searched_annotation_params[x] and searched_annotation_params[x] in annotations_table_row[5:11])) # annotation columns
                                         for x in searched_annotation_params )]

        elif output_range:
            # edit min column count if change!
            passvalues_annotations += [(annotations_table_row[max(0,output_range[0]):min(output_range[1],30)]) for annotations_table_row in annotations_table.iterrows() 
                                if all( 
                                        ((searched_annotation_params[x] is None)  or 
                                        (isinstance(searched_annotation_params[x][0], int) and annotations_table_row[x] == searched_annotation_params[x][0]) or
                                        (isinstance(searched_annotation_params[x], float) and annotations_table_row[x] == searched_annotation_params[x]) or 
                                        (isinstance(searched_annotation_params[x], str) and annotations_table_row[x] == searched_annotation_params[x].encode()) or
                                        ('annotation' in searched_annotation_params[x] and searched_annotation_params[x] in annotations_table_row[5:11])) # annotation columns
                                         for x in searched_annotation_params )]

        else:
            passvalues_annotations += [(annotations_table_row[:]) for annotations_table_row in annotations_table.iterrows() 
                                if all( 
                                        ((searched_annotation_params[x] is None)  or 
                                        (isinstance(searched_annotation_params[x][0], int) and annotations_table_row[x] == searched_annotation_params[x][0]) or
                                        (isinstance(searched_annotation_params[x], float) and annotations_table_row[x] == searched_annotation_params[x]) or 
                                        (isinstance(searched_annotation_params[x], str) and annotations_table_row[x] == searched_annotation_params[x].encode()) or
                                        ('annotation' in searched_annotation_params[x] and searched_annotation_params[x] in annotations_table_row[5:11])) # annotation columns
                                         for x in searched_annotation_params )]
    #print('Gefundene annotations: ', passvalues_annotations)
    #print('Anzahl', len(passvalues_annotations))
    return passvalues_annotations



def main(hdf5_file, output_range = None, outsingle = None, is_search_in_kernel = True, **kwargs):
#    try:
    #print(is_search_in_kernel)
    
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    get_annotation_from_params_dict(annotations_table, kwargs, is_search_in_kernel, output_range, outsingle)
    
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-out", "--output", type=int, nargs=2, help="output range of two indizes")
    parser.add_argument("-outs", "--outsingle", type=int, nargs='*', help="output singles")
    
    parser.add_argument("-aid", "--annotationid", type=int, nargs=1, help="annotation id searched")
    parser.add_argument("-vid", "--variantid", type=int, nargs=1, help="variant id searched")
    parser.add_argument("-vtid", "--varianttableid", type=int, nargs=1, choices=(0,1), help="variant table id searched")
    parser.add_argument("-tid", "--transcriptid", type=int, nargs=1, help="transcript id searched")

    parser.add_argument("-al", "--allele", help="annotation allele searched")
    parser.add_argument("-anno", "--annotation", help="annotation searched")
    parser.add_argument("-anno2", "--annotation2", help="annotation searched")
    parser.add_argument("-anno3", "--annotation3", help="annotation searched")
    parser.add_argument("-anno4", "--annotation4", help="annotation searched")
    parser.add_argument("-anno5", "--annotation5", help="annotation searched")
    parser.add_argument("-anno6", "--annotation6", help="annotation searched")
    parser.add_argument("-anno7", "--annotation7", help="annotation searched")
    parser.add_argument("-puim", "--putativeimpact",  help="annotation putative_impact searched")
    parser.add_argument("-gn", "--genename", help="annotation gene_name searched")
    parser.add_argument("-gi", "--geneid", help="annotation gene_id searched")
    parser.add_argument("-ft", "--featuretype", help="annotation feature_type searched")
    parser.add_argument("-fi", "--featureid", help="annotation feature_id searched")
    parser.add_argument("-tb", "--transcriptbiotype", help="annotation transcript_biotype searched")
    parser.add_argument("-ra", "--rank", type=int, nargs=1, help="annotation rank searched")
    parser.add_argument("-to", "--total", type=int, nargs=1, help="annotation total searched")
    parser.add_argument("-hc", "--hgvsc", help="annotation hgvsc searched")
    parser.add_argument("-hp", "--hgvsp", help="annotation hgvsp searched")
    parser.add_argument("-cp", "--cdnaposition", type=int, nargs=1, help="annotation cdna_position searched")
    parser.add_argument("-cl", "--cdnalen", type=int, nargs=1, help="annotation cdna_len searched")
    parser.add_argument("-cdp", "--cdsposition", type=int, nargs=1, help="annotation cds_position searched")
    parser.add_argument("-cdl", "--cdslen", type=int, nargs=1, help="annotation cds_len searched")
    parser.add_argument("-pp", "--proteinposition", type=int, nargs=1, help="annotation protein_position searched")
    parser.add_argument("-pl", "--proteinlen", type=int, nargs=1, help="annotation protein_len searched")
    parser.add_argument("-dtf", "--distancetofeature", type=int, nargs=1, help="annotation distance_to_feature searched")
    parser.add_argument("-inf", "--info", help="annotation info searched")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    
    
    print(args.issearchnotinkernel)
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.output, args.outsingle, args.issearchnotinkernel, annotation_id = args.annotationid, variant_id = args.variantid, variant_table_id = args.varianttableid, transcript_id = args.transcriptid, allele = args.allele, annotation = args.annotation, annotation2 = args.annotation2, annotation3 = args.annotation3, annotation4 = args.annotation4, annotation5 = args.annotation5, annotation6 = args.annotation6, annotation7 = args.annotation7, putative_impact = args.putativeimpact, gene_name = args.genename, gene_id = args.geneid, feature_type = args.featuretype, feature_id = args.featureid, transcript_biotype = args.transcriptbiotype, rank = args.rank, total = args.total, hgvsc = args.hgvsc, hgvsp = args.hgvsp, cdna_position = args.cdnaposition, cdna_len = args.cdnalen, cds_position = args.cdsposition, cds_len = args.cdslen, protein_position = args.proteinposition, protein_len = args.proteinlen, distance_to_feature = args.distancetofeature, info = args.info ))    
    if(args.enableprofiling):
        disable_profiling(profiling_object)

