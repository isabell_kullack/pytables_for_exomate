from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np
from testabfrage3 import get_variants_clinical_from_patient
#from testabfrage6_k1 import get_join_variants_calls_samples_entries_by_variant_chrom_and_pos
from testabfrage6_k2 import get_join_variants_calls_samples_entries_by_variant_chrom_and_pos

#from testabfrage7_k1 import get_annotations_from_samples_by_calls_qual__in_kernel, get_annotations_from_samples_by_calls_qual__iterrows, get_annotations_from_samples_by_calls_qual__in_kernel__to_array
#from testabfrage7_dict_k2 import get_annotations_from_samples_by_calls_qual__opt
#from testabfrage7_k3_k4 import get_annotations_from_samples_by_calls_qual__all

from testabfrage7_radius_search_params_vs2_k5 import get_annotations_from_samples_by_calls_qual__radius_search_with_params
from testabfrage7_radius_search_opt_k6 import get_annotations_from_samples_by_calls_qual__radius_search_opt
from testabfrage7_radius_search_k5 import get_annotations_from_samples_by_calls_qual__radius_search


__author__ = "Isabell Kullack"



def main(hdf5_file, test_query_number, is_search_in_kernel = True, first_iteration = False, testcase_nr = 0):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    if test_query_number == 10:
        is_search_in_kernel = True
        
        searched_variant_dict = dict()
        
        searched_variant_dict = dict(
                { 'chrom': '04', 'pos': 146806762, 'ref': 'CAGAGAGAG' 
                }
            )
        #searched_variant_dict = dict({ 'chrom': '04', 'pos': 146806762, 'ref': 'CAGAGAGAG', 'alt':'CAGAG'}            )
                
        #print(searched_variant_dict['chrom'])
        start = time.time()
        get_variant_id_from_params_list(variants_snp_table, variants_nosnp_table, searched_variant_dict, is_search_in_kernel)
        print('Abfragezeit: ', time.time()-start)
    if test_query_number == 1:
                
        searched_ids = [107348,214696,322045,429393,536741,644089,751437,858786,966134,1073480]
        
        # Abfrage mit Sets
        for searched_id in searched_ids:
            variant_set =  set()
            start = time.time()
            variant_set = get_variant_entry_by_id_set(variants_snp_table, searched_id, is_search_in_kernel)
            print('Abfragezeit Sets: ', time.time()-start)
            #print(variant_set)
        
        #Abfrage mit List
        for searched_id in searched_ids:
            variant = []
            start = time.time()
            variant = get_variant_entry_by_id(variants_snp_table, variants_nosnp_table, 'variant_snp_id', searched_id, is_search_in_kernel)
            print('Abfragezeit List: ', time.time()-start)
            #print(variant)
            
        #Abfrage mit NumPy-Array
        for searched_id in searched_ids:
            variant = []
            start = time.time()
            variant = get_entry_by_radius_search(variants_snp_table, [searched_id], 'variant_id')
            print('Abfragezeit NpArray: ', time.time()-start)
            #print(variant)    
        
    if test_query_number == 11:
                
        searched_ids = [107348,214696,322045,429393,536741,644089,751437,858786,966134,1073480]
        time_list = []
        from statistics import median
        
        if first_iteration:
            print('Werte;')
        is_search_in_kernel = False
        
        for i in range(2):
        
            #Abfrage mit List
            for searched_id in searched_ids:
                variant = []
                start = time.time()
                variant = get_variant_entry_by_id(variants_snp_table, variants_nosnp_table, 'variant_snp_id', searched_id, is_search_in_kernel)
                end = time.time()-start
                print(end, ';')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            
            is_search_in_kernel = not is_search_in_kernel

    if test_query_number == 12:
                
        searched_ids = [107348,214696,322045,429393,536741,644089,751437,858786,966134,1073480]
        time_list = []
        from statistics import median
        
        if first_iteration:
            print('Werte;')
        is_search_in_kernel = False
        
        for i in range(2):
        
            #Abfrage mit List
            for searched_id in searched_ids:
                variant_set =  set()
                start = time.time()
                variant_set = get_variant_entry_by_id_set(variants_snp_table, searched_id, is_search_in_kernel)

                end = time.time()-start
                print(end, ';')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            
            is_search_in_kernel = not is_search_in_kernel
            
    if test_query_number == 13:
                
        searched_ids = [107348,214696,322045,429393,536741,644089,751437,858786,966134,1073480]
        time_list = []
        from statistics import median
        
        if first_iteration:
            print('Werte;')
        is_search_in_kernel = False
        
        for searched_id in searched_ids:
            variant = []
            start = time.time()
            variant = get_entry_by_radius_search(variants_snp_table, [searched_id], 'variant_id')

            end = time.time()-start
            print(end, ';')
            time_list.append(end)
        median_t = median(time_list)
        max_t = max(time_list)
        print(max_t, ';')
        print(median_t, ';')
        

        
    if test_query_number == 2:
        for i in range(10):
            start = time.time()
            max_variant_id = get_maximum_id(variants_snp_table, 'variant_id')
            print('Abfragezeit: ', time.time()-start)
            #print(max_variant_id)
            
        """for i in range(10):
            start = time.time()
            max_nrecarray = variants_snp_table.read_coordinates([range(variants_snp_table._v_attrs.NROWS)], field='variant_id').max()
            print('Abfragezeit nrecarray: ', time.time()-start)
            #print(max_nrecarray)"""
            
    if test_query_number == 21:
        time_list = []
        from statistics import median
        if first_iteration:
            print('Werte;')
        for i in range(10):
            start = time.time()
            max_variant_id = get_maximum_id(variants_snp_table, 'variant_id')
            end = time.time()-start
            print(end, ';')
            time_list.append(end)
        median_t = median(time_list)
        max_t = max(time_list)
        print(max_t, ';')
        print(median_t, ';')
        
                
    if test_query_number == 3:
        samples_searched = ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040']
        qual_low_limit = 50
        
        for sample_searched in samples_searched:
            start = time.time()
            variants_A_without_B, variants_B_without_A, variants_difference, variants_intersection, intersection_calls, difference_calls, passvalues_calls_A, passvalues_calls_B, passvalues_sample_A, passvalues_sample_B, calls_A_without_B, calls_B_without_A = get_variants_clinical_from_patient(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, sample_searched, qual_low_limit, is_search_in_kernel)
            print('Abfragezeit: ', time.time()-start)
        
    if test_query_number == 31:
        samples_searched = ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040']
        qual_low_limit = 50
        time_list = []
        from statistics import median
        if first_iteration:
            print('Werte;')
        for sample_searched in samples_searched:
            start = time.time()
            variants_A_without_B, variants_B_without_A, variants_difference, variants_intersection, intersection_calls, difference_calls, passvalues_calls_A, passvalues_calls_B, passvalues_sample_A, passvalues_sample_B, calls_A_without_B, calls_B_without_A = get_variants_clinical_from_patient(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, sample_searched, qual_low_limit, is_search_in_kernel)
            end = time.time()-start
            print(end, ';')
            time_list.append(end)
        median_t = median(time_list)
        max_t = max(time_list)
        print(max_t, ';')
        print(median_t, ';')
        
    if test_query_number == 41:
        from statistics import median
        if first_iteration:
            print('Werte;')
        #represents content of template variatns_results. multiple variants also possible
        searched_items = dict({0: {'searched_chrom' : '01', 'searched_pos_range' : [4875318,48762422]},
                              1: {'searched_chrom' : '04', 'searched_pos_range' : [9750638,48762422]},
                              2: {'searched_chrom' : '06', 'searched_pos_range' : [14625957,48762422]},
                              3: {'searched_chrom' : '09', 'searched_pos_range' : [19501277,48762422]},
                              4: {'searched_chrom' : '11', 'searched_pos_range' : [24376596,48762422]},
                              5: {'searched_chrom' : '13', 'searched_pos_range' : [29251916,48762422]},
                              6: {'searched_chrom' : '16', 'searched_pos_range' : [34127235,48762422]},
                              7: {'searched_chrom' : '18', 'searched_pos_range' : [39002555,48762422]},
                              8: {'searched_chrom' : '21', 'searched_pos_range' : [43877874,48762422]},
                              9: {'searched_chrom' : 'X', 'searched_pos_range' : [48753194,48762422]}})
        is_search_in_kernel = False
        for j in range(2):
            time_list = []
            for i in range(len(searched_items)):
                variant = []
                start = time.time()
                variant = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_snp_id', is_search_in_kernel, searched_items[i]['searched_chrom'], searched_items[i]['searched_pos_range'])
            
                end = time.time()-start
                print(end, ';')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            is_search_in_kernel = not is_search_in_kernel
            
            
    if test_query_number == 4:
        #represents content of template variatns_results. multiple variants also possible
        searched_items = dict({0: {'searched_chrom' : '01', 'searched_pos_range' : [4875318,48762422]},
                              1: {'searched_chrom' : '04', 'searched_pos_range' : [9750638,48762422]},
                              2: {'searched_chrom' : '06', 'searched_pos_range' : [14625957,48762422]},
                              3: {'searched_chrom' : '09', 'searched_pos_range' : [19501277,48762422]},
                              4: {'searched_chrom' : '11', 'searched_pos_range' : [24376596,48762422]},
                              5: {'searched_chrom' : '13', 'searched_pos_range' : [29251916,48762422]},
                              6: {'searched_chrom' : '16', 'searched_pos_range' : [34127235,48762422]},
                              7: {'searched_chrom' : '18', 'searched_pos_range' : [39002555,48762422]},
                              8: {'searched_chrom' : '21', 'searched_pos_range' : [43877874,48762422]},
                              9: {'searched_chrom' : 'X', 'searched_pos_range' : [48753194,48762422]}})
        
        for i in range(len(searched_items)):
            #print(searched_items[i]['searched_chrom'], searched_items[i]['searched_pos_range'])
            variant = []
            start = time.time()
            variant = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_snp_id', is_search_in_kernel, searched_items[i]['searched_chrom'], searched_items[i]['searched_pos_range'])
            #searched_chrom = 'X'
            #searched_pos_range=[48753194,48762422]
            #variant = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_snp_id', is_search_in_kernel, searched_chrom, searched_pos_range)

            print('Abfragezeit: ', time.time()-start)
            #print(variant)
            #print('Anzahl: ', len(variant))
        
    if test_query_number == 5:
        
        limit = 10
        #limit = None
        #gene_name = 'PQBP1' 
        # 
        #gene_name = 'PDLIM5'
        #is_search_in_kernel = False
        
        searched_genes = ['PQBP1', 'PDLIM5', 'KANK2', 'STK11', 'LDLR', 'ZNF823', 'MIDN', 'WDR83OS', 'TMEM105', 'BAHCC1' ]
        
        for gene_name in searched_genes:
            annotation = []
            start = time.time()
            annotation = get_annotation_entry_by_gene_name_opt_limit2(annotations_table, gene_name, limit, is_search_in_kernel)
            print('Abfragezeit: ', time.time()-start)
            
        for gene_name in searched_genes:
            annotation = []
            start = time.time()
            annotation = get_annotation_entry_by_gene_name_opt_limit(annotations_table, gene_name, limit, is_search_in_kernel)
            print('Abfragezeit: ', time.time()-start)
            #print(annotation)
            #print('Anzahl Annotationen: ', len(annotation))
            
    if test_query_number == 521:
        if first_iteration:
            print('Werte;')
        is_search_in_kernel = False
        from statistics import median
        for i in range(2):
            limit = 10
            time_list = []
            
            searched_genes = ['PQBP1', 'PDLIM5', 'KANK2', 'STK11', 'LDLR', 'ZNF823', 'MIDN', 'WDR83OS', 'TMEM105', 'BAHCC1' ]
        
            for gene_name in searched_genes:
                annotation = []
                start = time.time()
                annotation = get_annotation_entry_by_gene_name_opt_limit2(annotations_table, gene_name, limit, is_search_in_kernel)
                end = time.time()-start
                print(end, ';')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            is_search_in_kernel = not is_search_in_kernel
        
    if test_query_number == 511:
        if first_iteration:
            print('Werte;')
        from statistics import median
        is_search_in_kernel = False
        for i in range(2):
            limit = 10
            time_list = []
            
        
        
            searched_genes = ['PQBP1', 'PDLIM5', 'KANK2', 'STK11', 'LDLR', 'ZNF823', 'MIDN', 'WDR83OS', 'TMEM105', 'BAHCC1' ]
        
            for gene_name in searched_genes:
                annotation = []
                start = time.time()
                annotation = get_annotation_entry_by_gene_name_opt_limit(annotations_table, gene_name, limit, is_search_in_kernel)
                end = time.time()-start
                print(end, ';')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            is_search_in_kernel = not is_search_in_kernel                    
    
    if test_query_number == 6:
    
        #searched_chrom = '01'
        #pos = 142637034
        
        searched_items = dict({0: {'searched_chrom' : '01', 'pos' : 142637034},
                              1: {'searched_chrom' : '04', 'pos' : 185614903},
                              2: {'searched_chrom' : '06', 'pos' : 99881639},
                              3: {'searched_chrom' : '09', 'pos' : 131882540},
                              4: {'searched_chrom' : '11', 'pos' : 5896144},
                              5: {'searched_chrom' : '13', 'pos' : 32606135},
                              6: {'searched_chrom' : '16', 'pos' : 142637034},
                              7: {'searched_chrom' : '18', 'pos' : 11752763},
                              8: {'searched_chrom' : '21', 'pos' : 38417209},
                              9: {'searched_chrom' : 'X', 'pos' : 48923681}})
        
        for i in range(len(searched_items)):
            output_join = None
            start = time.time()
            output_join = get_join_variants_calls_samples_entries_by_variant_chrom_and_pos(calls_table, samples_table, variants_snp_table, variants_nosnp_table, is_search_in_kernel, searched_items[i]['searched_chrom'], searched_items[i]['pos'])
            print('Abfragezeit: ', time.time()-start)
            #print('Anzahl', len(output_join))
            #print(output_join)
    
    if test_query_number == 61:
        if first_iteration:
            print('Werte;')
        from statistics import median
        is_search_in_kernel = False
        for i in range(2):
            limit = 10
            time_list = []

        
            searched_items = dict({0: {'searched_chrom' : '01', 'pos' : 142637034},
                              1: {'searched_chrom' : '04', 'pos' : 185614903},
                              2: {'searched_chrom' : '06', 'pos' : 99881639},
                              3: {'searched_chrom' : '09', 'pos' : 131882540},
                              4: {'searched_chrom' : '11', 'pos' : 5896144},
                              5: {'searched_chrom' : '13', 'pos' : 32606135},
                              6: {'searched_chrom' : '16', 'pos' : 142637034},
                              7: {'searched_chrom' : '18', 'pos' : 11752763},
                              8: {'searched_chrom' : '21', 'pos' : 38417209},
                              9: {'searched_chrom' : 'X', 'pos' : 48923681}})
        
            for i in range(len(searched_items)):
                output_join = None
                start = time.time()
                output_join = get_join_variants_calls_samples_entries_by_variant_chrom_and_pos(calls_table, samples_table, variants_snp_table, variants_nosnp_table, is_search_in_kernel, searched_items[i]['searched_chrom'], searched_items[i]['pos'])
                end = time.time()-start
                print(end, ';')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            is_search_in_kernel = not is_search_in_kernel
            
    
    """
    if test_query_number == 7:
        if is_search_in_kernel:
            start = time.time()
            get_annotations_from_samples_by_calls_qual__in_kernel(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table)
            print('Abfragezeit: ', time.time()-start)
        else:
            start = time.time()
            get_annotations_from_samples_by_calls_qual__iterrows(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table)
            print('Abfragezeit: ', time.time()-start)
    if test_query_number == 8:
        start = time.time()
        get_annotations_from_samples_by_calls_qual__in_kernel__to_array(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel)
        print('Abfragezeit: ', time.time()-start)
    """
    
    if test_query_number == 71:
        if "/" in hdf5_file:
            hdf5_file_path2, rest = hdf5_file.rsplit("/", maxsplit=1)
            hdf5_file2 = hdf5_file_path2 + '/statistic.h5'
        else:
            hdf5_file2 = 'statistic.h5'
        hdf5_file_obj2 = open_in_mode(hdf5_file2, 'r')
    
        statistics_snp_table, statistics_nosnp_table = get_prepare_statistics(hdf5_file_obj2)
        # Originalabfrage: abgerufen 9 Samples in Set
        #['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040']
    
        searched_items = dict({0: {'samples_searched' : ['RB_E_021', 'RB_E_032'], 'qual_low_limit' : None},
                              1: {'samples_searched' : ['RB_E_025', 'RB_E_039'], 'qual_low_limit' : 50},
                              2: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : None},
                              3: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : 50},
                              4: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : None},
                              5: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : 50},
                              6: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : None},
                              7: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : 50},
                              8: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : None},
                              9: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : 50}})
                              
        
        if first_iteration:
            print('Werte;')
        from statistics import median
        is_search_in_kernel = False
        for j in range(2):
            time_list = []
            
            for i in range(len(searched_items)):
            
                start = time.time()
                output = get_annotations_from_samples_by_calls_qual__radius_search(statistics_snp_table, statistics_nosnp_table, calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, searched_items[i]['qual_low_limit'], searched_items[i]['samples_searched'], is_search_in_kernel = is_search_in_kernel)
                end = time.time()-start
                print(end, ';')
                #print(len(output), 'output length')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            #print(len(output), 'output length')
            is_search_in_kernel = not is_search_in_kernel
        
        hdf5_file_obj2.close()
    
    
    if test_query_number == 771:
        if "/" in hdf5_file:
            hdf5_file_path2, rest = hdf5_file.rsplit("/", maxsplit=1)
            hdf5_file2 = hdf5_file_path2 + '/statistic.h5'
        else:
            hdf5_file2 = 'statistic.h5'
        hdf5_file_obj2 = open_in_mode(hdf5_file2, 'r')
        
        is_annotation_required = True
        gene_name_searched = 'PYGL'
        
        statistics_snp_table, statistics_nosnp_table = get_prepare_statistics(hdf5_file_obj2)
        # Originalabfrage: abgerufen 9 Samples in Set
        #['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040']
    
        searched_items = dict({0: {'samples_searched' : ['RB_E_021', 'RB_E_032'], 'qual_low_limit' : None},
                              1: {'samples_searched' : ['RB_E_025', 'RB_E_039'], 'qual_low_limit' : 50},
                              2: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : None},
                              3: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : 50},
                              4: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : None},
                              5: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : 50},
                              6: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : None},
                              7: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : 50},
                              8: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : None},
                              9: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : 50}})
                              
        
        if first_iteration:
            print('Werte;')
        from statistics import median
        #is_search_in_kernel = False
        is_search_in_kernel = True
        #for j in range(2):
        time_list = []
            
        for i in range(len(searched_items)):
            
            start = time.time()
            output = get_annotations_from_samples_by_calls_qual__radius_search_with_params(statistics_snp_table, statistics_nosnp_table, calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, searched_items[i]['qual_low_limit'], searched_items[i]['samples_searched'], is_search_in_kernel = is_search_in_kernel, is_annotation_required=is_annotation_required, gene_name_searched=gene_name_searched)

            end = time.time()-start
            print(end, ';')
            #print(len(output), 'output length')
            time_list.append(end)
        median_t = median(time_list)
        max_t = max(time_list)
        print(max_t, ';')
        print(median_t, ';')
        #print(len(output), 'output length')
        #is_search_in_kernel = not is_search_in_kernel
        
        hdf5_file_obj2.close()

    
        
    if test_query_number == 711:
        if "/" in hdf5_file:
            hdf5_file_path2, rest = hdf5_file.rsplit("/", maxsplit=1)
            hdf5_file2 = hdf5_file_path2 + '/statistic.h5'
        else:
            hdf5_file2 = 'statistic.h5'
        hdf5_file_obj2 = open_in_mode(hdf5_file2, 'r')
    
        statistics_snp_table, statistics_nosnp_table = get_prepare_statistics(hdf5_file_obj2)
        # Originalabfrage: abgerufen 9 Samples in Set
        
    
        searched_items = dict({0: {'samples_searched' : ['RB_E_021', 'RB_E_032'], 'qual_low_limit' : None},
                              1: {'samples_searched' : ['RB_E_025', 'RB_E_039'], 'qual_low_limit' : 50},
                              2: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : None},
                              3: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : 50},
                              4: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : None},
                              5: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : 50},
                              6: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : None},
                              7: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : 50},
                              8: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : None},
                              9: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : 50}})
                              
        
        if first_iteration:
            print('Werte;')
        #from statistics import median
        is_search_in_kernel = False
        for j in range(2):
            
            i = testcase_nr
            start = time.time()
            output = get_annotations_from_samples_by_calls_qual__radius_search(statistics_snp_table, statistics_nosnp_table, calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, searched_items[i]['qual_low_limit'], searched_items[i]['samples_searched'], is_search_in_kernel = is_search_in_kernel)
            end = time.time()-start
            print(end, ';')
            
            #print(len(output), 'output length')
            is_search_in_kernel = not is_search_in_kernel
        
        hdf5_file_obj2.close()

        
    if test_query_number == 72:
        
        if "/" in hdf5_file:
            hdf5_file_path2, rest = hdf5_file.rsplit("/", maxsplit=1)
            hdf5_file2 = hdf5_file_path2 + '/statistic.h5'
        else:
            hdf5_file2 = 'statistic.h5'
        hdf5_file_obj2 = open_in_mode(hdf5_file2, 'r')
        
        statistics_snp_table, statistics_nosnp_table = get_prepare_statistics(hdf5_file_obj2)
        
        searched_items = dict({0: {'samples_searched' : ['RB_E_021', 'RB_E_032'], 'qual_low_limit' : None},
                              1: {'samples_searched' : ['RB_E_025', 'RB_E_039'], 'qual_low_limit' : 50},
                              2: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : None},
                              3: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020'], 'qual_low_limit' : 50},
                              4: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : None},
                              5: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026'], 'qual_low_limit' : 50},
                              6: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : None},
                              7: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033'], 'qual_low_limit' : 50},
                              8: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : None},
                              9: {'samples_searched' : ['RB_E_011', 'RB_E_014', 'RB_E_017', 'RB_E_020', 'RB_E_023', 'RB_E_026', 'RB_E_029', 'RB_E_033', 'RB_E_036', 'RB_E_040'], 'qual_low_limit' : 50}})   

        if first_iteration:
            print('Werte;')
        from statistics import median
        is_search_in_kernel = False
        for j in range(2):
            time_list = []
        
            for i in range(len(searched_items)):
        
                start = time.time()
                output, output2 = get_annotations_from_samples_by_calls_qual__radius_search_opt(statistics_snp_table, statistics_nosnp_table, calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, searched_items[i]['qual_low_limit'], searched_items[i]['samples_searched'], is_search_in_kernel = is_search_in_kernel)
            
                end = time.time()-start
                print(end, ';')
                time_list.append(end)
            median_t = median(time_list)
            max_t = max(time_list)
            print(max_t, ';')
            print(median_t, ';')
            
            #print(len(output), len(output2))
            is_search_in_kernel = not is_search_in_kernel
        
        hdf5_file_obj2.close()
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")
    parser.add_argument("-f", "--firstiteration", action="store_true",
                        help="boolean if search not is in kernel")
    parser.add_argument("-tc", "--testcasenr", type=int, help="testcase number")
    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel, args.firstiteration, args.testcasenr))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

