from tables import *
from filehandler import *
from profiling import *
from datastructures import *
import time
import sys
import argparse
from interface import get_prepare


__author__ = "Isabell Kullack"

COLUMN_NAMES = dict({ 0: 'sample_id', 1: 'variant_id', 2: 'variant_table_id', 3: 'qual', 4: 'is_heterozygous', 5: 'read_depth', 6: 'ref_depth', 7: 'alt_depth', 8: 'strand_bias', 9: 'qual_by_depth', 10: 'mapping_qual', 11: 'haplotype_score', 12: 'mapping_qual_bias', 13: 'read_pos_bias', 14: 'call_id'})

#@profile_method
def get_call_from_params_dict(calls_table, searched_call_params, is_search_in_kernel = True, output_range = None, outsingle = None, searched_compare = None, is_nan_allowed = False):
    """ funtional interface of calls table which creates querys of dictionary searched_call_params and creates outsingle for each given column by number or as range of columns by output_range """

    
    passvalues_calls = []
    
    if searched_compare:
        is_search_in_kernel = True
    
    if is_search_in_kernel:
        query = """("""
        for key, value in searched_call_params.items():
            if value is None:
                continue
            if isinstance(value, str):
                value = value.encode()
            #print(key, value)
            if isinstance(value, list) or isinstance(value, set):
                query = """(("""
                
                for i in value:
                    if isinstance(i, str):
                        i = i.encode()
                    if searched_compare is not None:
                        compare = "=="
                        try:
                            if searched_compare[key]:
                                compare = searched_compare[key]
                        except KeyError:
                            compare = "=="
                        query += """({0} {1} {2}) | """.format(key, compare, i) 
                        if is_nan_allowed:
                            query += """({0} != {0}) | """.format(key)
                    else:
                        query += """({0} == {1}) | """.format(key, i) 
                        if is_nan_allowed:
                            query += """({0} != {0}) | """.format(key)
                query += """ False ) & """
            
            
            else:
                if searched_compare is not None:
                    compare = "=="
                    try:
                        if searched_compare[key]:
                            compare = searched_compare[key]
                    except KeyError:
                        compare = "=="
                    if is_nan_allowed:
                        query += """(({0} {1} {2}) | ({0} != {0})) & """.format(key, compare, value)
                    else:
                        query += """({0} {1} {2}) & """.format(key, compare, value) 
                else:
                    if is_nan_allowed: # np.nan is 0.00000000e+000 but results too much: ({0} == 0.00000000e+000) or ({0} != {0}) 
                        query += """(({0} == {1}) | (({0} != {0})))) & """.format(key, value)
                    else:
                        query += """({0} == {1}) & """.format(key, value) 
            
        query += """ True )"""
        print(query)
        calls_table_row = calls_table.row
        
        if outsingle:
            passvalues_calls += [[calls_table_row[COLUMN_NAMES[single_nr]] for single_nr in outsingle] for calls_table_row in calls_table.where(query)]
        elif output_range:
            # edit min column count if change! column_count-1 len(COLUMN_NAMES)-1
            passvalues_calls += [(calls_table_row[max(0,output_range[0]):min(output_range[1],14)]) for calls_table_row in calls_table.where(query)]
        else:
            passvalues_calls += [(calls_table_row[:]) for calls_table_row in calls_table.where(query)]
    else:
        calls_table_row = calls_table.row
        passvalues_calls += [(calls_table_row[:]) for calls_table_row in calls_table.iterrows() 
                                if all( 
                                        ((searched_call_params[x] is None)  or 
                                        (isinstance(searched_call_params[x], int) and calls_table_row[x] == searched_call_params[x]) or
                                        (isinstance(searched_call_params[x], float) and calls_table_row[x] == searched_call_params[x]) or 
                                        (isinstance(searched_call_params[x], str) and calls_table_row[x] == searched_call_params[x].encode()))
                                         for x in searched_call_params )]
    #print('Gefundene Calls: ', passvalues_calls)
    #print('Anzahl', len(passvalues_calls))
    return passvalues_calls



def main(hdf5_file, is_search_in_kernel = True, **kwargs):
#    try:
    print(is_search_in_kernel)
    
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    get_call_from_params_dict(calls_table, kwargs, is_search_in_kernel)
    
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-sid", "--sampleid", type=int, help="sample id searched")
    parser.add_argument("-vid", "--variantid", type=int, help="variant id searched")
    parser.add_argument("-vtid", "--varianttableid", type=int, choices=(0,1), help="variant table id searched")
    parser.add_argument("-qual", "--qual", type=float, help="call qual searched")
    parser.add_argument("-heterozygous", "--isheterozygous", type=int, choices=(0,1), help="call is_heterozygous searched")
    parser.add_argument("-readd", "--readdepth", type=int, help="call read_depth searched")
    parser.add_argument("-refd", "--refdepth",  type=int, help="call ref_depth searched")
    parser.add_argument("-altd", "--altdepth", type=int, help="call alt_depth searched")
    parser.add_argument("-strandb", "--strandbias", type=float, help="call strand_bias searched")
    parser.add_argument("-qualbd", "--qualbydepth", type=float, help="call qual_by_depth searched")
    parser.add_argument("-mapqual", "--mappingqual", type=float, help="call mapping_qual searched")
    parser.add_argument("-hs", "--haplotypescore", type=float, help="call haplotype_score searched")
    parser.add_argument("-mqb", "--mappingqualbias", type=float, help="call mapping_qual_bias searched")
    parser.add_argument("-rpb", "--readposbias", type=float, help="call read_pos_bias searched")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    
    print(args.issearchnotinkernel)
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.issearchnotinkernel, sample_id = args.sampleid, variant_id = args.variantid, variant_table_id = args.varianttableid, qual = args.qual, is_heterozygous = args.isheterozygous, read_depth = args.readdepth, ref_depth = args.refdepth, alt_depth = args.altdepth, strand_bias = args.strandbias, qual_by_depth = args.qualbydepth, mapping_qual = args.mappingqual, haplotype_score = args.haplotypescore, mapping_qual_bias = args.mappingqualbias, read_pos_bias = args.readposbias ))    
    if(args.enableprofiling):
        disable_profiling(profiling_object)

