from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np
from functioninterface_annotations import get_annotation_from_params_dict


__author__ = "Isabell Kullack"


def fields_view(arr, fields):
    """ creates view of extracted numpy array - author Christopher Schroeder """
    dtype2 = np.dtype({name:arr.dtype.fields[name] for name in fields})
    return np.ndarray(arr.shape, dtype2, arr, 0, arr.strides) 


@profile_method
def get_annotations_from_samples_by_calls_qual__all(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, qual_low_limit = None, samples_searched = ['RB_E_021', 'RB_E_032'], is_search_in_kernel = True):
    """
    prints entries of calls, samples, variants and annotations joined
    """
    start=time.time()
    
    # print('number of rows in table', annotations_table._v_attrs.NROWS)
    annotations_row = annotations_table.row
    calls_row = calls_table.row
    samples_row = samples_table.row
    variants_snp_row = variants_snp_table.row
    
    passvalues_annotations = set()
    #passvalues_calls = set()
    passvalues_calls = []
    passvalues_sample = set()
    
    if qual_low_limit:
        qual_low_limit = float(qual_low_limit)
    
    samples_searched = ['RB_E_021', 'RB_E_032']
    #qual_low_limit = 50
    
    # get sample id by asseccion    
    passvalues_sample = get_sample_id_from_list(samples_table, samples_searched)
    
    # get all call entries for sample ids
    passvalues_calls = get_call_entry_from_sample_id_list(calls_table, passvalues_sample, qual_low_limit, is_search_in_kernel)

    print('Anzahl gefundene Calls', len(passvalues_calls))
    
    print("Zeit 1. Teil:" ,time.time()-start)
    start=time.time()
    output = dict()
    
    # get annotation table as numpy array
    all_annotations = annotations_table.read()
    #passvalues_annotations = get_annotation_count_list(annotations_table, variant_id_table_id_list, is_search_in_kernel, False)
    
    print("Zeit 2. Teil:" ,time.time()-start)
    
    
    #get_annotation_from_params_dict(annotations_table, dict({ 'variant_id': call[1], 'variant_table_id': call[2]}), is_search_in_kernel)
    
    
    for call_counter, call in enumerate(passvalues_calls):
        #print(call)
        #print(call[1])
        #print(call[2])
        
        """#Konfiguration 3 
        bools = all_annotations['variant_id'] == call[1]
        bools2 = all_annotations['variant_table_id'] == call[2]
        print("Zeit 0. Teil:" ,time.time()-start)
        start=time.time()
        bools3 = np.array([all(b) for b in zip(bools, bools2)])
        annotations_of_call = all_annotations[bools.reshape(all_annotations.shape[0])]"""
        
        #Konfiguration 4
        tuple = np.array((call[1], call[2]), dtype = {'names':['variant_id','variant_table_id'], 'formats':['<i8','i1'], 'offsets':[8,16], 'itemsize':17})
        #print(tuple)
        vid_vtid_view = fields_view(all_annotations, ["variant_id", "variant_table_id"])
        #print(vid_vtid_view)
        bools = np.in1d(vid_vtid_view, tuple) 
        #print(vid_vtid_view.dtype)
        #print(bools)
        #print(any(bools))
        #print(len(all_annotations))
        #print(len(bools))
        annotations_of_call = all_annotations[bools]
        
        
        #annotations_of_call = [ annotation for annotation in all_annotations if annotation['variant_id'] == call[1] and annotation['variant_table_id'] == call[2]]
        #print(call[2]
        
        
        #print(annotations_of_call)
        if output:
            output.update({call_counter: {'call': call, 'annotation': annotations_of_call}})
        else:
            output = dict({call_counter: {'call': call, 'annotation': annotations_of_call}})
        #print(output)
        print(time.time()-start)
    
    return output

    
    """start = time.time()
    # TODO variants NO SNP table?
    for counter,(variant_id, variant_table_id) in enumerate(passvalues_calls):
        # round brackets inside required"""
        #query = """((variant_id == {0}) & (variant_table_id == {1}))""".format(variant_id, variant_table_id) 
        #print(query)
        
    """    passvalues_annotations.update({(annotations_row[:]) for annotations_row in annotations_table.where(query)} )
        #print('gefundene annotations', passvalues_annotations)    
        #if counter % 100 == 0:
        print('durchlauf',time.time() - start)
    print('gefundene annotations', passvalues_annotations)
    return passvalues_annotations"""





   
def main(hdf5_file, test_query_number, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    output = get_annotations_from_samples_by_calls_qual__all(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel)
    


#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)
#        get_number_of_rows(variants_table)

