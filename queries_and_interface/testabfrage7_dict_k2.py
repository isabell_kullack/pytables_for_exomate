from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np
from functioninterface_annotations import get_annotation_from_params_dict
from functioninterface_calls import get_call_from_params_dict


__author__ = "Isabell Kullack"


#@profile_method
def get_annotations_from_samples_by_calls_qual__opt(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel, qual_low_limit = None, samples_searched = ['RB_E_021', 'RB_E_032']):
    """
    prints entries of calls, samples, variants and annotations joined
    """
    start=time.time()
    
    annotations_row = annotations_table.row
    calls_row = calls_table.row
    samples_row = samples_table.row
    variants_snp_row = variants_snp_table.row
    
    #passvalues_calls = set()
    passvalues_calls = []
    passvalues_sample = set()
    
    if qual_low_limit:
        qual_low_limit = float(qual_low_limit)
    
        
    #get_annotation_entry_by_variant_id(annotations_table, is_search_in_kernel, searched_variant_id, variant_table_name = None, variant_table_id = None)

    # get sample_id by asseccion
    passvalues_sample = get_sample_id_from_list(samples_table, samples_searched)
    # get variant_id and variant_table_id by sample id from call
    #passvalues_calls = get_variant_id_table_id_from_sample_id_set(calls_table, passvalues_sample, qual_low_limit)
    passvalues_calls = get_variant_id_table_id_from_sample_id_list(calls_table, passvalues_sample, qual_low_limit)
    call_len = len(passvalues_calls)
    print('Anzahl gefundene Calls', call_len)

    """
    count_annotation_matches = get_annotation_count(annotations_table, passvalues_calls, is_search_in_kernel) #TODO
    print('Anzahl gefundene Annotations', count_annotation_matches)
    """
    prediction_annotations = 758629
    count_annotation_matches = prediction_annotations
    
    # returns as in evaluated query: sample_id, variant_id, call-entries, all annotation-entries
    
    #print(output)
    #print(output.shape)
    number_of_columns = 44#shape does not work
    call_counter = 0
    annotation_counter = 0
    is_new_call = True
    annotations_of_call = []
    call_entry = []
    call_of_variant_counter = 0
    row = 0
    
    print("Zeit 1. Teil:" ,time.time()-start)
    start=time.time()
    #iterrates over rows
    
    output = dict()
    
    for call_counter, call in enumerate(passvalues_calls):
        #call_entry = get_call_entry_by_id(calls_table, call[1], passvalues_sample, is_search_in_kernel, call[0])
        call_entry = get_call_from_params_dict(calls_table, dict({ 'variant_id': call[0], 'variant_table_id': call[1]}), is_search_in_kernel)
        annotations_of_call = get_annotation_from_params_dict(annotations_table, dict({ 'variant_id': [call[0]], 'variant_table_id': [call[1]]}), is_search_in_kernel)
        if output:
            output.update({call_counter: {'call': call_entry, 'annotation': annotations_of_call}})
        else:
            output = dict({call_counter: {'call': call_entry, 'annotation': annotations_of_call}})
        #print(output)
        print(time.time()-start)
    
    return output


   
def main(hdf5_file, test_query_number, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    output = get_annotations_from_samples_by_calls_qual__opt(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel)


#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

