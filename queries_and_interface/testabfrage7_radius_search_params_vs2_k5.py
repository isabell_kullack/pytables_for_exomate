from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np
from functioninterface import insert_query

__author__ = "Isabell Kullack"


def fields_view(arr, fields):
    """ creates view of extracted numpy array - author Christopher Schroeder """
    dtype2 = np.dtype({name:arr.dtype.fields[name] for name in fields})
    return np.ndarray(arr.shape, dtype2, arr, 0, arr.strides) 


#@profile_method
def get_annotations_from_samples_by_calls_qual__radius_search_with_params(statistics_snp_table, statistics_nosnp_table, calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, qual_low_limit = None, samples_searched = ['RB_E_021', 'RB_E_032'], is_search_in_kernel = True, is_annotation_required = False, gene_name_searched = 'PQBP1'):
    """
    prints entries of calls, samples, variants and annotations joined
    """
    #start=time.time()
    
    # print('number of rows in table', annotations_table._v_attrs.NROWS)
    annotations_row = annotations_table.row
    calls_row = calls_table.row
    samples_row = samples_table.row
    variants_snp_row = variants_snp_table.row
    
    passvalues_annotations = set()
    #passvalues_calls = set()
    passvalues_calls = []
    passvalues_sample = set()
    passvalues_calls_snps = []
    passvalues_calls_nosnps = []
    
    if qual_low_limit is not None:
        qual_low_limit = float(qual_low_limit)
    
    # get sample id by asseccion    
    passvalues_sample = get_sample_id_from_list(samples_table, samples_searched)
    
    
    snps_call_params = dict({
                          'variant_table_id': 0,
                          #'sample_id': passvalues_sample,
                          'strand_bias': 60.0, 
                            'qual_by_depth': 2.0,
                            'mapping_qual': 40.0, 
                            'haplotype_score': 13.0,
                            'mapping_qual_bias': -12.5,
                            'read_pos_bias' : -8.0
                          
                          })
                          
    snps_call_compare_params = dict({
                            'strand_bias': '<=', 
                            'qual_by_depth': '>=',
                            'mapping_qual': '>=', 
                            'haplotype_score': '>=',
                            'mapping_qual_bias': '>=',
                            'read_pos_bias' : '>='
                            }) 
                            
    nosnps_call_params = dict({
                          'variant_table_id': 1,
                          #'sample_id': passvalues_sample,
                          'strand_bias': 20.0, 
                            'qual_by_depth': 2.0,
                            'read_pos_bias' : -20.0
                          
                          })
                          
    nosnps_call_compare_params = dict({
                            'strand_bias': '<=', 
                            'qual_by_depth': '>=',
                            'read_pos_bias' : '>='
                            }) 
            
            
    if isinstance(qual_low_limit, float):
        snps_call_params['qual'] = qual_low_limit
        snps_call_compare_params['qual'] = '>='
        nosnps_call_params['qual'] = qual_low_limit
        nosnps_call_compare_params['qual'] = '>='
    
    
    for sample in passvalues_sample:
        snps_call_params['sample_id'] = sample
        nosnps_call_params['sample_id'] = sample
        
        search_values_snps = dict({'calls' : {'table_ref':calls_table, 'params' : snps_call_params, 'compares' : snps_call_compare_params, 'output_range' : None, 'outsingle' : None}, 'samples' : None, 'variants' : None, 'annotations' : None})
        search_values_nosnps = dict({'calls' : {'table_ref':calls_table, 'params' : nosnps_call_params, 'compares' : nosnps_call_compare_params, 'output_range' : None, 'outsingle' : None}, 'samples' : None, 'variants' : None, 'annotations' : None})
    
        # results of possibly other queries not used and not done - because other table values are None!
        s, c, v, a = insert_query(search_values_snps, is_search_in_kernel, True)
        passvalues_calls_snps += c
        #print('Anzahl gefundene Calls SNPs', len(passvalues_calls_snps))
    
        # results of possibly other queries not used and not done - because other table values are None!
        s, c, v, a = insert_query(search_values_nosnps, is_search_in_kernel, True)
        passvalues_calls_nosnps += c
        #print('Anzahl gefundene Calls NoSNPs', len(passvalues_calls_nosnps))
    
    
    # get all call entries for sample ids - former version
    #passvalues_calls = get_call_entry_from_sample_id_list(calls_table, passvalues_sample, qual_low_limit, is_search_in_kernel)
    
    passvalues_calls = passvalues_calls_snps + passvalues_calls_nosnps
    
    #print('Anzahl gefundene Calls', len(passvalues_calls))
    #print(passvalues_calls)
    
    #print("Zeit 1. Teil:" ,time.time()-start)
    #start=time.time()
    output = dict()
    
        
    for call_counter, call in enumerate(passvalues_calls):
        #print(call)
        annotations_of_call = None
        #print('vid ', call[1])
        #print('vtid ', call[2])
        
        if call[2]:
            statistics_entry = get_entry_by_radius_search(statistics_nosnp_table, call[1], 'variant_id')
        else:
            statistics_entry = get_entry_by_radius_search(statistics_snp_table, call[1], 'variant_id')
        
        if statistics_entry is None:
            print("Error. Not sufficient statistics created.")
            return None
        
        if statistics_entry[0]['annotation_count'][0]:
            #print('vid ', call[1])
            #print('vtid ', call[2])
        
            #print('statistic_entry ', statistics_entry)
            #print('annotation start id', statistics_entry[0]['annotation_start_id'][0])
            #print('annotation Anzahl', statistics_entry[0]['annotation_count'][0])
            
        
            temp_annotations_of_call, is_fully_entry = get_range_of_nparray(annotations_table, statistics_entry[0]['annotation_start_id'][0], statistics_entry[0]['annotation_count'][0], call[1], 'variant_id')
            
            #print(temp_annotations_of_call)
            #for tests:
            #is_fully_entry = False
            
            if is_fully_entry:
                result = []
                #print(annotations_of_call)

                for annotation in temp_annotations_of_call:
                    #print(annotation, 'eintrag')
                    #print(annotation['gene_name'])
                    
                    if annotation['gene_name'] == gene_name_searched.encode():
                        #print(annotation['gene_name'])
                        #print(result)
                        result.append(annotation)
                                    
                annotations_of_call = result
                
            else:
                #print('vid ', call[1])
                #print('vtid ', call[2])
        
                #print('statistic_entry ', statistics_entry)
                #print('annotation start id', statistics_entry[0]['annotation_start_id'][0])
                #print('annotation Anzahl', statistics_entry[0]['annotation_count'][0])
                
                                
                query = """((variant_id == {0}) & (variant_table_id == {1}) & (gene_name == {2}))""".format(call[1], call[2], gene_name_searched.encode()) 
                #print(query)
                pyt_result = [(annotations_row[:]) for annotations_row in annotations_table.where(query)]
                #print("PyTables result", pyt_result)
                #print("Annotation not totally found", temp_annotations_of_call)
                
                #print(statistics_entry[0]['annotation_count'][0], 'annotations required, but only found: ', len(pyt_result))
                
                annotations_of_call = pyt_result
                
        if is_annotation_required:
            if annotations_of_call:
                if output:
                    output.update({call_counter: {'call': call, 'annotation': annotations_of_call}})
                else:
                    output = dict({call_counter: {'call': call, 'annotation': annotations_of_call}})
        else:
            if output:
                output.update({call_counter: {'call': call, 'annotation': annotations_of_call}})
            else:
                output = dict({call_counter: {'call': call, 'annotation': annotations_of_call}})
        #print("Zeilendurchlauf Output-Dict:" ,time.time()-start)
    
    #print("Zeit 2. Teil:" ,time.time()-start)
    #print(output)
        
    return output

    
   
def main(hdf5_file, test_query_number, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    hdf5_file2 = 'statistic.h5'
    
    hdf5_file_obj2 = open_in_mode(hdf5_file2, 'r')
    
    statistics_snp_table, statistics_nosnp_table = get_prepare_statistics(hdf5_file_obj2)
    
    is_annotation_required = True
    gene_name_searched = 'PYGL'
    
    output = get_annotations_from_samples_by_calls_qual__radius_search_with_params(statistics_snp_table, statistics_nosnp_table, calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel=is_search_in_kernel, is_annotation_required=is_annotation_required, gene_name_searched=gene_name_searched)
    


#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    hdf5_file_obj2.close()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

