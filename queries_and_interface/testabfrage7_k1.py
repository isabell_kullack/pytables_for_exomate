from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np


__author__ = "Isabell Kullack"


#@profile_method
def get_annotations_from_samples_by_calls_qual__in_kernel__to_array(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel, qual_low_limit = None, samples_searched = ['RB_E_021', 'RB_E_032']):
    """
    prints entries of calls, samples, variants and annotations joined
    """
    start=time.time()
    
    annotations_row = annotations_table.row
    calls_row = calls_table.row
    samples_row = samples_table.row
    variants_snp_row = variants_snp_table.row
    
    #passvalues_calls = set()
    passvalues_calls = []
    passvalues_sample = set()
    
    if qual_low_limit:
        qual_low_limit = float(qual_low_limit)
    
        
    #get_annotation_entry_by_variant_id(annotations_table, is_search_in_kernel, searched_variant_id, variant_table_name = None, variant_table_id = None)

    # get sample_id by asseccion
    passvalues_sample = get_sample_id_from_list(samples_table, samples_searched)
    # get variant_id and variant_table_id by sample id from call
    #passvalues_calls = get_variant_id_table_id_from_sample_id_set(calls_table, passvalues_sample, qual_low_limit)
    passvalues_calls = get_variant_id_table_id_from_sample_id_list(calls_table, passvalues_sample, qual_low_limit)
    call_len = len(passvalues_calls)
    print('Anzahl gefundene Calls', call_len)

    """
    count_annotation_matches = get_annotation_count(annotations_table, passvalues_calls, is_search_in_kernel) #TODO
    print('Anzahl gefundene Annotations', count_annotation_matches)
    """
    prediction_annotations = 758629
    count_annotation_matches = prediction_annotations
    
    # returns as in evaluated query: sample_id, variant_id, call-entries, all annotation-entries
    output = np.zeros(count_annotation_matches, dtype={'names':['sample_id', 'variant_id', 'variant_table_id', 'qual', 'is_heterozygous', 'read_depth', 'ref_depth', 'alt_depth', 'strand_bias', 'qual_by_depth',
                                                                'mapping_qual', 'haplotype_score', 'mapping_qual_bias','read_pos_bias', 'annotation_id', 'variant_id_ann', 'variant_table_id_ann',
                                                                'transcript_id_ann', 'allele', 'annotation', 'annotation2', 'annotation3', 'annotation4', 'annotation5', 'annotation6', 'annotation7',
                                                                'putative_impact', 'gene_name', 'gene_id', 'feature_type', 'feature_id', 'transcript_biotype', 'rank', 'total', 'hgvsc', 'hgvsp', 
                                                                'cdna_position', 'cdna_len', 'cds_position', 'cds_len', 'protein_position', 'protein_len', 'distance_to_feature', 'info'],
                                                      'formats':['i8', 'i8', 'i2', 'f8', 'i2', 'i8', 'i8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'i8', 'i8', 'i2', 'i4', 'S30', 'S180', 'S25', 'S25', 'S25', 'S25', 'S25', 'S25', 'S8', 'S40', 'S40', 'S24', 'S40', 'S10', 'i8', 'i8', 'S30', 'S30', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'S30']})
    
    #print(output)
    #print(output.shape)
    number_of_columns = 44#shape does not work
    call_counter = 0
    annotation_counter = 0
    is_new_call = True
    annotations_of_call = []
    call_entry = []
    call_of_variant_counter = 0
    row = 0
    
    print("Zeit 1. Teil:" ,time.time()-start)
    start=time.time()
    #iterrates over rows
    for row in range(count_annotation_matches): #call in enumerate(passvalues_calls):
        #print(call[0])
        column = 0
        annotation_value = 0
        #print(call_counter, "Call Counter")
        #for call in passvalues_calls:
        if is_new_call:
            #print(passvalues_calls[call_counter], 'aktueller Call')
            #print(call_counter, 'call counter')
            if call_counter < call_len:
                call_entry = get_call_entry_by_id(calls_table, passvalues_calls[call_counter][1], passvalues_sample, is_search_in_kernel, passvalues_calls[call_counter][0])
                #print(call_entry, 'call_entry')
            #print(len(call_entry), 'Anzahl Calls')
                annotations_of_call = get_annotation_entry_by_variant_id(annotations_table, passvalues_calls[call_counter][1], is_search_in_kernel, passvalues_calls[call_counter][0])
                #annotations_of_call = get_annotation_from_params_dict(annotations_table, dict({ 'variant_id': passvalues_calls[call_counter][0], 'variant_table_id': passvalues_calls[call_counter][1]}), is_search_in_kernel)
                
                is_new_call = False
                if len(call_entry) == 0:
                    print('Zu Ende. Annotationen gefunden: ', row)
                    break
                #print(annotations_of_call)
                if len(annotations_of_call) == 0:
                    annotations_of_call = [(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)]
                print("Zeilendurchlauf Output-Array:" ,time.time()-start)
                
            else:
                break
        #print('Anzahl an Annotationen', len(annotations_of_call))
        #print('call_of_variant_counter', call_of_variant_counter)
        #print('Zeile', row)
        
        #iterrates over columns
        for column in range(number_of_columns):
            #entry of each call value in first cells
            #print(row,column)
            
            
            if column <= 13:
                #print(call_counter, 'call counter')
                #print(call_entry[call_counter][column])
                output[row][column] = call_entry[call_of_variant_counter][column]
                #print(output)
            #entry of each annotation value 
            else:
                #print('Annotationcounter', annotation_counter)
                #print('Anzahl annotation values ', len(annotations_of_call[annotation_counter]))
                #print('Annotation value', annotation_value)
                #print('Spalte', column)
                #print('value', annotations_of_call[annotation_counter][annotation_value])
                output[row][column] = annotations_of_call[annotation_counter][annotation_value]
                annotation_value += 1
                #print(output[row][column])
            column = column + 1
                
        annotation_counter += 1
        if annotation_counter >= len(annotations_of_call):
            call_counter += 1
            call_of_variant_counter += 1
            annotation_counter = 0
        if(call_of_variant_counter >= len(call_entry)):
            is_new_call = True
            call_of_variant_counter = 0
        #print(output[row])
        
    #print(output)
    print('Annotationen gefunden: ', row+1)
    return output


   
def main(hdf5_file, test_query_number, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    output = get_annotations_from_samples_by_calls_qual__in_kernel__to_array(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel)


#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

