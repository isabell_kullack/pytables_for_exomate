from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np


__author__ = "Isabell Kullack"


#@profile_method
def testabfrage_7_join(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel, qual_low_limit = None, samples_searched = ['RB_E_021', 'RB_E_032']):
    """
    prints entries of calls, samples, variants and annotations joined
    """
    start=time.time()
    
    annotations_row = annotations_table.row
    calls_row = calls_table.row
    samples_row = samples_table.row
    variants_snp_row = variants_snp_table.row
    
    #passvalues_calls = set()
    passvalues_calls = []
    passvalues_sample = set()
    
    if qual_low_limit:
        qual_low_limit = float(qual_low_limit)
    
    start = time.time()
    output = []
    
    for sample in samples_table.where('''(accession == b'RB_E_021') | (accession == b'RB_E_032')'''):
        for call in calls_table.where('''(sample_id == sid)''' , {'sid': sample['sample_id']}):
        #for call in calls_table.where('''(sample_id == %r) & (qual >= 50)''' % sample['sample_id']):
            for annotation in annotations_table.where('''(variant_id == %r) & (variant_table_id == %r)''' % (call['variant_id'], call['variant_table_id'])):
                output.append((call[:], annotation[:]))
#                print(call[:])
#                print(annotation[:])
#                print(time.time() - start)
    print(time.time() - start)
    return output


   
def main(hdf5_file, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    output = testabfrage_7_join(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel)
    print(output)
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

