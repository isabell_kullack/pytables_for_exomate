from tables import *
from filehandler import *
from profiling import *
from datastructures import *
import time
import sys
import argparse
from interface import get_prepare
from functioninterface_annotations import get_annotation_from_params_dict
from functioninterface_variants import get_variant_from_params_dict
from functioninterface_samples import get_sample_from_params_dict
from functioninterface_calls import get_call_from_params_dict


__author__ = "Isabell Kullack"



#@profile_method
def insert_query(search_values, is_search_in_kernel = True, is_nan_allowed = False, show_result = False):
    """ fuctional interfaces splits up dictionary searched input values of search_values to functional interfaces of tables """
    #hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    #calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    passvalues_samples = []
    passvalues_calls = []
    passvalues_variants = []
    passvalues_annotations = []
    
    if 'calls' in search_values.keys() and search_values['calls']:
        passvalues_calls = get_call_from_params_dict(search_values['calls']['table_ref'], search_values['calls']['params'], is_search_in_kernel, search_values['calls']['output_range'], search_values['calls']['outsingle'], search_values['calls']['compares'], is_nan_allowed)    
    
    if 'samples' in search_values.keys() and search_values['samples']:
        passvalues_samples = get_sample_from_params_dict(search_values['samples']['table_ref'], search_values['samples']['params'], is_search_in_kernel, search_values['samples']['output_range'], search_values['samples']['outsingle'], search_values['samples']['compares'])
    
    if 'variants' in search_values.keys() and search_values['variants']:
        passvalues_variants = get_variant_from_params_dict(search_values['variants']['table_ref_snps'], search_values['variants']['table_ref_nosnps'], search_values['variants']['params'], is_search_in_kernel, search_values['variants']['output_range'], search_values['variants']['outsingle'], search_values['variants']['compares'])
    
    
    if 'annotations' in search_values.keys() and search_values['annotations']:
        passvalues_annotations = get_annotation_from_params_dict(search_values['annotations']['table_ref'], search_values['annotations']['params'], is_search_in_kernel, search_values['annotations']['output_range'], search_values['annotations']['outsingle'], search_values['annotations']['compares'])
    
    if show_result:
        print(passvalues_samples, passvalues_calls, passvalues_variants, passvalues_annotations)
    #hdf5_file_obj.close()
    return passvalues_samples, passvalues_calls, passvalues_variants, passvalues_annotations



def main(hdf5_file, is_search_in_kernel = True, show_result = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    searched_call_params = dict({
                          'variant_id': 3,
                          'sample_id': 2
                          })
    searched_sample_params = dict({
                          'tissue': ['B','T'],
                          'sample_id': 2
                          })
    searched_annotation_params = dict({'annotation_id': [2], 
                          'variant_id': [3]
                          })
    searched_variant_params = dict({'variant_id': 3, 
                          'chrom': '01'
                          })
                          
    searched_call_compare_params = dict({'variant_id': '==', 
                          'sample_id': '<='}) 
    searched_sample_compare_params = dict({'annotation_id': '==', 
                          'sample_id': '<='})                      
    searched_annotation_compare_params = dict({'annotation_id': '==', 
                          'variant_id': '<='})
    searched_variant_compare_params = dict({'annotation_id': '==', 
                          'variant_id': '<='})
    
    
    search_values = dict({'calls' : {'table_ref':calls_table, 'params' : searched_call_params, 'compares' : searched_call_compare_params, 'output_range' : None, 'outsingle' : None}, 
                          #'samples' : None,
                          'samples' : {'table_ref':samples_table, 'params' : searched_sample_params, 'compares' : searched_sample_compare_params, 'output_range' : None, 'outsingle' : None}, 
                          #'variants' : None,
                          #'variants' : {'table_ref_snps':variants_snp_table, 'table_ref_nosnps':variants_nosnp_table, 'params' : searched_variant_params, 'compares' : searched_variant_compare_params, 'output_range' : None, 'outsingle' : None}, 
                          #'annotations' : {'table_ref':annotations_table, 'params' : searched_annotation_params, 'compares' : searched_annotation_compare_params, 'output_range' : None, 'outsingle' : None}
                          })
    
    passvalues_samples, passvalues_calls, passvalues_variants, passvalues_annotations = insert_query(search_values, is_search_in_kernel=is_search_in_kernel, show_result=show_result)
    
    hdf5_file_obj.close()
    
#    except:
#        pass
#    finally:
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    

    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.issearchnotinkernel))    
    if(args.enableprofiling):
        disable_profiling(profiling_object)

