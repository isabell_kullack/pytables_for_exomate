from tables import *
import sys
import argparse
from interface import *


__author__ = "Isabell Kullack"



   
def main(hdf5_file, indexnr, is_cs_index):
    """set index konfiguration on tables by konfiguration number"""
    
    hdf5_file_obj = open_in_mode(hdf5_file, 'a')
    
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    create_indexes(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_cs_index = is_cs_index, index_number = indexnr)
    
    hdf5_file_obj.close()
    
    """hdf5_file_obj = open_in_mode(hdf5_file, 'a')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    variants_snp_table.remove_rows(0, 100)
    hdf5_file_obj.close()"""
    
    
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("indexnr", type=int, choices=(0,1,2,3,4,5,6), help="number of index konfiguration")
    parser.add_argument("iscsindex", type=bool, choices=(True, False), help="boolean if sorted index shall be created")

    
    args = parser.parse_args()
    
    sys.exit(main(args.hdf5file, args.indexnr, args.iscsindex))
    
