from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np
#from datastructures import variant_table_reverse as variant_table


__author__ = "Isabell Kullack"


#@profile_method
def get_variants_clinical_from_patient(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, samples_searched = ['RB_E_011'], qual_low_limit = 50, is_search_in_kernel=True):
    #start = time.time()
    passvalues_calls_A = set()
    passvalues_sample_A = set()
    passvalues_calls_B = set()
    passvalues_sample_B = set()
    intersection_calls = set()
    passvalues_variants = set() 
    difference_calls = set()
    calls_A_without_B = set()
    calls_B_without_A = set()
    
    variant_table = dict( {
                            0 : variants_snp_table, 
                            1 : variants_nosnp_table}
                            )
    
    if isinstance(samples_searched,str):
        samples_searched = [samples_searched]
    
    # get sample id by asseccion    
    passvalues_sample_A = get_sample_id_from_list(samples_table, samples_searched, "T", is_search_in_kernel=is_search_in_kernel)
    #print(passvalues_sample_A)
    
    # get tuple of variant_id and variant_table_id
    passvalues_calls_A = get_variant_id_table_id_from_sample_id_set(calls_table, passvalues_sample_A, qual_low_limit, is_search_in_kernel)
    #print(passvalues_calls)
    
    # get sample id by asseccion    
    passvalues_sample_B = get_sample_id_from_list(samples_table, samples_searched, "B", is_search_in_kernel=is_search_in_kernel)
    #print(passvalues_sample_B)
    
    # get tuple of variant_id and variant_table_id
    passvalues_calls_B = get_variant_id_table_id_from_sample_id_set(calls_table, passvalues_sample_B, qual_low_limit, is_search_in_kernel)
    #print(passvalues_calls_B)
    
    # get intersection of variant_tuples
    intersection_calls = passvalues_calls_A & passvalues_calls_B
    #print(searched_calls)
    
    # get differences of variant_tuples
    difference_calls = passvalues_calls_A ^ passvalues_calls_B
    calls_A_without_B = passvalues_calls_A - passvalues_calls_B
    calls_B_without_A = passvalues_calls_B - passvalues_calls_A
    
    #print('Anzahl Calls: ', len(intersection_calls))
    #passvalues_variants = get_variant_entry_by_variant_id_pair_list(variants_snp_table, variants_nosnp_table, intersection_calls, is_search_in_kernel = is_search_in_kernel)
    
    """from interfaceGlobalVariants import get_variant_from_params_dict
    for (variant_id, variant_table_id) in intersection_calls:
        searched_variant_params = dict({'variant_id': variant_id, 'variant_table_id': variant_table_id})
        passvalues_variants.update(get_variant_from_params_dict(variants_snp_table, variants_nosnp_table, searched_variant_params, is_search_in_kernel))"""
    #print(passvalues_variants)
    
    #print("Zeit erster Teil: ", time.time()-start)
    
    # Prediction try
    #result = get_variant_entry_by_list_of_ids(variants_snp_table, [1000000], 'variant_id')
    #print(result)
    #return
    
    
    # Konfig. 1 Set try
    #get_variant_entry_by_variant_id_pair_set(variants_snp_table, variants_nosnp_table, passvalues_calls_A, is_search_in_kernel = is_search_in_kernel)
    
    
    # Konfig. 2 List try
    # get_variant_entry_by_variant_id_pair_list(variants_snp_table, variants_nosnp_table, passvalues_calls_A, is_search_in_kernel = is_search_in_kernel)
    
    # Konfig. 3 Dict try
    #get_variant_entry_by_variant_id_pair_dict(variants_snp_table, variants_nosnp_table, passvalues_calls_A, is_search_in_kernel = is_search_in_kernel)

    # Konfig. 5
    #Nur Reguläre Abfrage möglich, IK wäre approximativ
    #get_variant_entry_by_variant_id_pair_all(variants_snp_table, variants_nosnp_table, passvalues_calls_A)
    
    
    # Konfig. 7 Array try
    #passvalues_variants = get_variant_entry_by_variant_id_pair_array(variants_snp_table, variants_nosnp_table, passvalues_calls_A, is_search_in_kernel = is_search_in_kernel)
    
    
    # testabfrage 3 end_version
    
    variants_A_without_B, variants_B_without_A, variants_difference, variants_intersection = None, None, None, None
    
    """snps0, nospns0 = get_variant_entry_by_id_pair_split(variants_snp_table, variants_nosnp_table, intersection_calls)
    variants_intersection = snps0+nospns0
    
    snps, nospns = get_variant_entry_by_id_pair_split(variants_snp_table, variants_nosnp_table, calls_A_without_B)
    variants_A_without_B = snps+nospns
    
    snps2, nospns2 = get_variant_entry_by_id_pair_split(variants_snp_table, variants_nosnp_table, calls_B_without_A)
    variants_B_without_A = snps2+nospns2
    
    snps3, nospns3 = get_variant_entry_by_id_pair_split(variants_snp_table, variants_nosnp_table, difference_calls)
    variants_difference = snps3+nospns3"""
    
    # alternatively get all variants of one sample:
    snps4, nospns4 = get_variant_entry_by_id_pair_split(variants_snp_table, variants_nosnp_table, passvalues_calls_A)
    variants_A = snps4+nospns4
    
    
    #print(len(snps)+len(nospns))
    #print(nospns)
    #passvalues_variants = get_variant_entry_by_variant_id_pair_array(variants_snp_table, variants_nosnp_table, calls_A_without_B, is_search_in_kernel = is_search_in_kernel)

    return variants_A_without_B, variants_B_without_A, variants_difference, variants_intersection, intersection_calls, difference_calls, passvalues_calls_A, passvalues_calls_B, passvalues_sample_A, passvalues_sample_B, calls_A_without_B, calls_B_without_A

    
    #return passvalues_variants, intersection_calls, difference_calls, passvalues_calls_A, passvalues_calls_B, passvalues_sample_A, passvalues_sample_B, calls_A_without_B, calls_B_without_A

   
def main(hdf5_file, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    get_variants_clinical_from_patient(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel=is_search_in_kernel)
    
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

