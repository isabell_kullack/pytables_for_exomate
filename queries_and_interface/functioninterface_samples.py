from tables import *
from filehandler import *
from profiling import *
from datastructures import *
import time
import sys
import argparse
from interface import get_prepare


__author__ = "Isabell Kullack"

COLUMN_NAMES = dict({ 0: 'sample_id', 1: 'accession', 2: 'patient_id', 3: 'tissue', 4: 'disease_id'})

#@profile_method
def get_sample_from_params_dict(samples_table, searched_sample_params, is_search_in_kernel = True, output_range = None, outsingle = None, searched_compare = None):
    """ funtional interface of samples table which creates querys of dictionary searched_sample_params and creates outsingle for each given column by number or as range of columns by output_range """

    
    passvalues_samples = []
    
    if searched_compare:
        is_search_in_kernel = True
    
    if is_search_in_kernel:
        query = """("""
        for key, value in searched_sample_params.items():
            if value is None:
                continue
            if isinstance(value, str):
                value = value.encode()
            if isinstance(value, list):
                query = """(("""
                for i in value:
                    if isinstance(i, str):
                        i = i.encode()
                    if searched_compare is not None:
                        compare = "=="
                        try:
                            if searched_compare[key]:
                                compare = searched_compare[key]
                        except KeyError:
                            compare = "=="
                        query += """({0} {1} {2}) | """.format(key, compare, i) 
                    else:
                        query += """({0} == {1}) | """.format(key, i) 
                query += """ False ) & """
            else:
                #print(key, value)
                
                if searched_compare is not None:
                    compare = "=="
                    try:
                        if searched_compare[key]:
                            compare = searched_compare[key]
                    except KeyError:
                        compare = "=="
                    query += """({0} {1} {2}) & """.format(key, compare, value) 
                else:
                    query += """({0} == {1}) & """.format(key, value) 
                
            
        query += """ True )"""
        print(query)
        samples_table_row = samples_table.row
        
        if outsingle:
           passvalues_samples += [[ samples_table_row[COLUMN_NAMES[single_nr]] for single_nr in outsingle] for samples_table_row in samples_table.where(query)]
        elif output_range:
            # edit min column count if change!
            passvalues_samples += [(samples_table_row[max(0,output_range[0]):min(output_range[1],5)]) for samples_table_row in samples_table.where(query)]
        else:
            passvalues_samples += [(samples_table_row[:]) for samples_table_row in samples_table.where(query)]
              
        
    else:
        samples_table_row = samples_table.row
        passvalues_samples += [(samples_table_row[:]) for samples_table_row in samples_table.iterrows() 
                                if all( 
                                        ((searched_sample_params[x] is None)  or 
                                        (isinstance(searched_sample_params[x], int) and samples_table_row[x] == searched_sample_params[x]) or
                                        #(isinstance(searched_sample_params[x], float) and samples_table_row[x] == searched_sample_params[x]) or 
                                        (isinstance(searched_sample_params[x], str) and samples_table_row[x] == searched_sample_params[x].encode()))
                                         for x in searched_sample_params )]
    #print('Gefundene samples: ', passvalues_samples)
    #print('Anzahl', len(passvalues_samples))
    return passvalues_samples



def main(hdf5_file, is_search_in_kernel = True, **kwargs):
#    try:
    print(is_search_in_kernel)
    
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    get_sample_from_params_dict(samples_table, kwargs, is_search_in_kernel)
    
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-sid", "--sampleid", type=int, help="sample id searched")
    parser.add_argument("-accession", "--accession", help="sample accession searched")
    parser.add_argument("-pid", "--patientid", type=int, help="patient id searched")
    parser.add_argument("-tissue", "--tissue", help="sample tissue searched")
    parser.add_argument("-did", "--diseaseid", type=int, help="disease id searched")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    
    print(args.issearchnotinkernel)
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.issearchnotinkernel, sample_id = args.sampleid, accession = args.accession, patient_id = args.patientid, tissue = args.tissue, disease_id = args.diseaseid ))    
    if(args.enableprofiling):
        disable_profiling(profiling_object)

