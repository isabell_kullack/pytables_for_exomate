from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np
from collections import defaultdict

from flask import render_template, Flask, request, make_response, get_template_attribute, g
#Response, request
from flask_admin import *
#from exomate.application import app
#from exomate.database import get_session
#from exomate.models import Variant, Call, KnownVariant
#from exomate import *
#from exomate.shortener import shorten
#from werkzeug import *
#from werkzeug.routing import Map, Rule, NotFound, RequestRedirect
#import jinja2

from testabfrage7 import get_annotations_from_samples_by_calls_qual__in_kernel, get_annotations_from_samples_by_calls_qual__iterrows, get_annotations_from_samples_by_calls_qual__in_kernel__to_array
from testabfrage6 import get_join_variants_calls_samples_entries_by_variant_chrom_and_pos
from testabfrage3_B import get_variants_clinical_from_patient


__author__ = "Isabell Kullack"

app = Flask(__name__)


"""
url_map = Map([
    Rule('/', endpoint='variants-result'),
    Rule('/variants', endpoint='/variants-result')
])

def application(environ, start_response):
    urls = url_map.bind_to_environ(environ)
    try:
        endpoint, args = urls.match()
    except HTTPException as e:
        return e(environ, start_response)
    start_response('200 OK', [('Content-Type', 'text/plain')])
    return ['Rule points to %r with arguments %r' % (endpoint, args)]
"""

@app.before_request
def start_time_measurement():
    g.start_time = time.time()
    g.request_time = lambda: "%.1fs" % (time.time() - g.start_time)


@app.route("/")
@app.route("/index")
def index(): #hdf5_file, test_query_number, is_search_in_kernel = True):
    
    hdf5_file, test_query_number, is_search_in_kernel = args.hdf5file, args.testnr, args.issearchnotinkernel
    
    version = hdf5_file
    app.jinja_env.globals.update(version=version)

    #    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')

    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    rendered_obj = None
    
    if test_query_number == 1:
        searched_id = 555
        searched_id = 1063482

        searched_id = 1019618
        searched_id = 1019619
        
        searched_id = 1073481
        searched_id = 1024189


        searched_id = 998642
    
        get_variant_entry_by_id(variants_snp_table, variants_nosnp_table, 'variant_snp_id', searched_id, is_search_in_kernel)
    if test_query_number == 2:
        max_variant_id = get_maximum_id(variants_snp_table, 'variant_id')
    if test_query_number == 3:
        qual_low_limit = 50
        passvalues_variants, intersection_calls, difference_calls, passvalues_calls_A, passvalues_calls_B, passvalues_sample_A, passvalues_sample_B, calls_A_without_B, calls_B_without_A = get_variants_clinical_from_patient(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, qual_low_limit=qual_low_limit, is_search_in_kernel=is_search_in_kernel)
        
        patients = passvalues_sample_A & passvalues_sample_B
        
        stats = defaultdict(list)
        
        print('fertig!')
        
        def stats(patient):
            callcounts = dict(intersection_calls, passvalues_sample_A, passvalues_sample_B)
            #session.query(Sample.accession, func.count(Call.sample_id)).outerjoin(Call).filter(or_(Call.qual >= quality, Call.qual == None)).join(Patient).group_by(Sample.accession).filter(Patient.id == patient.id).all())
            for count, sample1, sample2 in callcounts:
                #session.execute(intersection.format(patient=patient.accession, quality=quality)):
                excl1 = calls_A_without_B
                excl2 = calls_B_without_A
                union = float(count + excl1 + excl2)
                if not union:
                    union = 1  # ensure that ratios become 0
                common_ratio = count / union # jaccard index
                excl1_ratio = excl1 / union
                excl2_ratio = excl2 / union
                yield (
                    sample1, sample2,
                    count, common_ratio,
                    excl1, excl1_ratio,
                    excl2, excl2_ratio
                )
        
        rendered_obj = render_template('sample-overlap-result.html', quality=qual_low_limit, patients=patients, stats=stats) # +passvalues_variants
    
    if test_query_number == 4:
        
        variant = []
        variant = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_snp_id', is_search_in_kernel)
        
       

        
        
    if test_query_number == 5:

        limit = 10
        #limit = None
        gene_name = 'PQBP1' 
        # 
        #gene_name = 'PDLIM5'
        #is_search_in_kernel = False
        get_annotation_entry_by_gene_name_opt_limit(annotations_table, gene_name, limit, is_search_in_kernel)
    if test_query_number == 6:
        searched_chrom = '01'
        pos = 142637034
        
        known = False
        show_id = True
        warning = None
        not_found = False
        highlight_pos = ('X',3)
    
        #calls = sorted(output, key=lambda output: ('variant_id', 'pos'))
    
        #session = get_session()
    
        #print(calls)
    
        """
        calls = [  # fake array of posts
             { 
             'variant_id':27, 'chrom':'Y', 'pos':24, 'ref':'G', 'alt':'R', 'is_transition':True, 'is_transversion':False, 'sample_id':45, 'variant_table_id':0,
             'qual':50, 'is_heterozygous':True, 'read_depth':1, 'ref_depth':12, 'alt_depth':13, 'strand_bias':23, 'qual_by_depth':25, 'mapping_qual':468,
             'haplotype_score':4, 'mapping_qual_bias':1,'read_pos_bias':2, 'accession':'ASDF', 'patient_id':4, 'tissue':'T', 'disease_id':2 
             },
             { 
             'variant_id':23, 'chrom':'X', 'pos':24, 'ref':'A', 'alt':'G', 'is_transition':True, 'is_transversion':False, 'sample_id':45, 'variant_table_id':0,
             'qual':50, 'is_heterozygous':True, 'read_depth':1, 'ref_depth':12, 'alt_depth':13, 'strand_bias':23, 'qual_by_depth':25, 'mapping_qual':468,
             'haplotype_score':4, 'mapping_qual_bias':1,'read_pos_bias':2, 'accession':'ASDF', 'patient_id':4, 'tissue':'T', 'disease_id':2}
             ]
             print(calls)
            """
        
        output = get_join_variants_calls_samples_entries_by_variant_chrom_and_pos(calls_table, samples_table, variants_snp_table, variants_nosnp_table, is_search_in_kernel, searched_chrom, pos)
    
        rendered_obj = render_template('/variants-result.html', calls=output, known=known, not_found=not_found, show_id=show_id, warning=warning, highlight_pos=highlight_pos)
    
    if test_query_number == 7:
        qual_low_limit = None
        samples_searched = ['RB_E_021', 'RB_E_032']
        if is_search_in_kernel:
            passvalues_annotations = get_annotations_from_samples_by_calls_qual__in_kernel(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, qual_low_limit, samples_searched)
        else:
            passvalues_annotations = get_annotations_from_samples_by_calls_qual__iterrows(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, qual_low_limit, samples_searched)
    if test_query_number == 8:
        output = get_annotations_from_samples_by_calls_qual__in_kernel__to_array(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel)
    
    
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    return rendered_obj



if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")
                        
    args = parser.parse_args()

    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    #sys.exit(index(args.hdf5file, args.testnr, args.issearchnotinkernel))
    app.run(host='0.0.0.0', port=5000, threaded=True, debug=True)
    if(args.enableprofiling):
        disable_profiling(profiling_object)

