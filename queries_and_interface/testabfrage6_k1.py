from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np


__author__ = "Isabell Kullack"


#@profile_method
def get_join_variants_calls_samples_entries_by_variant_chrom_and_pos(calls_table, samples_table, variants_snp_table, variants_nosnp_table, is_search_in_kernel = True, searched_chrom = '01', pos = 142637034):
    """ 
    select * from variants v join calls c on c.variant_id=v.id join samples s on s.id=c.sample_id where v.chrom='1' and pos = 6204143; 
    
    search variantsSNP and variantsNoSNP
    
    Größe der Tabelle als Array?
    7Spalten Varianten +
    """
    passvalues_variants = []
    passvalues_variants = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_snp_id', is_search_in_kernel, searched_chrom , [pos,pos])

    	#print(passvalues_variants)
    #print(len(passvalues_variants))
    
    first_len = len(passvalues_variants)
    
    #passvalues_nosnp_variants = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_nosnp_id',  is_search_in_kernel, searched_chrom , [pos,pos])
    
    passvalues_variants = passvalues_variants + get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_nosnp_id',  is_search_in_kernel, searched_chrom , [pos,pos])
    
    
    #print(passvalues_nosnp_variants)
    #print(len(passvalues_nosnp_variants))
    #output_len = len(passvalues_snp_variants) + len(passvalues_nosnp_variants)
    #output_len = len(passvalues_variants)

    #print(output)
    variant_table_id = 0
    passvalues_calls = []
    for counter, variant in enumerate(passvalues_variants):
        #print(counter)
        if counter >= first_len:
            variant_table_id = 1
        #print(variant[0])
        
        temp_calls = get_call_entry_by_id(calls_table, variant_table_id, is_search_in_kernel=is_search_in_kernel, searched_id=variant[0]) # list(variant) +
        if temp_calls is not None:
            passvalues_calls = passvalues_calls + temp_calls
    #print(passvalues_calls)
    
    output_len = len(passvalues_calls)

    output = np.zeros(output_len, dtype={'names':['variant_id', 'chrom', 'pos', 'ref', 'alt', 'is_transition', 'is_transversion', 'sample_id', 'variant_table_id',
                                              'qual', 'is_heterozygous', 'read_depth', 'ref_depth', 'alt_depth', 'strand_bias', 'qual_by_depth', 'mapping_qual',
                                              'haplotype_score', 'mapping_qual_bias','read_pos_bias', 'accession', 'patient_id', 'tissue', 'disease_id'],
                                        'formats':['i8','S12', 'i8', 'S64', 'S64', 'i2', 'i2', 'i8', 'i2', 'f8', 'i2', 'i8', 'i8', 'i8', 'f8', 'f8', 'f8',
                                                   'f8', 'f8', 'f8', 'S30', 'i8', 'S15', 'i8']})

    #print(output)
    for counter, call in enumerate(passvalues_calls):
        #print(call[0])
        passvalues_sample = get_sample_entry_by_id(samples_table, is_search_in_kernel, call[0])[0][1:]
        #print(passvalues_sample)
        counter2 = 0
        for counter2, variant in enumerate(passvalues_variants[0]):
            output[counter][counter2] = variant
        counter2 = counter2 + 1
        output[counter][counter2] = call[0]
        counter2 = counter2 + 1
        
        for call in call[2:]:
            #print(call)
            try:
                output[counter][counter2] = call
            except:
                pass
            counter2 = counter2 + 1
        for sample in passvalues_sample:
            #print(sample)
            output[counter][counter2] = sample
            counter2 = counter2 + 1
        #print(output)
    #output[counter] = np.matrix([passvalues_variants[0] + call[1:] + passvalues_sample[0][1:]]) #TODO sample statt variant oder variant und sample id doppelt
    #print(output)
    return output

   
def main(hdf5_file, test_query_number, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    get_join_variants_calls_samples_entries_by_variant_chrom_and_pos(calls_table, samples_table, variants_snp_table, variants_nosnp_table, is_search_in_kernel)
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

