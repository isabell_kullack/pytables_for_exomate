from tables import *
from filehandler import *
from profiling import *
from datastructures import *
import time
import sys
import argparse
from operator import itemgetter
import numpy as np


__author__ = "Isabell Kullack"



def get_number_of_rows(hdf5_file, previous_rows):
    """
    prints the number of existing rows in table_name
    """
    hdf5_file_obj = open_in_mode(hdf5_file, 'r') #TODO keep open?
    calls_table, variants_snp_table, variants_nosnp_table, samples_table, annotations_table, variant_id, variant_nosnp_id, sample_id, annotation_id = get_import_objects_and_ids(hdf5_file_obj)
    snp_rows = variants_snp_table._v_attrs.NROWS
    nosnp_rows = variants_nosnp_table._v_attrs.NROWS
    previous_rows = snp_rows + nosnp_rows - previous_rows
    print("Varianten: ",previous_rows)
    hdf5_file_obj.close()
    #print("Varianten dieses Records": , table_name._v_attrs.NROWS)
    return previous_rows
    
    

#@profile_method
def get_prepare(hdf5_file):
    """ get table relations and check for indexes """
    
    calls_table = get_table_from_file(hdf5_file, "calls")
    variants_snp_table = get_table_from_file(hdf5_file, "variantsSNP")
    variants_nosnp_table = get_table_from_file(hdf5_file, "variantsNoSNP")
    samples_table = get_table_from_file(hdf5_file, "samples")
    annotations_table = get_table_from_file(hdf5_file, "annotations")
    
    #create_indexes(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table)
    return calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table



def get_prepare_statistics(hdf5_file):
    """ get table relations and check for indexes """
    
    statistics_snp_table = get_table_from_file(hdf5_file, "statisticsSNP")
    statistics_nosnp_table = get_table_from_file(hdf5_file, "statisticsNoSNP")
    
    """print(statistics_snp_table.indexed, 'statistics_snp_table.indexed')
    print(statistics_snp_table.autoindex, 'statistics_snp_table.autoindex')
    print(statistics_nosnp_table.indexed, 'statistics_nosnp_table.indexed')
    print(statistics_nosnp_table.autoindex, 'statistics_nosnp_table.autoindex')"""
    
    return statistics_snp_table, statistics_nosnp_table



#@profile_method
def get_maximum_id(table_obj, id_column):
    """ prints the number of existing rows in table_name """
    max_id = 0
    len_table = len(table_obj)
    if len_table > 0:
        row = table_obj.row
        passvalues = [ row[id_column] for row in table_obj.iterrows() ]
        if passvalues:
            max_id = max(passvalues)
            max_id = max(max_id, len_table) # otherwise zero id entry creates zero instead of 1
    return max_id



def create_indexes(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_cs_index = False, index_number= 0):
    """ display indexing of tables and create indexes if values are given """

    #annotations_table.cols.variant_table_id.create_index()
#    annotations_table.cols.variant_id.create_index() / reindex() / reindex_dirty()
#    annotations_table.cols.variant_id.remove_index()
    if index_number:
        if is_cs_index:
            samples_table.cols.sample_id.create_csindex()
            calls_table.cols.variant_id.create_csindex()
            variants_snp_table.cols.variant_id.create_csindex()
            variants_nosnp_table.cols.variant_id.create_csindex()
            annotations_table.cols.variant_id.create_csindex()
            if index_number >=2:
                calls_table.cols.sample_id.create_csindex()
                annotations_table.cols.variant_table_id.create_csindex()
                if index_number >= 3:
                    calls_table.cols.variant_table_id.create_csindex()
        else:
            samples_table.cols.sample_id.create_index()
            calls_table.cols.variant_id.create_index()
            variants_snp_table.cols.variant_id.create_index()
            variants_nosnp_table.cols.variant_id.create_index()
            annotations_table.cols.variant_id.create_index()
            if index_number >=2:
                calls_table.cols.sample_id.create_index()
                annotations_table.cols.variant_table_id.create_index()
                if index_number >= 3:
                    calls_table.cols.variant_table_id.create_index()
        print("Index corrected!")
        

    print(annotations_table.indexed, 'annotations_table.indexed')
    print(annotations_table.autoindex, 'annotations_table.autoindex')
    print(annotations_table.cols.variant_id.is_indexed, 'annotations_table.cols.variant_id.indexed')
    print(annotations_table.cols.variant_table_id.is_indexed, 'annotations_table.cols.variant_table_id.indexed')
    
    print(calls_table.indexed, 'calls_table.indexed')
    print(calls_table.autoindex, 'calls_table.autoindex')
    
    print(samples_table.indexed, 'samples_table.indexed')
    print(samples_table.autoindex, 'samples_table.autoindex')
    
    print(variants_snp_table.indexed, 'variants_snp_table.indexed')
    print(variants_snp_table.autoindex, 'variants_snp_table.autoindex')
    
    print(variants_nosnp_table.indexed, 'variants_nosnp_table.indexed')
    print(variants_nosnp_table.autoindex, 'variants_nosnp_table.autoindex')


    
#@profile_method
def get_sample_id_from_list(samples_table, samples_searched, tissue = None, is_search_in_kernel = True):
    """ gets sample ids from sample accession list and if given by tissue """
    
    passvalues_sample = set()
    samples_row = samples_table.row
    #print(samples_searched)
    if is_search_in_kernel:
        for k in samples_searched:
            if tissue:
                query = """(accession == {0}) & (tissue == {1})""".format(k.encode(), tissue.encode())
            else:
                query = """(accession == {0})""".format(k.encode())
            passvalues_sample.update({samples_row['sample_id'] for samples_row in samples_table.where(query)})
        #print("sample ids", passvalues_sample)
    else:
        if tissue:
            for k in samples_searched:
                passvalues_sample.update({samples_row['sample_id'] for samples_row in samples_table.iterrows() if (samples_row['accession'] == k.encode()) and (samples_row['tissue'] == tissue.encode())})
        else:
            for k in samples_searched:
                passvalues_sample.update({samples_row['sample_id'] for samples_row in samples_table.iterrows() if (samples_row['accession'] == k.encode())})
    return passvalues_sample



#depricated and false reduction in sets.
#@profile_method 
def get_variant_id_table_id_from_sample_id_set(calls_table, passvalues_sample, qual_low_limit = None, is_search_in_kernel = True):
    """ select variant id and variant table id by sample id and optional qual minimum value by sample id as set output - depricated """
    
    passvalues_calls = set()
    #start = time.time()
    calls_row = calls_table.row
    if is_search_in_kernel:
        for sample_id in passvalues_sample:
            # round brackets inside required
            if qual_low_limit is None:
                query = """(sample_id == {0})""".format(sample_id)
            else:
                query = """((sample_id == {0}) & (qual >= {1}))""".format(sample_id, qual_low_limit)
            passvalues_calls.update({(calls_row['variant_id'],calls_row['variant_table_id']) for calls_row in calls_table.where(query)})
                #print(query)
            #print('Durchlauf call',time.time() - start)
            #print('Anzahl gefundene calls', len(passvalues_calls))
        #print('Gesamtzeit call',time.time() - start)
        #print('Anzahl gefundene calls', len(passvalues_calls))
    else:
        if qual_low_limit is None:
            for sample_id in passvalues_sample:
                passvalues_calls.update({(calls_row['variant_id'],calls_row['variant_table_id']) for calls_row in calls_table.iterrows() if (calls_row['sample_id'] == sample_id)})
                #print('Durchlauf call',time.time() - start)
                #print('Anzahl gefundene calls', len(passvalues_calls))
        else:
            for sample_id in passvalues_sample:
                passvalues_calls.update({(calls_row['variant_id'],calls_row['variant_table_id']) for calls_row in calls_table.iterrows() if (calls_row['sample_id'] == sample_id) and (calls_row['qual'] >= qual_low_limit)})
                #print('Durchlauf call',time.time() - start)
                #print('Anzahl gefundene calls', len(passvalues_calls))
        #print('Gesamtzeit call',time.time() - start)
        #print('Anzahl gefundene calls', len(passvalues_calls))
    return passvalues_calls



#@profile_method
def get_variant_id_table_id_from_sample_id_list(calls_table, sample_list, qual_low_limit = None, is_search_in_kernel = True):
    """ select a variant id of calls table by searched sample id list and optional qual minimum value """
    
    passvalues_calls = []
    #start = time.time()
    calls_row = calls_table.row
    if is_search_in_kernel:
        for sample_id in sample_list:
            # round brackets inside required
            #print(sample_id)
            if qual_low_limit is None:
                query = """(sample_id == {0})""".format(sample_id)
            else:
                query = """((sample_id == {0}) & (qual >= {1}))""".format(sample_id, qual_low_limit)
            passvalues_calls = passvalues_calls + [(calls_row['variant_id'],calls_row['variant_table_id']) for calls_row in calls_table.where(query)]
            #print(query)
            #print('Durchlauf call',time.time() - start)
            #print('Anzahl gefundene calls', len(passvalues_calls))
    else:
        if qual_low_limit is None:
            for calls_row in calls_table.iterrows():
                if (calls_row['sample_id'] in sample_list):
                    passvalues_calls += [list(calls_row['variant_id'],calls_row['variant_table_id'])]
        else:
            for calls_row in calls_table.iterrows():
                if (calls_row['sample_id'] in sample_list) and calls_row['qual'] >= qual_low_limit:
                    passvalues_calls += [list(calls_row['variant_id'],calls_row['variant_table_id'])]
        
    #print('Gesamtzeit call',time.time() - start)
    #print('Anzahl gefundene calls', len(passvalues_calls))
    return passvalues_calls


    
#@profile_method
def get_call_entry_from_sample_id_list(calls_table, sample_list, qual_low_limit = None, is_search_in_kernel = True):
    """ select a call entry by a sample id and optional qual value as list output """

    passvalues_calls = []
    #start = time.time()
    calls_row = calls_table.row
    if is_search_in_kernel:
        for sample_id in sample_list:
            # round brackets inside required
            #print(sample_id)
            if qual_low_limit is None:
                query = """(sample_id == {0})""".format(sample_id)
            else:
                query = """((sample_id == {0}) & (qual >= {1}))""".format(sample_id, qual_low_limit)
            passvalues_calls += [ calls_row[:] for calls_row in calls_table.where(query)]
            #print(query)
            #print('Durchlauf call',time.time() - start)
            #print('Anzahl gefundene calls', len(passvalues_calls))
            #print(passvalues_calls)
    else:
        if qual_low_limit is None:
            #for calls_row in calls_table.iterrows():
                #if (calls_row['sample_id'] in sample_list):
            passvalues_calls += [ calls_row[:] for calls_row in calls_table.iterrows() if calls_row['sample_id'] in sample_list]
        else:
            #for calls_row in calls_table.iterrows():
                #if (calls_row['sample_id'] in sample_list) and calls_row['qual'] >= qual_low_limit:
                    #passvalues_calls += [list(calls_row['variant_id'],calls_row['variant_table_id'])]
            passvalues_calls += [ calls_row[:] for calls_row in calls_table.iterrows() if calls_row['sample_id'] in sample_list and calls_row['qual'] >= qual_low_limit]
            #print(passvalues_calls)
        
    #print('Gesamtzeit call',time.time() - start)
    #print('Anzahl gefundene calls', len(passvalues_calls))
    return passvalues_calls



#@profile_method
def get_sample_entry_by_id(samples_table, is_search_in_kernel = True, searched_id = 1):
    """ select a sample id from sample table """
    
    samples_row = samples_table.row
    if searched_id > samples_table._v_attrs.NROWS:
        return print("Searched id not in table.")
    
    if is_search_in_kernel:
        query = """(sample_id == {0})""".format(searched_id)
        #print(query)
        passvalues = [samples_row[:] for samples_row in samples_table.where(query)]
        #print(passvalues[0])
        return passvalues
    else:
        passvalues = []
        for samples_row in samples_table.iterrows():
            if (samples_row['sample_id'] == searched_id):
                #print(samples_row[:])
                passvalues = passvalues + [list(samples_row[:])]
        return passvalues



#@profile_method
def get_call_entry_by_id(calls_table, variant_table_id, sample_set = None, is_search_in_kernel = True, searched_id = 1063482):
    """ select a call entry by variant id and variant table id and optional sample ids as a set """
    
    calls_row = calls_table.row
    if searched_id > calls_table._v_attrs.NROWS:
        return print("Searched id not in table.")
    
    if is_search_in_kernel:
        query = """((variant_id == {0}) & (variant_table_id == {1}))""".format(searched_id, variant_table_id)
        if sample_set is not None:
            query += """ & ( """
            for sample in sample_set:
                query += """(sample_id == {0})| """.format(sample)
            query += """ False ) """
        #print(query)
        passvalues = [calls_row[:] for calls_row in calls_table.where(query)]
        #print(passvalues[0])
        return passvalues
    else:
        passvalues = []
        for calls_row in calls_table.iterrows():
            if sample_set is not None:
                if (calls_row['variant_id'] == searched_id) and (calls_row['variant_table_id'] == variant_table_id) and (calls_row['sample_id'] in sample_set):
                    #print(calls_row[:])
                    passvalues = passvalues + [list(calls_row[:])]
            else:
                if (calls_row['variant_id'] == searched_id and calls_row['variant_table_id'] == variant_table_id):
                    #print(calls_row[:])
                    passvalues = passvalues + [list(calls_row[:])]
        return passvalues



#@profile_method
def get_variant_pair_split(variants_snp_table, variants_nosnp_table, variant_pair):
    variant_pair = sorted(variant_pair,key=itemgetter(1))
    """ splitting up variant searched pair into two searched lists """
    
    snps = []
    nosnps = []
    #start = time.time()
    for variant_id, table_id in variant_pair:
        #print(variant_id, table_id)
        if table_id:
            nosnps.append(variant_id)
        else:
            snps.append(variant_id)
                
    return s,n



#@profile_method
def get_variant_entry_by_id_pair_split(variants_snp_table, variants_nosnp_table, variant_pair):
    variant_pair = sorted(variant_pair,key=itemgetter(1))
    """ select a variant entry by a variant id and variant table id  - split of searched lists - preprocessing of radius search 
    """
    
    snps = []
    nosnps = []
    #start = time.time()
    for variant_id, table_id in variant_pair:
        #print(variant_id, table_id)
        if table_id:
            nosnps.append(variant_id)
        else:
            snps.append(variant_id)
                
    #print(time.time()-start)
    #print(len(snps))
    s = get_entry_by_radius_search(variants_snp_table, snps, 'variant_id')
    n = get_entry_by_radius_search(variants_nosnp_table, nosnps, 'variant_id')

    return s,n
    
    
#@profile_method
def get_entry_by_radius_search(table_name, searched_id_list, column_name):
    """ radius search around id count row of table"""
    
    if not isinstance(searched_id_list, list):
        searched_id_list = [searched_id_list]
    
    searched_id_list = sorted(searched_id_list)
    result = []
    for searched_id in searched_id_list:
        temp = table_name.read(searched_id,searched_id+1)
        if temp[column_name] == searched_id:
            result.append(temp)
        else:
            temp_result = None
            exp_numbers = (100**exp for exp in range(1, 4))
            for limit in exp_numbers:
                temp_result = get_entry_of_nparray(table_name, limit, searched_id, column_name, searched_id)
                if temp_result is not None:
                    result.append(temp_result)
                    break
                    #pass
            # if no entry found in Range (100, 10.000, 1.000.000) all is searched
            if temp_result is None:
                temp_result = get_entry_of_nparray(table_name, table_name._v_attrs.NROWS, searched_id, column_name, searched_id)
                result.append(temp_result)
            print("Please reimport variants or rearrange IDs for improved performance.")
    return result



#@profile_method
def get_entry_of_nparray(table_name, limit, searched_value, column_name, start_row):
    """ radius search to get entry as numpy array of limit for searched value around start_row"""
    
    row_count = table_name._v_attrs.NROWS
    #this = table_name.read(0,row_count) # whole table
    this = table_name.read(max(0,start_row-limit),min(start_row+limit+1, row_count))
    #print(searched_value)
    bools = this[column_name] == searched_value
    counter = 0
    
    try: 
        #print('np bool index', np.where(bools == True)[0][0])
        #print(this[np.where(bools == True)[0][0]])
        #print(this[10000000000000])
        return this[np.where(bools == True)[0][0]]
    except IndexError:
        #print('IndexError catched.')
        return None
    
    """
    # first, working solution
    for i in bools:
        if i:
            print(this[counter])
            print("counter", counter)
            return this[counter]
        counter += 1
    return None """



#@profile_method
def get_range_of_nparray(table_name, startindex, count_of_rows_to_fetch, searched_value, column_name):
    """ get range as numpy array by an area from startindex to startindex + count_of_rows_to_fetch """
    
    row_count = table_name._v_attrs.NROWS
    #this = table_name.read(0,row_count) # whole table as well as table_name.read()
    this = table_name.read(max(0,startindex),min(startindex+count_of_rows_to_fetch+1, row_count))
    bools = this[column_name] == searched_value
    counter = 0
    # annotation found at its index
    if bools.all():
        #print(this)
        return this, True
    
    #print(this)
    # radius search 
    exp_numbers = (100**exp for exp in range(1, 4))
    for limit in exp_numbers:
        this = table_name.read(max(0,startindex-limit),min(startindex+count_of_rows_to_fetch+limit, row_count))
        bools = this[column_name] == searched_value
        counter = 0
        #print(this)
        for i in bools:
            if i:
                #print(i, counter, startindex, limit, searched_value)
                this2 = table_name.read(max(0,startindex-limit+counter),min(startindex+limit+counter+count_of_rows_to_fetch, row_count))
                bools2 = this2[column_name] == searched_value
                if bools2.all():
                    #print(this)
                    print("Found but not at startindex ", startindex, " but at index ", startindex-limit+counter, " with radius ", limit)
                    return this2, True
            counter += 1
    this = table_name.read()
    bools = this[column_name] == searched_value
    counter = 0
    for i in bools:
        if i:
            this2 = table_name.read(max(0,startindex-limit+counter),min(startindex+limit+counter+count_of_rows_to_fetch, row_count))
            bools2 = this2[column_name] == searched_value
            if bools2.all():
                #print(this)
                print("Found but not at startindex ", startindex, " but at index ", startindex-limit+counter, " with radius ", limit)
                return this2, True
        counter += 1
    return None, False



#@profile_method
def get_variant_entry_by_variant_id_pair_array(variants_snp_table, variants_nosnp_table, searched_variant_pair, is_search_in_kernel = True):
    """ select a variant entry by a variant id and variant table id - numpy structured array output"""

    variant_table_reverse = dict( {
                                 0 : variants_snp_table, 
                                 1 : variants_nosnp_table}
                                 )
    #passvalues_variants = []
    searched_variant_pair = sorted(searched_variant_pair,key=itemgetter(1))
    counter_row = 0
    
    #print(len(searched_variant_pair))
    output_len = len(searched_variant_pair)

    output = np.zeros(output_len, dtype={'names':['variant_id', 'chrom', 'pos', 'ref', 'alt', 'is_transition', 'is_transversion'],
                                        'formats':['i8','S12', 'i8', 'S64', 'S64', 'i2', 'i2']})
    
    start = time.time()
    if is_search_in_kernel:    
        for (variant_id, variant_table_id) in searched_variant_pair:
            #start = time.time()
            query = """((variant_id == {0}))""".format(variant_id) 
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants = []
            passvalues_variants = [temp_table_row[:] for temp_table_row in temp_table.where(query)]
            counter_column = 0
            
            for variant in passvalues_variants[0]:
                #print(variant)
                #print(counter_row,counter_column)
                output[counter_row][counter_column] = variant
                counter_column += 1
            
            
            #print('Gefundene Varianten: ', passvalues_variants)
            #print('Anzahl', len(passvalues_variants))
            counter_row += 1
            print(time.time()-start)
    else:
        for (variant_id, variant_table_id) in searched_variant_pair:
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants = []
            passvalues_variants = [(temp_table_row[:]) for temp_table_row in temp_table.iterrows() if temp_table_row['variant_id'] == variant_id]
            counter_column = 0
            
            for variant in passvalues_variants[0]:
                #print(variant)
                #print(counter_row,counter_column)
                output[counter_row][counter_column] = variant
                counter_column += 1
            
            
            #print('Gefundene Varianten: ', passvalues_variants)
            #print('Anzahl', len(passvalues_variants))
            print(time.time()-start)
            counter_row += 1

    #print('Gefundene Varianten: ', passvalues_variants)
    #print('Anzahl Varianten', len(output))
    return output



#@profile_method
def get_variant_entry_by_variant_id_pair_dict(variants_snp_table, variants_nosnp_table, searched_variant_pair, is_search_in_kernel = True):
    """ select a variant entry by a variant id and variant table id - dictionary output version"""

    variant_table_reverse = dict( {
                                 0 : variants_snp_table, 
                                 1 : variants_nosnp_table}
                                 )
    passvalues_variants = dict()
    searched_variant_pair = sorted(searched_variant_pair,key=itemgetter(1))
    counter = 0
    start = time.time()
    
    if is_search_in_kernel:    
        for (variant_id, variant_table_id) in searched_variant_pair:
            query = """((variant_id == {0}))""".format(variant_id) 
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants.update(dict(zip([counter], [temp_table_row[:] for temp_table_row in temp_table.where(query)])))
            #passvalues_variants.update(dict(zip(['variant_id', 'chrom', 'pos', 'ref', 'alt', 'is_transition', 'is_transversion'], [temp_table_row[:] for temp_table_row in temp_table.where(query)])))
            #print('Gefundene Varianten: ', passvalues_variants)
            #print('Anzahl', len(passvalues_variants))
            counter += 1
            print(time.time()-start)
    else:
        for (variant_id, variant_table_id) in searched_variant_pair:
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants.update(dict(zip([counter], [temp_table_row[:] for temp_table_row in temp_table.iterrows() if temp_table_row['variant_id'] == variant_id])))
            counter += 1
            print(time.time()-start)
    #print('Gefundene Varianten: ', passvalues_variants)
    #print('Anzahl Varianten', len(passvalues_variants))
    return passvalues_variants
    


#@profile_method
def get_variant_entry_by_variant_id_pair_list(variants_snp_table, variants_nosnp_table, searched_variant_pair, is_search_in_kernel = True):
    """ select a variant entry by a variant id and variant table id """

    variant_table_reverse = dict( {
                                 0 : variants_snp_table, 
                                 1 : variants_nosnp_table}
                                 )
    passvalues_variants = []
    #searched_variant_pair = sorted(searched_variant_pair,key=itemgetter(1))
    start = time.time()
    if is_search_in_kernel:    
        for (variant_id, variant_table_id) in searched_variant_pair:
            query = """((variant_id == {0}))""".format(variant_id) 
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants += [temp_table_row[:] for temp_table_row in temp_table.where(query)] 
            #temp_table_row.fetch_all_fields()
            #print('Gefundene Varianten: ', passvalues_variants)
            #print('Anzahl', len(passvalues_variants))
            print(time.time()-start)
    else:
        for (variant_id, variant_table_id) in searched_variant_pair:
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants += [(temp_table_row[:]) for temp_table_row in temp_table.iterrows() if temp_table_row['variant_id'] == variant_id]
            print(time.time()-start) # 
    #print('Gefundene Varianten: ', passvalues_variants)
    #print('Anzahl Varianten', len(passvalues_variants))
    return passvalues_variants



@profile_method
def get_variant_entry_by_variant_id_pair_all(variants_snp_table, variants_nosnp_table, searched_variant_pair):
    """ select a variant entry by a variant id and variant table id - split up into two searched sets """


    variant_table_reverse = dict( {
                                 0 : variants_snp_table, 
                                 1 : variants_nosnp_table}
                                 )
    passvalues_variants = set()
    
    #searched_variant_pair = sorted(searched_variant_pair,key=itemgetter(1))
    #searched_variant_pair = sorted(searched_variant_pair, key=lambda x: x[1])
    #print(searched_variant_pair)
    snps = []
    nosnps = []
    #start = time.time()
    for variant_id, table_id in searched_variant_pair:
        #print(variant_id, table_id)
        if table_id:
            nosnps.append(variant_id)
        else:
            snps.append(variant_id)
                
    #print(time.time()-start)
    #print(len(snps))
    s = set()
    n = set()
    
    start = time.time()
    variants_snp_table_row = variants_snp_table.row
    s.update({variants_snp_table_row[:] for variants_snp_table_row in variants_snp_table.iterrows() if variants_snp_table_row['variant_id'] in snps})
    variants_nosnp_table_row = variants_nosnp_table.row
    n.update({variants_nosnp_table_row[:] for variants_nosnp_table_row in variants_nosnp_table.iterrows() if variants_nosnp_table_row['variant_id'] in nosnps})
    
    print(time.time()-start)
    return s+n



#@profile_method
def get_variant_entry_by_variant_id_pair_set(variants_snp_table, variants_nosnp_table, searched_variant_pair, is_search_in_kernel = True):
    """ select a variant entry by a variant id and variant table id """

    variant_table_reverse = dict( {
                                 0 : variants_snp_table, 
                                 1 : variants_nosnp_table}
                                 )
    passvalues_variants = set()
    
    searched_variant_pair = sorted(searched_variant_pair,key=itemgetter(1))
    #searched_variant_pair = sorted(searched_variant_pair, key=lambda x: x[1])
    #print(searched_variant_pair)
    start = time.time()
    if is_search_in_kernel:    
        for (variant_id, variant_table_id) in searched_variant_pair:
            query = """((variant_id == {0}))""".format(variant_id) 
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants.update({(temp_table_row[:]) for temp_table_row in temp_table.where(query)})
            #print('Gefundene Varianten: ', passvalues_variants)
            #print('Anzahl', len(passvalues_variants))
            #print(time.time()-start)
    else:
        for (variant_id, variant_table_id) in searched_variant_pair:
            temp_table = variant_table_reverse[variant_table_id]
            temp_table_row = temp_table.row
            passvalues_variants.update({(temp_table_row[:]) for temp_table_row in temp_table.iterrows() if temp_table_row['variant_id'] == variant_id})
            #print(time.time()-start)
    #print('Gefundene Varianten: ', passvalues_variants)
    #print('Anzahl Varianten', len(passvalues_variants))
    return passvalues_variants


#@profile_method
def get_variant_id_from_params_list(variants_snp_table, variants_nosnp_table, searched_variant_params, is_search_in_kernel = True):
    """ select variant id of a list of parameters """

    variant_table_reverse = dict( {
                                 0 : variants_snp_table, 
                                 1 : variants_nosnp_table}
                                 )
    passvalues_variants = []
    
    if is_search_in_kernel:    
        for temp_table in variant_table_reverse.values():
            query = """((chrom == {0}) & (pos == {1}) & (ref == {2}) """.format(searched_variant_params['chrom'].encode(), searched_variant_params['pos'], searched_variant_params['ref'].encode()) 
            if 'alt' in searched_variant_params:
                query += """& (alt == {0}) """.format(searched_variant_params['alt'].encode())
            query += """)"""
            temp_table_row = temp_table.row
            passvalues_variants += [(temp_table_row[:]) for temp_table_row in temp_table.where(query)]
            #print('Gefundene Varianten: ', passvalues_variants)
            #print('Anzahl', len(passvalues_variants))
        #print(query)
    else:
        #TODO: mehr dict keys abfragen
        for temp_table in variant_table_reverse.values():
            temp_table_row = temp_table.row
            passvalues_variants += [(temp_table_row[:]) for temp_table_row in temp_table.iterrows() if temp_table_row['chrom'] == searched_variant_params['chrom'].encode() and temp_table_row['pos'] == searched_variant_params['pos'] and temp_table_row['ref'] == searched_variant_params['ref'].encode()]
    #print('Gefundene Varianten: ', passvalues_variants)
    #print('Anzahl Varianten', len(passvalues_variants))
    return passvalues_variants



#@profile_method
def get_variant_entry_by_id(variants_snp_table, variants_nosnp_table, variant_table_name, searched_id = 0, is_search_in_kernel = True):
    """ select a row of variants_tables by its variant id"""
    
    temp_variant_table = variants_snp_table
    variants_row = temp_variant_table.row
    passvalues = []
    
    """if variant_table[variant_table_name] == 0:
        #print(variant_table[variant_table_name])
        temp_variant_table = variants_snp_table
        variants_row = variants_snp_table.row
    if variant_table[variant_table_name] == 1:
        temp_variant_table = variants_nosnp_table
        variants_row = variants_nosnp_table.row"""
    
    #if searched_id > temp_variant_table._v_attrs.NROWS:
    #    return print("Searched id not in table.")
    if is_search_in_kernel:
        query = """(variant_id == {0})""".format(searched_id)
        #print(query)
        passvalues = [variants_row[:] for variants_row in temp_variant_table.where(query)]
        #print(passvalues)
        
    else:
        #passvalues += [variants_row[:] for variants_row in temp_variant_table.iterrows() if variants_row['variant_id'] == searched_id]
        for variants_row in temp_variant_table.iterrows():
            if (variants_row['variant_id'] == searched_id):
                #print(variants_row[:])
                passvalues = passvalues + [list(variants_row[:])]
    return passvalues



#@profile_method
def get_variant_entry_by_id_set(temp_variant_table, searched_id = 0, is_search_in_kernel = True):
    """ select a row of variants_tables by its variant id as set comprehension """
    
    passvalues = set()
    variants_row = temp_variant_table.row
    
    if is_search_in_kernel:
        query = """(variant_id == {0})""".format(searched_id)
        #print(query)
        passvalues = {variants_row[:] for variants_row in temp_variant_table.where(query)}
        #print(passvalues)
        
    else:
        #for variants_row in temp_variant_table.iterrows():
        #    if (variants_row['variant_id'] == searched_id):
        #        print(variants_row[:])
        passvalues.update({variants_row[:] for variants_row in temp_variant_table.iterrows() if variants_row['variant_id'] == searched_id})
    return passvalues



#@profile_method
def get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, variant_table_name, is_search_in_kernel = True, searched_chrom = 'X', searched_pos_range=[48753194,48762422]):
    """ select variant entry by chrom and position range """
    #print(searched_chrom, searched_pos_range)
    temp_variant_table = None
    passvalues = []
    
    if variant_table[variant_table_name] == 0:
        #print(variant_table[variant_table_name])
        temp_variant_table = variants_snp_table
        variants_row = variants_snp_table.row
    if variant_table[variant_table_name] == 1:
        temp_variant_table = variants_nosnp_table
        variants_row = variants_nosnp_table.row
    if is_search_in_kernel:
        query = """((chrom == {0}) & (pos >= {1}) & (pos <= {2}))""".format(searched_chrom.encode(), searched_pos_range[0], searched_pos_range[1])
        #print(query)
        passvalues = [variants_row[:] for variants_row in temp_variant_table.where(query)]
        #print(passvalues[:])
    else:
        for variants_row in temp_variant_table.iterrows():
            if (variants_row['chrom'] == searched_chrom.encode() and variants_row['pos'] >= searched_pos_range[0] and variants_row['pos'] <= searched_pos_range[1]):
                #print(variants_row[:])
                passvalues = passvalues + [list(variants_row[:])]
    return passvalues



#@profile_method
def get_annotation_entry_by_variant_id(annotations_table, variant_table_id = None, is_search_in_kernel = True, searched_variant_id = 1063482):
    """
    SELECT *FROM annotations WHERE variant_id = 1063482 and variant_table_id = None
    """

    if variant_table_id is None:
        return "Either variant_table_name or variant_table_id must be given."
    
    annotations_row = annotations_table.row

    if is_search_in_kernel:
        query = """((variant_id == {0}) & (variant_table_id == {1}))""".format(searched_variant_id, variant_table_id)
        #print(query)
        passvalues = [annotations_row[:] for annotations_row in annotations_table.where(query)]
        #print(passvalues)
        #print(passvalues[0])
        return passvalues
    else:
        passvalues = []
        for annotations_row in annotations_table.iterrows():
            if (annotations_row['variant_id'] == searched_variant_id) and (annotations_row['variant_table_id'] == variant_table_id):
                #print(annotations_row[:])
                passvalues = passvalues + [list(annotations_row[:])]
        return passvalues



#@profile_method
def get_annotation_entry_by_gene_name_opt_limit(annotations_table, gene_name = 'PQBP1', limit = None, is_search_in_kernel = True):
    """ select annotation entry by gene name and optional limit of output """
    
    annotations_row = annotations_table.row
    passvalues = []
    if is_search_in_kernel:    
        query = """(gene_name == {0})""".format(gene_name.encode())
        #print(query)
        passvalues = [annotations_row[:] for annotations_row in annotations_table.where(query)]
        
    else:
        passvalues = [annotations_row[:] for annotations_row in annotations_table.iterrows() if annotations_row['gene_name'] == gene_name.encode()]
    if limit:
        #print(passvalues[:limit])
        return passvalues[:limit]
    #print(passvalues)
    return passvalues
    
    

def get_annotation_entry_by_gene_name_opt_limit2(annotations_table, gene_name = 'PQBP1', limit = None, is_search_in_kernel = True):
    """ select annotation entry by gene name and optional limit of output konfiguration 2 """
    
    annotations_row = annotations_table.row
    passvalues = []
    if is_search_in_kernel:    
        query = """(gene_name == {0})""".format(gene_name.encode())
        #print(query)
        if limit:
            passvalues = [annotations_row[:] for annotations_row in annotations_table.where(query)][:limit]
        else:
            passvalues = [annotations_row[:] for annotations_row in annotations_table.where(query)]
        
    else:
        if limit:
            passvalues = [annotations_row[:] for annotations_row in annotations_table.iterrows() if annotations_row['gene_name'] == gene_name.encode()][:limit]
        else:
            passvalues = [annotations_row[:] for annotations_row in annotations_table.iterrows() if annotations_row['gene_name'] == gene_name.encode()]
    #print(passvalues)
    return passvalues



#@profile_method
def get_annotation_count(annotations_table, variant_id_table_id_set, is_search_in_kernel = True):
    """ counts annotation previously for output - approximation of in-kernel maximum query length - deprecated"""
    #import sys
    #sys.setrecursionlimit(1000000000)
    #option = variant_id_table_id_set
    
    annotation_row = annotations_table.row
    count_annotation_matches = 0
    #is_search_in_kernel = False
    if is_search_in_kernel:
        #print('inkernel')
        query = ""
        #first = True
        step = 32
        option = []
        for i in range(0,len(variant_id_table_id_set),step):
            e = variant_id_table_id_set.pop()
            option = e 
            query = """(((variant_id == {0}) & (variant_table_id == {1}))""".format(e[0], e[1])
            for j in range(step-2):
                e = variant_id_table_id_set.pop()
                option += e
                query = query + """ | ((variant_id == {0}) & (variant_table_id == {1}))""".format(e[0], e[1])
            query += ")"
            #print(query)
            count_annotation_matches = count_annotation_matches + len([annotations_row['variant_id'] for annotations_row in annotations_table.where(query, option)])  
        #break"""
        
        #query = """((variant_id, variant_table_id) in {0})""".format(variant_id_table_id_set)#', '.join(map(str, variant_id_table_id_set)))

        
        """query = ""
        first = True
        for e in variant_id_table_id_set:
            if first:
                query = """#((variant_id == {0}) & (variant_table_id == {1}))""".format(e[0], e[1])
        """    else:
                query = query + """#| ((variant_id == {0}) & (variant_table_id == {1}))""".format(e[0], e[1])
        #first = False
            #print(e[0])
            #break"""
        
        #query = """((variant_id, variant_table_id) in {0})""".format(variant_id_table_id_set)#', '.join(map(str, variant_id_table_id_set)))


        #query = """((variant_id, variant_table_id) == (entry[x],entry[y]) for entry[x,y] in {0})""".format(variant_id_table_id_set)#', '.join(map(str, variant_id_table_id_set)))

        #query = """(((variant_id == entry[x]) & (variant_table_id == entry[y])) for entry[x,y] in {0})""".format(', '.join(map(str, variant_id_table_id_set)))
        #query = """((variant_id, variant_table_id) in {0})""".format(', '.join(map(str, variant_id_table_id_set)))
        #print(query)
        #count_annotation_matches = count_annotation_matches + len( [ annotations_row['variant_id'] for annotations_row in annotations_table.where(query)] )
    #print(count_annotation_matches)
    else:
        #print('here')
        count_annotation_matches = len([annotations_row['TDCcount'] for annotations_row in annotations_table.iterrows() if (annotations_row['variant_id'],['variant_table_id']) in enumerate(variant_id_table_id_set)])
         
        #for annotations_row in annotations_table:
            #    if ((annotations_row['variant_id'],['variant_table_id']) in enumerate(variant_id_table_id_set)):
                #print(annotations_row[:])
                #count_annotation_matches = count_annotation_matches + 1
                                                                   
                                                                   
    """
    for (searched_variant_id, variant_table_id) in variant_id_table_id_set:
        if is_search_in_kernel:
            query = """#((variant_id == {0}) & (variant_table_id == {1}))""".format(searched_variant_id, variant_table_id)
    """#print(query)
            count_annotation_matches = count_annotation_matches + len([annotations_row['variant_id'] for annotations_row in annotations_table.where(query)])
            #print(count_annotation_matches)
        else:
            for annotations_row in annotations_table:
                if (annotations_row['variant_id'] == searched_variant_id) and (annotations_row['variant_table_id'] == variant_table_id):
                    #print(annotations_row[:])
                    count_annotation_matches = count_annotation_matches + 1"""
    return count_annotation_matches



#@profile_method
def get_annotation_count_list(annotations_table, variant_id_table_id_list, is_search_in_kernel = True, is_count = True):
    """ get all annotations of variant_id and variant_table_id approximatly most efficient or get the count of it """
    #import sys
    #sys.setrecursionlimit(1000000000)
    #option = variant_id_table_id_set
    
    # TODO-sort?? nach varianten id oder nach table id
    #variant_id_table_id_list = sorted(variant_id_table_id_list,key=itemgetter(1))
    
    annotation_row = annotations_table.row
    count_annotation_matches = 0
    passvalues_annotations = []
    #is_search_in_kernel = False
    start = time.time()
    
    if is_search_in_kernel:
        #print('inkernel')
        query = ""
        #first = True
        step = 32
        option = []
        for i in range(0,len(variant_id_table_id_list),step):
            e = variant_id_table_id_list[i]
            option = e 
            query = """(((variant_id == {0}) & (variant_table_id == {1}))""".format(e[0], e[1])
            for j in range(step-2):
                e = variant_id_table_id_list.pop()
                option += e
                query = query + """ | ((variant_id == {0}) & (variant_table_id == {1}))""".format(e[0], e[1])
            query += ")"
            #print(query)
            if is_count:
                count_annotation_matches += len([annotations_row['variant_id'] for annotations_row in annotations_table.where(query, option)])
            else:
                #passvalues_annotations += [annotations_row.fetch_all_fields() for annotations_row in annotations_table.where(query, option)]
                passvalues_annotations += [annotations_row[:] for annotations_row in annotations_table.where(query, option)]
                print('Durchgang Annotation', time.time()-start)
        if is_count:
            return count_annotation_matches
        else:
            return passvalues_annotations
    else:
        #print('here')
        if is_count:
            count_annotation_matches = len([annotations_row['TDCcount'] for annotations_row in annotations_table.iterrows() if (annotations_row['variant_id'],['variant_table_id']) in enumerate(variant_id_table_id_set)])
            return count_annotation_matches
        else:
            passvalues_annotations = [annotations_row[:] for annotations_row in annotations_table.iterrows() if (annotations_row['variant_id'],['variant_table_id']) in enumerate(variant_id_table_id_set)]
            return passvalues_annotations


 
   
def main(hdf5_file):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file))    
    if(args.enableprofiling):
        disable_profiling(profiling_object)

