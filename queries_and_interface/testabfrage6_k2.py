from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np


__author__ = "Isabell Kullack"


#@profile_method
def get_join_variants_calls_samples_entries_by_variant_chrom_and_pos(calls_table, samples_table, variants_snp_table, variants_nosnp_table, is_search_in_kernel = True, searched_chrom = '01', pos = 142637034):
    """ 
    select * from variants v join calls c on c.variant_id=v.id join samples s on s.id=c.sample_id where v.chrom='1' and pos = 6204143; 
    
    search variantsSNP and variantsNoSNP
    
    Größe der Tabelle als Array?
    7Spalten Varianten +
    """
    
    # get variant entry of for snps
    passvalues_variants = []
    passvalues_variants = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_snp_id', is_search_in_kernel, searched_chrom , [pos,pos])

    #print(passvalues_variants)
    #print(len(passvalues_variants))
    
    first_len = len(passvalues_variants)
    
    #passvalues_nosnp_variants = get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_nosnp_id',  is_search_in_kernel, searched_chrom , [pos,pos])
    
    # get variant entries for nosnps
    passvalues_variants = passvalues_variants + get_variant_entry_by_selected_chrom_and_pos_range(variants_snp_table, variants_nosnp_table, 'variant_nosnp_id',  is_search_in_kernel, searched_chrom , [pos,pos])
    
    
    #print(passvalues_nosnp_variants)
    #print(len(passvalues_nosnp_variants))
    #output_len = len(passvalues_snp_variants) + len(passvalues_nosnp_variants)
    #output_len = len(passvalues_variants)

    #print(output)
    variant_table_id = 0
    passvalues_calls = []
    for counter, variant in enumerate(passvalues_variants):
        #print(counter)
        if counter >= first_len:
            variant_table_id = 1
        #print(variant)
        
        temp_calls = get_call_entry_by_id(calls_table, variant_table_id, is_search_in_kernel=is_search_in_kernel, searched_id=variant[0]) # list(variant) +
        #print(temp_calls, 'temp calls') 
        #print(temp_calls[0], 'sample_id?') 
        
        if temp_calls is not None:
            passvalues_calls = passvalues_calls + temp_calls
    #print(passvalues_calls)
    
    output = dict()
    
    counter = 0
    for call in passvalues_calls:
        #print(call, 'call')
        sample = get_sample_entry_by_id(samples_table, is_search_in_kernel, call[0])[0][1:]
        #print(sample, 'sample')
        
        for variant in passvalues_variants:
            #print(variant, 'variant' )
            if output:
                output.update({counter: {'variant': variant, 'call': call, 'sample': sample}})
            else:
                output = dict({counter: {'variant': variant, 'call': call, 'sample': sample}})
            counter += 1
                
    #print(output)
    return output

   
def main(hdf5_file, test_query_number, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    get_join_variants_calls_samples_entries_by_variant_chrom_and_pos(calls_table, samples_table, variants_snp_table, variants_nosnp_table, is_search_in_kernel)
    

#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

