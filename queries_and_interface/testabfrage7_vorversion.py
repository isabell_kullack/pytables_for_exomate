from tables import *
from filehandler import *
from profiling import *
import time
import sys
import argparse
from interface import *
import numpy as np


__author__ = "Isabell Kullack"



@profile_method
def get_annotations_from_samples_by_calls_qual__in_kernel(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, qual_low_limit = None, samples_searched = ['RB_E_021', 'RB_E_032']):
    """
    prints entries of calls, samples, variants and annotations joined
    """
    # print('number of rows in table', annotations_table._v_attrs.NROWS)
    start = time.time()
    
    annotations_row = annotations_table.row
    calls_row = calls_table.row
    samples_row = samples_table.row
    variants_snp_row = variants_snp_table.row
    
    passvalues_annotations = set()
    #passvalues_calls = set()
    passvalues_calls = []
    passvalues_sample = set()
    
    if qual_low_limit:
        qual_low_limit = float(qual_low_limit)
    
    
    # get sample id by asseccion    
    passvalues_sample = get_sample_id_from_list(samples_table, samples_searched)
    # get variant ids by sample id from call 
    #passvalues_calls = get_variant_id_table_id_from_sample_id_set(calls_table, passvalues_sample, qual_low_limit)
    passvalues_calls = get_variant_id_table_id_from_sample_id_list(calls_table, passvalues_sample, qual_low_limit)
    
    print('1. Teil',time.time() - start)
    print('Anzahl gefundene Calls', len(passvalues_calls))
    
    start = time.time()
    # TODO variants NO SNP table?
    for counter,(variant_id, variant_table_id) in enumerate(passvalues_calls):
        # round brackets inside required
        query = """((variant_id == {0}) & (variant_table_id == {1}))""".format(variant_id, variant_table_id) 
        #print(query)
        
        passvalues_annotations.update({(annotations_row[:]) for annotations_row in annotations_table.where(query)} )
        #print('gefundene annotations', passvalues_annotations)    
        #if counter % 100 == 0:
        print('durchlauf',time.time() - start)
    print('gefundene annotations', passvalues_annotations)
    return passvalues_annotations



@profile_method
def get_annotations_from_samples_by_calls_qual__iterrows(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, qual_low_limit = None, samples_searched  = ['RB_E_021', 'RB_E_032']):
    """
    prints entries of calls, samples, variants and annotations joined
    """
    # print('number of rows in table', annotations_table._v_attrs.NROWS)
    annotations_row = annotations_table.row
    calls_row = calls_table.row
    samples_row = samples_table.row
    variants_snp_row = variants_snp_table.row

    passvalues_annotations = set()
    passvalues_calls = set()
    passvalues_calls = []
    passvalues_sample = set()
    
    #qual_low_limit = 50
    if qual_low_limit:
        qual_low_limit = float(qual_low_limit)
    start = time.time()
    
    for k in samples_searched:
        passvalues_sample.update({samples_row['sample_id'] for samples_row in samples_table.iterrows() if samples_row['accession'] == k.encode()})
        
    #passvalues_sample.update([samples_row for samples_row in samples_table.iterrows() if samples_row['accession'] in {'RB_E_021_B'.encode(), 'RB_E_032_T'.encode()} ])
    print("samples", passvalues_sample)
    print('Samples Gesamtabfragezeit', time.time() - start)
    
    
    start = time.time()

    #passvalues_calls = [calls_row for calls_row in calls_table if calls_row['sample_id'] in passvalues_sample[]['sample_id']]
    for sample_id in passvalues_sample:
        
        #passvalues = [calls_row for calls_row in calls_table if calls_row['sample_id'] == i['sample_id'] and calls_row['qual'] >= 50]
        
        if qual_low_limit is None:
            #passvalues_calls.update({(calls_row['variant_id'], calls_row['variant_table_id']) for calls_row in calls_table.iterrows() if calls_row['sample_id'] == sample_id})
            passvalues_calls += [(calls_row['variant_id'], calls_row['variant_table_id']) for calls_row in calls_table.iterrows() if calls_row['sample_id'] == sample_id]
        else: 
            #passvalues_calls.update({(calls_row['variant_id'], calls_row['variant_table_id']) for calls_row in calls_table.iterrows() if calls_row['sample_id'] == sample_id and calls_row['qual'] >= qual_low_limit})
            passvalues_calls += [(calls_row['variant_id'], calls_row['variant_table_id']) for calls_row in calls_table.iterrows() if calls_row['sample_id'] == sample_id >= qual_low_limit]
        

        print('Durchlauf call',time.time() - start)
        #passvalues_calls = [calls_row['variant_id'] for calls_row in calls_table if calls_row['sample_id'] == i['sample_id']]
        #print(passvalues_calls)
        print('Anzahl gefundene Calls', len(passvalues_calls))
    
    #print("calls", passvalues_calls)
    print('end calls abfrage',time.time() - start)
    start = time.time()
    
    #TODO variant no snp table!
    
    # ALL IN ONE
    """passvalues_1 = { (calls_row[:], annotations_row[:]) for calls_row in calls_table.iterrows() for samples_row in samples_table.iterrows() for variants_snp_row in variants_snp_table.iterrows() for annotations_row in annotations_table.iterrows() if calls_row['sample_id'] == samples_row['sample_id'] and samples_row['accession'] in {'RB_E_015_B', 'RB_E_011_T'} and calls_row['variant_id'] == variants_row['variant_id'] and variants_row['variant_id'] == annotations_row['variant_id'] and variants_row['variant_table_id'] == annotations_row['variant_table_id'] }
    print('test abfrage',time.time() - start)
    print(passvalues_1)"""
    
    
    for (variant_id, variant_table_id) in passvalues_calls:
    
        passvalues_annotations.update({ annotations_row[:] for annotations_row in annotations_table.iterrows() if annotations_row['variant_id'] == variant_id and annotations_row['variant_table_id'] == variant_table_id })
        #print('variant id: ', variant_id)
        
        #passvalues_annotations.update({(annotations_row['variant_id'] == variant_id, annotations_row['variant_table_id'] == variant_table_id) for annotations_row in annotations_table.iterrows()} ) # works, slow, but gives boolean array
        print('durchlauf',time.time() - start)
        #print('gefundene annotations', passvalues_annotations)
        
    print(time.time() - start)
    
    
    #passvalues_annotations = [ annotations_row for annotations_row in
                   #annotations_table if annotations_row['variant_id'] == 0 ]
    
    #print(passvalues[0]['allele'].decode())
    
    #for i in annotation_cols:
    #    print(passvalues[0][i])
    
    return passvalues_annotations


   
def main(hdf5_file, test_query_number, is_search_in_kernel = True):
#    try:
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    #    select_data_by_iterating(hdf5_file_obj)
    #    select_annotations_in_kernel(hdf5_file_obj)
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    #if test_query_number == 7:
    if is_search_in_kernel:
        output = get_annotations_from_samples_by_calls_qual__in_kernel(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table)
    else:
        output = get_annotations_from_samples_by_calls_qual__iterrows(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table)
    #if test_query_number == 8:
    #    output = get_annotations_from_samples_by_calls_qual__in_kernel__to_array(calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table, is_search_in_kernel)


#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    parser.add_argument("-nr", "--testnr", type=int, help="test query number")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")

    
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.testnr, args.issearchnotinkernel))
    if(args.enableprofiling):
        disable_profiling(profiling_object)

