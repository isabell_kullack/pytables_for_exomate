from tables import *
from filehandler import *
from profiling import *
from datastructures import *
import time
import sys
import argparse
from interface import get_prepare


__author__ = "Isabell Kullack"

COLUMN_NAMES = dict({ 0: 'variant_id', 1: 'chrom', 2: 'pos', 3: 'ref', 4: 'alt', 5: 'is_transition', 6: 'is_transversion'})

#@profile_method
def get_variant_from_params_dict(variants_snp_table, variants_nosnp_table, searched_variant_params, is_search_in_kernel = True, output_range = None, outsingle = None, searched_compare = None):
    """ funtional interface of variants tables which creates querys of dictionary searched_variant_params and creates outsingle for each given column by number or as range of columns by output_range """

    variant_table_reverse = dict( {
                                 0 : variants_snp_table, 
                                 1 : variants_nosnp_table}
                                 )
    passvalues_variants = []
    
    if searched_compare:
        is_search_in_kernel = True
    
    if is_search_in_kernel:
        for temp_table in variant_table_reverse.values():
            query = """("""
            for key, value in searched_variant_params.items():
                if value is None:
                    continue
                if isinstance(value, str):
                    value = value.encode()
                #print(key, value)
                
                if searched_compare is not None:
                    compare = "=="
                    try:
                        if searched_compare[key]:
                            compare = searched_compare[key]
                    except KeyError:
                        compare = "=="
                    query += """({0} {1} {2}) & """.format(key, compare, value) 
                else:
                    query += """({0} == {1}) & """.format(key, value) 
            
            query += """ True )"""
            temp_table_row = temp_table.row
            
            if outsingle:
                passvalues_variants += [[temp_table_row[COLUMN_NAMES[single_nr]] for single_nr in outsingle] for temp_table_row in temp_table.where(query)]

            elif output_range:
                # TODO if nosnps searched max 5!
                # edit min column count if change!
                passvalues_variants += [(temp_table_row[max(0,output_range[0]):min(output_range[1],7)]) for temp_table_row in temp_table.where(query)]

            else:
                passvalues_variants += [(temp_table_row[:]) for temp_table_row in temp_table.where(query)]
        print(query)
    else:
        for temp_table in variant_table_reverse.values():
            temp_table_row = temp_table.row
            passvalues_variants += [(temp_table_row[:]) for temp_table_row in temp_table.iterrows() 
                                    if all( 
                                        ((searched_variant_params[x] is None)  or 
                                        (isinstance(searched_variant_params[x], int) and temp_table_row[x] == searched_variant_params[x]) or 
                                        #(isinstance(searched_sample_params[x], float) and samples_table_row[x] == searched_sample_params[x]) or 
                                        (isinstance(searched_variant_params[x], str) and temp_table_row[x] == searched_variant_params[x].encode()))
                                         for x in searched_variant_params )]
    #print('Gefundene Varianten: ', passvalues_variants)
    #print('Anzahl', len(passvalues_variants))
    return passvalues_variants



def main(hdf5_file, is_search_in_kernel = True, **kwargs):
#    try:
    print(is_search_in_kernel)
    
    hdf5_file_obj = open_in_mode(hdf5_file, 'r')
    calls_table, samples_table, variants_snp_table, variants_nosnp_table, annotations_table = get_prepare(hdf5_file_obj)
    
    get_variant_from_params_dict(variants_snp_table, variants_nosnp_table, kwargs, is_search_in_kernel)
    
#    except:
#        pass
#    finally:
    hdf5_file_obj.close()
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("-vid", "--variantid", type=int, help="variant id searched")
    parser.add_argument("-chrom", "--chrom", help="variant chrom searched")
    parser.add_argument("-pos", "--pos", type=int, help="variant position searched")
    parser.add_argument("-ref", "--ref", help="variant ref searched")
    parser.add_argument("-alt", "--alt", help="variant alt searched")
    parser.add_argument("-transition", "--istransition", type=int, choices=(0,1), help="int if searched variant is transition")
    parser.add_argument("-transversion", "--istransversion", type=int, choices=(0,1), help="int if searched variant is transversion")
    parser.add_argument("-nk", "--issearchnotinkernel", action="store_false",
                        help="boolean if search not is in kernel")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    
    print(args.issearchnotinkernel)
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.issearchnotinkernel, variant_id = args.variantid, chrom = args.chrom, pos = args.pos, ref = args.ref, alt = args.alt, is_transition = args.istransition, is_transversion = args.istransversion))    
    if(args.enableprofiling):
        disable_profiling(profiling_object)

