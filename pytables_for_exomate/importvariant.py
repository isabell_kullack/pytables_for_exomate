from tables import *
from filehandler import *
import sys

__author__ = "Isabell Kullack"

class ImportVariant():
    def __init__(self, hdf5_file, chrom, pos, ref, alt, is_transition, is_transversion, 
                 variant_id, variants_table, is_snp_table, is_variant_check = True):
        self.__hdf5_file = hdf5_file
        #self.__annotations_table_id = annotations_table_id
        self.__chrom = chrom
        self.__pos = pos
        self.__ref = ref
        self.__alt = alt
        self.__is_transition = is_transition
        self.__is_transversion = is_transversion
        self.variant_id = variant_id
        self.variants_table = variants_table
        self.variants_row = self.variants_table.row
        self.is_snp_table = is_snp_table
        
        self.validate_insertions()
        #self.is_variant_check = is_variant_check
        self.is_new_entry = True
        if is_variant_check:
            #print("data check variants", is_variant_check)
            self.is_new_entry = self.is_insertion_a_new_entry_in_table()
        if self.is_new_entry:
            self.insert_new_variant()
        else:
            pass

    def validate_insertions(self):
        self.__chrom = str(self.__chrom)
        self.__pos = replace_to_int(self.__pos)
        self.__ref = str(self.__ref)
        self.__alt = str(self.__alt)
        if self.is_snp_table:
            self.__is_transition = replace_true_false_to_int(self.__is_transition)
            self.__is_transversion = replace_true_false_to_int(self.__is_transversion)
        
        
    def is_insertion_a_new_entry_in_table(self):
        """checks if new variant is in variant table and sets variant_id in case"""
        query_condition = "(chrom == {0}) & (pos == {1}) & (ref == {2}) & (alt == {3})".format(self.__chrom.encode(), self.__pos, self.__ref.encode(), self.__alt.encode())
        #print(query_condition)
        passvalues = [self.variants_row['variant_id'] for self.variants_row in self.variants_table.where(query_condition)]
        if not passvalues:
            return True
        else:
            self.variant_id = passvalues[0]
            return False

    def insert_new_variant(self):
        """Insertion in pytables file"""
        self.variants_row['variant_id'] = self.variant_id
        #self.variants_row['annotations_table_id'] = self.__annotations_table_id
        self.variants_row['chrom'] = self.__chrom
        self.variants_row['pos'] = self.__pos
        self.variants_row['ref'] = self.__ref
        self.variants_row['alt'] = self.__alt
        if self.is_snp_table:
            self.variants_row['is_transition'] = self.__is_transition
            self.variants_row['is_transversion'] = self.__is_transversion
        # Insert a new variants record
        self.variants_row.append()
        
        
        
def import_variants_from_csv(hdf5_file, csv_file, variants_table):
    """Variants from csv_file will be imported in a pytables hdf5_file."""
    variant_id = len(variants_table)
    is_new_entry = False
    is_variant_check = False
    import csv
    csv.register_dialect('unixpwd', delimiter=';', quoting=csv.QUOTE_NONE)
    with open(csv_file, newline='', encoding='utf-8') as csvfile:
        csv_printer = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in csv_printer:
            try:
                #row[0] is id from export table
                import_variant = ImportVariant(hdf5_file, row[1], row[2], row[3], row[4], row[5], 
                                               row[6], variant_id, variants_table, 
                                               is_variant_check)
                if import_variant.is_new_entry:
                    is_new_entry = True
                    variant_id = variant_id + 1
                    if (variant_id == 10):
                        variants_table.flush()
                        variant_id = len(variants_table)
            except TypeError as e:
                sys.exit('file {}, line {}: TypeError: {}'.format(csv_file+'.csv', 
                                                                  csv_printer.line_num, e))
        if is_new_entry:
            variants_table.flush()
