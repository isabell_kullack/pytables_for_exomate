from tables import *
from filehandler import *
import sys

__author__ = "Isabell Kullack"

class ImportCall():
    def __init__(self, hdf5_file, variant_table_id, qual, is_heterozygous, read_depth, ref_depth, 
                 alt_depth, strand_bias, qual_by_depth, mapping_qual, haplotype_score, 
                 mapping_qual_bias, read_pos_bias, calls_table, sample_id=-999999999999999999, 
                 variant_id=-999999999999999999, is_call_check = True, call_id=-999999999999999999):
        self.__hdf5_file = hdf5_file
        self.__call_id = call_id
        self.__sample_id = sample_id
        self.__variant_id = variant_id
        self.__variant_table_id = variant_table_id
        self.__qual = qual
        self.__is_heterozygous = is_heterozygous
        self.__read_depth = read_depth
        self.__ref_depth = ref_depth
        self.__alt_depth = alt_depth
        self.__strand_bias = strand_bias
        self.__qual_by_depth = qual_by_depth
        self.__mapping_qual = mapping_qual
        self.__haplotype_score = haplotype_score
        self.__mapping_qual_bias = mapping_qual_bias
        self.__read_pos_bias = read_pos_bias
        self.validate_insertions()
        self.calls_table = calls_table
        self.calls_row = self.calls_table.row
        self.is_new_entry = True
        if is_call_check: 
            #print("data check calls")
            self.is_new_entry  = self.is_insertion_a_new_entry_in_table()
        if ((self.__sample_id == -999999999999999999) & (self.__variant_id == -999999999999999999)):
            print("Keine Sample ID und keine Variant ID gegeben. Kein Eintrag erfolgt.")
        elif self.is_new_entry:
            self.insert_new_call()
        else:
            pass


    def validate_insertions(self):
        self.__call_id = replace_to_int(self.__call_id)
        self.__sample_id = replace_to_int(self.__sample_id)
        self.__variant_id = replace_to_int(self.__variant_id)
        self.__variant_table_id = replace_to_int(self.__variant_table_id)
        self.__qual = replace_to_float(self.__qual)
        self.__is_heterozygous = replace_true_false_to_int(self.__is_heterozygous)
        self.__read_depth = replace_to_int(self.__read_depth)
        self.__ref_depth = replace_to_int(self.__ref_depth)
        self.__alt_depth = replace_to_int(self.__alt_depth)
        self.__strand_bias = replace_to_float(self.__strand_bias)
        self.__qual_by_depth = replace_to_float(self.__qual_by_depth)
        self.__mapping_qual = replace_to_float(self.__mapping_qual)
        self.__haplotype_score = replace_to_float(self.__haplotype_score)
        self.__mapping_qual_bias = replace_to_float(self.__mapping_qual_bias)
        self.__read_pos_bias = replace_to_float(self.__read_pos_bias)


   
    def is_insertion_a_new_entry_in_table(self):
        query_condition = "(sample_id == {0}) & (variant_id == {1}) & (variant_table_id == {2})".format(
                           self.__sample_id, self.__variant_id, self.__variant_table_id)
        passvalues = [[self.calls_row['sample_id'], self.calls_row['variant_id'], self.calls_row['variant_table_id']] for 
                       self.calls_row in self.calls_table.where(query_condition)] #call_id y/n TODO?
        if not passvalues:
            return True
        else:
            #self.call_id = passvalues[0] # notimplemented call_id
            return False

    def insert_new_call(self):
        """Insertion in pytables file"""
        self.calls_row['call_id'] = self.__call_id
        self.calls_row['sample_id'] = self.__sample_id
        self.calls_row['variant_id'] = self.__variant_id
        self.calls_row['variant_table_id'] = self.__variant_table_id
        if self.__qual is not None:
            self.calls_row['qual'] = self.__qual
        if self.__is_heterozygous is not None:
            self.calls_row['is_heterozygous'] = self.__is_heterozygous
        if self.__read_depth is not None:
            self.calls_row['read_depth'] = self.__read_depth
        if self.__ref_depth is not None:
            self.calls_row['ref_depth'] = self.__ref_depth
        if self.__alt_depth is not None:
            self.calls_row['alt_depth'] = self.__alt_depth
        if self.__strand_bias is not None:
            self.calls_row['strand_bias'] = self.__strand_bias
        if self.__qual_by_depth is not None:
            self.calls_row['qual_by_depth'] = self.__qual_by_depth
        if self.__mapping_qual is not None:
            self.calls_row['mapping_qual'] = self.__mapping_qual
        if self.__haplotype_score is not None:
            self.calls_row['haplotype_score'] = self.__haplotype_score
        if self.__mapping_qual_bias is not None:
            self.calls_row['mapping_qual_bias'] = self.__mapping_qual_bias
        if self.__read_pos_bias is not None:
            self.calls_row['read_pos_bias'] = self.__read_pos_bias
        # Insert a new calls record
        self.calls_row.append()
        
        
def import_calls_from_csv(hdf5_file, csv_file, calls_table):
    """Calls from csv_file will be imported in a pytables hdf5_file."""
    import csv
    csv.register_dialect('unixpwd', delimiter=';', quoting=csv.QUOTE_NONE)
    with open(csv_file, newline='', encoding='utf-8') as csvfile:
        csv_printer = csv.reader(csvfile, delimiter=';', quotechar='"')
        is_new_entry = False
        for row in csv_printer:
            try:
                #row[0] is its id from export table
                import_call = ImportCall(hdf5_file, row[4], row[5], row[6], 
                                         row[7], row[8], row[9], row[10], row[11], row[12], 
                                         row[13], row[14], row[15], calls_table, row[1], row[2], row[3])
                if import_call.is_new_entry:
                    is_new_entry = True
            except TypeError as e:
                sys.exit('file {}, line {}: TypeError: {}'.format(csv_file+'.csv', 
                                                                  csv_printer.line_num, e))
        if is_new_entry:
            calls_table.flush()
        


