from tables import *
from filehandler import *
from profiling import *
import argparse
import sys

__author__ = "Isabell Kullack"

class VariantsNoSNP(IsDescription):
    variant_id = Int64Col(pos=0)
    chrom = StringCol(16, dflt='', pos=1) # Int8Col(dflt=-99, pos=1)
    pos = Int64Col(dflt=-999999999999999999, pos=2)
    ref = StringCol(64, dflt='', pos=3)
    alt = StringCol(64, dflt='', pos=4)
    #is_transition = UInt8Col(pos=5) # BoolCol(1, pos=6) # boolean
    #is_transversion= UInt8Col(pos=6) # BoolCol(pos=7) # boolean
    
variantsNoSNP_structure = {'name':'variantsNoSNP', 'class': VariantsNoSNP, 'group':'variants_group', 'group_title':'variants group', 'table_title': 'variantsNoSNP table', 'is_in_group':True}

    
def main(hdf5_file_obj, init, import_file):
    if(init):
        variants_nosnp_table = init_structure(hdf5_file_obj, variantsNoSNP_structure) 
    else:
        try:
            variants_nosnp_table = get_table_from_file(hdf5_file_obj, variantsNoSNP_structure['name'])
        except NoSuchNodeError:
            variants_nosnp_table = init_structure(hdf5_file_obj, variantsNoSNP_structure) 
    
    if(import_file):
        import_variants_nosnp_from_csv(hdf5_file_obj, import_file, variants_nosnp_table)   
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("--init", type=bool, default=False, choices=(True, False),
                        help="boolean to init variants_nosnp table -- Caution ALL VARIANT nosnps LOST! [default: False]")
    parser.add_argument("-i", "--importvariantsfromcsv", type=str,
                        help="csv file name or path containing variants_nosnp to be imported into variants_nosnp table")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on variants_nosnp tables operations enabled")
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    hdf5_file_obj = open_in_mode(args.hdf5file, 'a')
    sys.exit(main(hdf5_file_obj, args.init, args.importvariantsfromcsv))
    hdf5_file_obj.close()
    if(args.enableprofiling):
        disable_profiling(profiling_object)

