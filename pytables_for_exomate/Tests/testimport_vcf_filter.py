from import_vcf import *
#from import_vcf import main as importvcfmain
from tables import *
from nose.tools import *
from nose.plugins.skip import Skip, SkipTest


__author__ = "Isabell Kullack"


def test_import_vcf_filter():
    """test import filter option"""
    vcf_file = "Tests/test_vcf_three_line_filter.vcf"
    hdf5_file = "Tests/test_vcf_three_line_filter.h5"
    defined_filter = "q10"
    start_value = 0
    limit = 1
    checkoff = False
    is_vcf_in_snp_pytables = None
    existing_snp_variant_ids = None
    preprocessingoff = False

    # reset of hdf5 file
    h5_file_reset = open(hdf5_file, 'w')
    h5_file_reset.close()
    
    with open_file(hdf5_file, mode = "w") as searched_file:
    # first variant with q10, third s50
        # try to insert only 2 rows but skip first
        import_values_from_vcf(searched_file, vcf_file, defined_filter, start_value, limit, 
                               checkoff, is_vcf_in_snp_pytables, existing_snp_variant_ids)

        retrieved_snp_table = searched_file.get_node('/variants_group', "variantsSNP")
        retrieved_nosnp_table = searched_file.get_node('/variants_group', "variantsNoSNP")
        
        snp_values = [variants_row['variant_id'] for variants_row in 
                      retrieved_snp_table.where("""(pos == 14930)""")]
        assert_equal(snp_values, [])
        
        nosnp_values = [variants_row['variant_id'] for variants_row in 
                         retrieved_nosnp_table.where("""(pos == 14930)""")]
        assert_equal(nosnp_values, [])
        
        # insert all entries except third with s50
        limit = 2
        defined_filter = "s50" 
        import_values_from_vcf(searched_file, vcf_file, defined_filter, start_value, limit, 
                               checkoff, is_vcf_in_snp_pytables, existing_snp_variant_ids)
        # test for q10 variant_id
        snp_values = [variants_row['variant_id'] for variants_row in 
                      retrieved_snp_table.where("""(pos == 14930)""")]
        assert_equal(snp_values, [1])
        
        nosnp_values = [variants_row['variant_id'] for variants_row in 
                        retrieved_nosnp_table.where("""(pos == 14930)""")]
        assert_equal(nosnp_values, [])
        
        # test for s50 variant_id
        snp_values = [variants_row['variant_id'] for variants_row in 
                      retrieved_snp_table.where("""(pos == 20144)""")]
        assert_equal(snp_values, [])
        
        nosnp_values = [variants_row['variant_id'] for variants_row in 
                        retrieved_nosnp_table.where("""(pos == 20144)""")]
        assert_equal(nosnp_values, [])
        


