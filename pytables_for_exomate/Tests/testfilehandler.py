from filehandler import *
from tables import *
from nose.tools import *

__author__ = "Isabell Kullack"

def test_get_table_from_file():
    """table object for variants is retrieved equal"""
    with open_file("Tests/test_generate_import.h5", mode = "r", title = "exomate_1") as searched_file:
        expected_variants_table_object = searched_file.root.variants_group.variantsSNP
        assert_equals(expected_variants_table_object,get_table_from_file(searched_file,"variantsSNP"))

        

