from import_vcf import *
from tables import *
from nose.tools import *
from nose.plugins.skip import Skip, SkipTest
from pytables_for_exomate import main as testmain
from sort_vcf import sort_vcf


__author__ = "Isabell Kullack"


def test_preprocessing():
    """test preprocessing array values"""
    
    hdf5_file = "Tests/test_preprocessing.h5"
    # reset of hdf5 file
    h5_file_reset = open(hdf5_file, 'w')
    h5_file_reset.close()
    
    vcf_file = "Tests/test_vcf_one_line.vcf"
    defined_filter = None
    start = None
    limit = None
    checkoff = False
    is_vcf_in_snp_pytables = None
    existing_snp_variant_ids = None
    preprocessingoff = False
    
    init_all = False
    init_calls = False
    import_calls = None
    init_variants = False
    import_variants = None
    init_variants_nosnp = False
    import_variants_nosnp = None
    init_samples = False
    import_samples = None
    init_annotations = False
    import_annotations = None
    
    # first insert 1 row of first vcf because of test_vcf_one_line.vcf
    testmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
             init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
             init_annotations, import_annotations, vcf_file, defined_filter, start, limit, 
             checkoff, preprocessingoff)
        
    # needs to compare sorted vcf with leading zeros
    vcf_file = "Tests/test_vcf_two_line.vcf"
    sorted_vcf_file = sort_vcf(vcf_file)
                   
    # create intersection arrays   
    is_vcf_in_snp_pytables, is_vcf_in_nosnp_pytables, existing_snp_variants, existing_nosnp_variants, existing_snp_variants_in_vcf, existing_nosnp_variants_in_vcf, existing_snp_variant_ids, existing_nosnp_variant_ids, is_preprocessing_successful, vcf_file_new = vcf_data_insertion_check(hdf5_file, sorted_vcf_file)
    
    #print(is_vcf_in_snp_pytables)
    assert_equals(is_vcf_in_snp_pytables[0], True)
    assert_equals(is_vcf_in_snp_pytables[1], False)
    assert_equals(existing_snp_variants_in_vcf[0][0], '01'.encode())
    assert_equals(existing_snp_variants_in_vcf[0][1], 14930)
    assert_equals(existing_snp_variants_in_vcf[0][2],'A'.encode())
    assert_equals(existing_snp_variants_in_vcf[0][3],'G'.encode())
    """assert_equals(variants_not_in_pytables[0][0], '01'.encode())
    assert_equals(variants_not_in_pytables[0][1], 15211)
    assert_equals(variants_not_in_pytables[0][2],'T'.encode())
    assert_equals(variants_not_in_pytables[0][3],'G'.encode())"""
    assert_equals(existing_snp_variant_ids[0], 0)
        
    # insert third entry
    start = 2
    limit = 2
    is_vcf_in_snp_pytables = None
    existing_snp_variant_ids = None
    vcf_file = "Tests/test_vcf_three_line.vcf"
    
    #insert only third value
    testmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
             init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
             init_annotations, import_annotations, vcf_file, defined_filter, start, limit, 
             checkoff, preprocessingoff)
    
    # needs to compare sorted vcf with leading zeros
    vcf_file = "Tests/test_vcf_three_line.vcf"
    sorted_vcf_file = sort_vcf(vcf_file)
                
    # create intersection arrays          
    is_vcf_in_snp_pytables, is_vcf_in_nosnp_pytables, existing_snp_variants, existing_nosnp_variants, existing_snp_variants_in_vcf, existing_nosnp_variants_in_vcf, existing_snp_variant_ids, existing_nosnp_variant_ids, is_preprocessing_successful, vcf_file_new = vcf_data_insertion_check(hdf5_file, sorted_vcf_file)
    
    assert_equals(is_vcf_in_snp_pytables.shape[0], 3)
    assert_equals(is_vcf_in_snp_pytables[0], True)
    assert_equals(is_vcf_in_snp_pytables[1], False)
    assert_equals(is_vcf_in_snp_pytables[2], True)
    assert_equals(existing_snp_variants_in_vcf[0][0], '01'.encode())
    assert_equals(existing_snp_variants_in_vcf[0][1], 14930)
    assert_equals(existing_snp_variants_in_vcf[0][2],'A'.encode())
    assert_equals(existing_snp_variants_in_vcf[0][3],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[1][0], '01'.encode())
    assert_equals(existing_snp_variants_in_vcf[1][1], 20144)
    assert_equals(existing_snp_variants_in_vcf[1][2],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[1][3],'A'.encode())
    """assert_equals(variants_not_in_pytables[0][0], '01'.encode())
    assert_equals(variants_not_in_pytables[0][1], 15211)
    assert_equals(variants_not_in_pytables[0][2],'T'.encode())
    assert_equals(variants_not_in_pytables[0][3],'G'.encode())"""
    assert_equals(existing_snp_variant_ids[0], 0)
    assert_equals(existing_snp_variant_ids[1], 1)


def test_preprocess_in_import_vcf():
    """test preprocessing values of imported vcf file"""
    #test preprocessing before and vcf_test_two_row.h5 is prepared with first line inserted
    hdf5_file = "Tests/test_preprocessing.h5"
    defined_filter = None
    start_value = None
    limit = None
    checkoff = False
    is_vcf_in_snp_pytables = None
    existing_snp_variant_ids = None
    preprocessingoff = False

    # shall insert all 3 variants but has inserted first and third one
    vcf_file = "Tests/test_vcf_three_line.vcf"
    
    main(hdf5_file, vcf_file, defined_filter, start_value, limit, checkoff, 
                   preprocessingoff)
                           
    with open_file(hdf5_file, mode = "a") as searched_file:
        retrieved_table = searched_file.get_node('/variants_group', "variantsSNP")
        #print(retrieved_table.shape)
        for variants_row in retrieved_table:
            if (variants_row['pos'] == 15211):
                assert_equals(variants_row['variant_id'], 2)
                assert_equals(variants_row['chrom'], '01'.encode())
                assert_equals(variants_row['ref'], 'T'.encode())
                assert_equals(variants_row['alt'], 'G'.encode())
                assert_equals(variants_row['is_known'], 0)
                assert_equals(variants_row['is_transition'], 0)
                assert_equals(variants_row['is_transversion'], 1)
                
        # test if ids and values of last entry (second one only missing) are correct
        retrieved_table = searched_file.get_node('/', "calls")
        for calls_row in retrieved_table:
            if (calls_row['variant_id'] == 2):
                assert_equals(calls_row['sample_id'], 0)
                assert_equals(calls_row['qual'], 76.35)
                assert_equals(calls_row['mapping_qual'], 15.29)
                
    # insert sort entries
    vcf_file = "Tests/test_vcf_sort.vcf"
    init_all = False
    init_calls = False
    import_calls = None
    init_variants = False
    import_variants = None
    init_variants_nosnp = False
    import_variants_nosnp = None
    init_samples = False
    import_samples = None
    init_annotations = False
    import_annotations = None
    
    # insert first three entries of vcf
    start = 0
    limit = 2
    
    testmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
             init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
             init_annotations, import_annotations, vcf_file, defined_filter, start, limit, 
             checkoff, preprocessingoff)
    
    
    # insert last four entries of vcf - one entry left
    start = 5
    limit = 8
    
    testmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
             init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
             init_annotations, import_annotations, vcf_file, defined_filter, start, limit, 
             checkoff, preprocessingoff)
    
    start = None
    limit = None
    
    # insert missing entry
    testmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
             init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
             init_annotations, import_annotations, vcf_file, defined_filter, start, limit, 
             checkoff, preprocessingoff)
    
    # needs to compare sorted vcf with leading zeros
    vcf_file = "Tests/test_vcf_sort.vcf"
    sorted_vcf_file = sort_vcf(vcf_file)
                   
    # create intersection arrays   
    is_vcf_in_snp_pytables, is_vcf_in_nosnp_pytables, existing_snp_variants, existing_nosnp_variants, existing_snp_variants_in_vcf, existing_nosnp_variants_in_vcf, existing_snp_variant_ids, existing_nosnp_variant_ids, is_preprocessing_successful, vcf_file_new = vcf_data_insertion_check(hdf5_file, sorted_vcf_file)
        
    print(existing_snp_variants_in_vcf)
    
    assert_equals(is_vcf_in_snp_pytables[0], True)
    assert_equals(is_vcf_in_snp_pytables[1], True)
    assert_equals(existing_snp_variants_in_vcf.shape[0], 7)
    assert_equals(existing_snp_variants_in_vcf[0][0], '01'.encode())
    assert_equals(existing_snp_variants_in_vcf[0][1], 14930)
    assert_equals(existing_snp_variants_in_vcf[0][2],'A'.encode())
    assert_equals(existing_snp_variants_in_vcf[0][3],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[1][0], '02'.encode())
    assert_equals(existing_snp_variants_in_vcf[1][1], 14930)
    assert_equals(existing_snp_variants_in_vcf[1][2],'A'.encode())
    assert_equals(existing_snp_variants_in_vcf[1][3],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[2][0], '11'.encode())
    assert_equals(existing_snp_variants_in_vcf[2][1], 14930)
    assert_equals(existing_snp_variants_in_vcf[2][2],'A'.encode())
    assert_equals(existing_snp_variants_in_vcf[2][3],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[3][0], 'X'.encode()) # X has id 23
    assert_equals(existing_snp_variants_in_vcf[3][1], 15211)
    assert_equals(existing_snp_variants_in_vcf[3][2],'A'.encode())
    assert_equals(existing_snp_variants_in_vcf[3][3],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[4][0], 'X'.encode())
    assert_equals(existing_snp_variants_in_vcf[4][1], 15211)
    assert_equals(existing_snp_variants_in_vcf[4][2],'T'.encode())
    assert_equals(existing_snp_variants_in_vcf[4][3],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[5][0], 'MT'.encode()) # MT has id 25
    assert_equals(existing_snp_variants_in_vcf[5][1], 20144)
    assert_equals(existing_snp_variants_in_vcf[5][2],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[5][3],'A'.encode())
    assert_equals(existing_snp_variants_in_vcf[6][0], 'GL000194.1'.encode()) # GL000194.1 has id 84
    assert_equals(existing_snp_variants_in_vcf[6][1], 20144)
    assert_equals(existing_snp_variants_in_vcf[6][2],'G'.encode())
    assert_equals(existing_snp_variants_in_vcf[6][3],'A'.encode())
    
    assert_equals(existing_snp_variant_ids.shape[0], 7)
    assert_equals(existing_snp_variant_ids[0], 0)
    assert_equals(existing_snp_variant_ids[1], 3)
    assert_equals(existing_snp_variant_ids[2], 4)
    # last entries are sorted in the middle
    assert_equals(existing_snp_variant_ids[3], 7) 
    assert_equals(existing_snp_variant_ids[4], 8)
    assert_equals(existing_snp_variant_ids[5], 5)
    assert_equals(existing_snp_variant_ids[6], 6)
    
    with open_file(hdf5_file, mode = "r") as searched_file:
    
        retrieved_calls_table = searched_file.get_node('/', "calls")
        
        # A	G 22.45	. 	AC=2;AF=1.00;AN=2;BaseQRankSum=-0.358;DB;DP=3;Dels=0.00;FS=0.000;
        #HaplotypeScore=0.0000;MQ=24.70;MQ0=1;MQRankSum=0.358;QD=3.74;ReadPosRankSum=-1.231
        calls_searched_0 = {'sample_id': 0, 'variant_id': 0, 'variant_table_id': 0, 
                            'qual': 22.1, 'is_heterozygous' : 0, 'read_depth':3, 'ref_depth': 1, 
                            'alt_depth': 2,  'strand_bias': 0.000, 'qual_by_depth' : 3.74, 
                            'mapping_qual' : 24.70, 'haplotype_score' :  0.0000,
                            'mapping_qual_bias' : 0.358, 'read_pos_bias' : -1.231}
       
        # test calls if id fit by qual created differently
        
                            
        calls_searched_1 = {'sample_id': 0, 'variant_id': 1, 'variant_table_id': 0, 
                            'qual': 25.52}

        calls_searched_2 = {'sample_id': 0, 'variant_id': 2, 'variant_table_id': 0, 
                            'qual': 76.35}

        calls_searched_3 = {'sample_id': 0, 'variant_id': 3, 'variant_table_id': 0, 
                            'qual': 22.2}

        calls_searched_4 = {'sample_id': 0, 'variant_id': 4, 'variant_table_id': 0, 
                            'qual': 22.11}
        
        calls_searched_5 = {'sample_id': 0, 'variant_id': 0, 'variant_table_id': 1, 
                            'qual': 6.35}
        
        calls_searched_6 = {'sample_id': 0, 'variant_id': 1, 'variant_table_id': 1, 
                            'qual': 76.5}

        calls_searched_7 = {'sample_id': 0, 'variant_id': 5, 'variant_table_id': 0, 
                            'qual': 25.52} # MT
                            
        calls_searched_8 = {'sample_id': 0, 'variant_id': 6, 'variant_table_id': 0, 
                            'qual': 225.52} # GL

        calls_searched_9 = {'sample_id': 0, 'variant_id': 7, 'variant_table_id': 0, 
                            'qual': 76.335}

        calls_searched_10 = {'sample_id': 0, 'variant_id': 8, 'variant_table_id': 0, 
                            'qual': 76.35}

                            

        
        counter_ref = {0: calls_searched_0, 1: calls_searched_1, 2: calls_searched_2, 
                       3: calls_searched_3, 4: calls_searched_4, 5: calls_searched_5, 
                       6: calls_searched_6, 7: calls_searched_7, 8: calls_searched_8, 
                       9: calls_searched_9, 10: calls_searched_10}
        counter = 0
        for variants_row in retrieved_calls_table:
            if counter in counter_ref:
                for k, v in counter_ref[counter].items():
                    #if isinstance(v, str):
                    #    v = v.encode()
                    print(k,v)
                    #print('ddddd') 
                    assert_equals(variants_row[k], v)
            counter = counter + 1
