from operator import itemgetter, attrgetter, methodcaller
from datastructures import *
import io
import random
from pytables_for_exomate import main as testmain
from tables import *
from nose.tools import *
from filehandler import remove_brackets_quotations
#from nose.plugins.skip import Skip, SkipTest
#from sort_vcf import sort_vcf


__author__ = "Isabell Kullack"

def set_leading_zeros(chrom):
    """set leading zeros to first chroms for sorting"""
    if len(chrom) == 1 and chrom != 'X' and chrom != 'Y':
        chrom = '0' + chrom
    return chrom



def test_generate_import_vcf():
    """test sorting and import of vcf file"""
    
    hdf5_file = "Tests/test_generate_import.h5"

    h5_file_reset = open(hdf5_file, 'w')
    h5_file_reset.close()
    
    
    #used for header
    vcf_file = "Tests/test_vcf_one_line.vcf"

    
    vcf_obj = open(vcf_file, 'r')
    
    vcf_file_new = 'Tests/test_generate_import.vcf'
    vcf_obj_new = open(vcf_file_new, 'w')
    
    vcf_lines = list()
    chrom = 0
    variant_searched = []
    
    sample_name = ''
    for line in vcf_obj:
        if(line[0] == '#'):
            if(line[1] == '#'):
                print(line, file=vcf_obj_new, end='')
            else:
                # #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	RB_E_032_T
                for i in range(30):
                    sample_name = sample_name + random.choice('RB_E032T1456789')
                sample_name_n = sample_name + '\n'
                print('#CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT', sample_name_n, sep='\t',file=vcf_obj_new, end='')
                    
        else:
            chrom, pos, vcf_id, ref, alt, qual, filters, info, formats, sample_values = line.split('\t')
            vcf_lines.append( (chrom, pos, vcf_id, ref, alt, qual, filters, info, formats, sample_values) )
            variant_searched.append( (chrom, pos, ref, alt, 1, 1, qual, False, 3, 0.000, 0.0000, 24.70, 0.358, 3.74, -1.231, 0,[], 1, 2))
        
    for j in range(random.randrange(1,101)):
        chrom_list = random.sample(['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', 'X', 'Y', 'MT', 'GL000207.1', 'GL000226.1', 'GL000229.1', 'GL000231.1', 'GL000210.1', 'GL000239.1', 'GL000235.1', 'GL000201.1', 'GL000247.1', 'GL000245.1', 'GL000197.1', 'GL000203.1', 'GL000246.1', 'GL000249.1', 'GL000196.1', 'GL000248.1', 'GL000244.1', 'GL000238.1', 'GL000202.1', 'GL000234.1', 'GL000232.1', 'GL000206.1', 'GL000240.1', 'GL000236.1', 'GL000241.1', 'GL000243.1', 'GL000242.1', 'GL000230.1', 'GL000237.1', 'GL000233.1', 'GL000204.1', 'GL000198.1', 'GL000208.1', 'GL000191.1', 'GL000227.1', 'GL000228.1', 'GL000214.1', 'GL000221.1', 'GL000209.1', 'GL000218.1', 'GL000220.1', 'GL000213.1', 'GL000211.1', 'GL000199.1', 'GL000217.1', 'GL000216.1', 'GL000215.1', 'GL000205.1', 'GL000219.1', 'GL000224.1', 'GL000223.1', 'GL000195.1', 'GL000212.1', 'GL000222.1', 'GL000200.1', 'GL000193.1', 'GL000194.1', 'GL000225.1', 'GL000192.1'],  1)
        chrom = chrom_list[0] # list entry
        pos = random.randrange(0, 1000000)
        ref = ''
        alt = ''
        
        # simulate 80% SNPs
        if random.randrange(1,6) == 5:
            ref_len = random.randrange(1,10)
        else:
            ref_len = 1
        if random.randrange(1,6) == 5:
            alt_len = random.randrange(1,10)
        else:
            alt_len = 1
        for i in range(ref_len):
            ref = ref + random.choice('ACGT')
        for i in range(alt_len):
            alt = alt + random.choice('ACGT')
            
        qual = random.uniform(0, 50)
        annotations = ''
        annotations_searched = []
        annotation_count = random.randrange(1,7)
        
        for k in range(annotation_count):
            allele = alt
            gene_name = ''
            gene_id = ''
            feature_id = ''
            hgvsc = ''
            hgvsp = ''
            annotation = ''
            annotation_list = []
            
            for l in range(random.randrange(1,8)):
                annotation_list.append( random.sample(['upstream_gene_variant', 'downstream_gene_variant', 'intron_variant', 'nc_transcript_variant', 'intergenic_region', 'synonymous_variant'], 1))
                if l == 0:
                    annotation = annotation_list[0][0] # remove_brackets_quotations(str(annotation_list[0]))
                else:
                    annotation = annotation + '&' + annotation_list[l][0] # remove_brackets_quotations(str(annotation_list[l]))
                
            #print(annotation)
            #print(annotation_list)
            
            impact = random.sample(['HIGH', 'MODERATE', 'LOW', 'MODIFIER'], 1)
            for i in range(random.randrange(1,13)):
                gene_name = gene_name + random.choice('MIR6859-1DXL1047C')
            for i in range(random.randrange(1,13)):
                gene_id = gene_id + random.choice('MIR6859-1DXL1047C')
            feature_type = random.sample(['transcript', 'motif', 'miRNA'], 1) # TODO rest possibilities
            for i in range(random.randrange(1,12)):
                feature_id = feature_id + random.choice('NR_456018.2')
            transcript_biotype = random.sample(['Coding', 'Noncoding'], 1)
            
            rank = random.randrange(1,100) # TODO 
            total = random.randrange(1,100) # TODO
            for i in range(random.randrange(1,10)):
                hgvsc = hgvsc + random.choice('n.*1652A>G8TC30+-c')
            if transcript_biotype == 'Coding':
                for i in range(random.randrange(1,12)):
                    hgvsp = hgvsp + random.choice('c.-81C>Gplu306')
            cdna_pos = random.randrange(1,1000)
            cdna_len = random.randrange(1,3000)
            cds_pos = random.randrange(1,1000)
            cds_len = random.randrange(1,3000)
            protein_pos = random.randrange(1,1000)
            protein_len = random.randrange(1,1000)
            distance = random.randrange(1,3000)
            error = random.sample(['', 'ERROR_CHROMOSOME_NOT_FOUND'], 1)
            
            annotations = annotations + '{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}/{9}|{10}|{11}|{12}/{13}|{14}/{15}|{16}/{17}|{18}|{19}'.format(allele, annotation, impact[0], gene_name, gene_id, feature_type[0], feature_id, transcript_biotype[0], rank, total, hgvsc, hgvsp, cdna_pos, cdna_len, cds_pos, cds_len, protein_pos, protein_len, distance, error[0])
            if k < annotation_count - 1:
                annotations = annotations + ','
            
            annotations_searched.append( (allele, annotation_list, impact[0], gene_name, gene_id, feature_type[0], feature_id, transcript_biotype[0], rank, total, hgvsc, hgvsp, cdna_pos, cdna_len, cds_pos, cds_len, protein_pos, protein_len, distance, error[0]) )
        #print(annotations_searched)
        
        # generate info values
        ac = random.randrange(1,11)
        af = random.uniform(0, 1)
        an = random.randrange(1,11)
        bqrs = random.uniform(-1, 1)
        dp = random.randrange(1,11)
        fs = random.uniform(0, 10)
        hs = random.uniform(0, 10)
        mq = random.uniform(0, 50)
        mqrs = random.uniform(0, 1)
        qd = random.uniform(0, 10)
        rprs = random.uniform(-3, 3)
        info = 'AC={0};AF={1};AN={2};BaseQRankSum={3};DB;DP={4};Dels=0.00;FS={5};HaplotypeScore={6};MQ={7};MQ0=1;MQRankSum={8};QD={9};ReadPosRankSum={10};ANN={11}'.format(ac, af, an, bqrs, dp, fs, hs, mq, mqrs, qd, rprs, annotations)
        #print(info)
        
        # generate sample values
        # GT:AD:DP:GQ:PL 1/1:1,2:3:3:28,3,0
        # AD required for ref_depth, alt_depth
        ad_0 = random.randrange(0, 10)
        ad_1 =random.randrange(0, 10)
        sample_values = "1/1:{0},{1}:3:3:28,3,0".format(ad_0, ad_1)
        sample_values = sample_values + '\n'
                
        vcf_lines.append( (chrom, pos, vcf_id, ref, alt, qual, filters, info, formats, sample_values) )
        variant_searched.append( (chrom, pos, ref, alt, ref_len, alt_len, qual, ac==1, dp, fs, hs, mq, mqrs, qd, rprs, annotation_count, annotations_searched, ad_0, ad_1))
        
    for (chrom, pos, vcf_id, ref, alt, qual, filters, info, formats, sample_values) in vcf_lines: 
        print(chrom, pos, vcf_id, ref, alt, qual, filters, info, formats, sample_values, sep='\t', file=vcf_obj_new, end='')
    
    vcf_obj.close()
    vcf_obj_new.close()
    
    sample_name = sample_name[:-2]
        
    init_all = False
    init_calls = False
    import_calls = None
    init_variants = False
    import_variants = None
    init_variants_nosnp = False
    import_variants_nosnp = None
    init_samples = False
    import_samples = None
    init_annotations = False
    import_annotations = None
    
    defined_filter = None
    start = None
    limit = None
    checkoff = False
    is_vcf_in_pytables = None
    existing_variant_ids = None
    preprocessingoff = False
    
    skip_known_vcf = False
    enable_indexing = False 
    
    # insert vcf entries
    testmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
             init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
             init_annotations, import_annotations, vcf_file_new, defined_filter, start, limit, 
             checkoff, preprocessingoff, skip_known_vcf, enable_indexing)
    
    # test entries in hdf5 file                      
    with open_file(hdf5_file, mode = "r") as searched_file:
        #print(variant_searched)
        for insertion in variant_searched:
            chrom = set_leading_zeros(insertion[0])
            variant_id = -1
            passvalues = None
            passvalues_calls = None
            
            # get variant table
            if insertion[4] == 1 and insertion[5] == 1: #ref_len and alt_len
                retrieved_variant_table = searched_file.get_node('/variants_group', "variantsSNP")
                variant_table_id = 0
            else:
                retrieved_variant_table = searched_file.get_node('/variants_group', "variantsNoSNP")
                variant_table_id = 1
            
            variants_row = retrieved_variant_table.row 
            
            # check variant entries
            query_condition = "(chrom == {0}) & (pos == {1}) & (ref == {2}) & (alt == {3})".format(chrom.encode(), insertion[1],insertion[2].encode(),insertion[3].encode())
            passvalues = [variants_row['variant_id'] for variants_row in retrieved_variant_table.where(query_condition)]
            
            if not passvalues:
                assert_equals("Variant entry not found: {0}".format(query_condition), passvalues)
            else:
                variant_id = passvalues[0]
            
            # check sample name entry 
            retrieved_samples_table = searched_file.get_node('/samples_group', "samples")
            samples_row = retrieved_samples_table.row
                    
            query_condition = "(accession == {0})".format(sample_name.encode())
            print(query_condition)
            passvalues_samples = [[samples_row['sample_id'], samples_row['accession']] for samples_row in retrieved_samples_table.where(query_condition)]
            
            print(passvalues_samples)
            print(passvalues_samples[0][0])
            if not passvalues_samples:
                assert_equals("Sample entry not found: {0}".format(query_condition), passvalues_samples)
            else:
                assert_equals(sample_name.encode(), passvalues_samples[0][1])
                sample_id = passvalues_samples[0][0]
                    
            # check call entries of variant id
            retrieved_calls_table = searched_file.get_node('/', "calls")
            calls_row = retrieved_calls_table.row
            query_condition = "(sample_id == {0}) & (variant_id == {1}) & (variant_table_id == {2})".format(sample_id, variant_id, variant_table_id)
            passvalues_calls = [[calls_row['qual'], calls_row['is_heterozygous'], calls_row['read_depth'], calls_row['strand_bias'], calls_row['haplotype_score'], calls_row['mapping_qual'], calls_row['mapping_qual_bias'], calls_row['qual_by_depth'], calls_row['read_pos_bias'], calls_row['ref_depth'], calls_row['alt_depth']] for calls_row in retrieved_calls_table.where(query_condition)]
            
            if not passvalues_calls:
                assert_equals("Entry not found in Calls Table: {0}".format(query_condition), passvalues_calls)
            else:
                for counter,l in enumerate(range(6,14)):
                    #print(insertion[l], passvalues_calls[0][counter])
                    # compares qual, ac==1, dp, fs, hs, mq, mqrs, qd, rprs of "insertions" 
                    # to retrieved values of pytables in above order
                    assert_equals(float(insertion[l]), passvalues_calls[0][counter])
                
                # ref_depth, alt_depth check
                assert_equals(insertion[17], passvalues_calls[0][9])
                assert_equals(insertion[18], passvalues_calls[0][10])
            
            # check annotation entries 
            retrieved_annotations_table = searched_file.get_node('/annotations_group', "annotations")
            annotations_row = retrieved_annotations_table.row
            if insertion[15]:
                for n in range(insertion[15]):
                    #print(insertion[15])
                    #print(n)
                    #print(insertion[16])
                    passvalues_annotations = None
                    
                    query_condition = "(variant_id == {0}) & (variant_table_id == {1}) & (allele == {2}) & (putative_impact == {4}) & (gene_name == {5}) & (gene_id == {6}) & (feature_type == {7}) & (feature_id == {8}) & (transcript_biotype == {9}) & (rank == {10})".format(variant_id, variant_table_id, insertion[16][n][0].encode(), insertion[16][n][1][0][0].encode(), insertion[16][n][2].encode(), insertion[16][n][3].encode(), insertion[16][n][4].encode(), insertion[16][n][5].encode(), insertion[16][n][6].encode(), insertion[16][n][7].encode(), insertion[16][n][8])
                    
                    
                    passvalues_annotations = [[annotations_row['allele'], 
                               annotations_row['annotation'], annotations_row['annotation2'], 
                               annotations_row['annotation3'], annotations_row['annotation4'], 
                               annotations_row['annotation5'], annotations_row['annotation6'], 
                               annotations_row['annotation7'], annotations_row['total'], 
                               annotations_row['hgvsc'], annotations_row['hgvsp'], 
                               annotations_row['cdna_position'], annotations_row['cdna_len'], 
                               annotations_row['cds_position'], annotations_row['cds_len'], 
                               annotations_row['protein_position'], annotations_row['protein_len'], 
                               annotations_row['distance_to_feature'], annotations_row['info'] ] for annotations_row in retrieved_annotations_table.where(query_condition)]
                    print(passvalues_annotations)
                    
                    if not passvalues_annotations:
                        assert_equals("Annotation entry not found: {0}".format(query_condition), passvalues_annotations)
                    else:
                        assert_equals(1, len(passvalues_annotations))
                        #print(insertion[16][n][1])
                        # test annotations
                        for o in range(len(insertion[16][n][1])):
                            assert_equals(insertion[16][n][1][o][0].encode(), passvalues_annotations[0][o + 1])
                        # total
                        assert_equals(insertion[16][n][9], passvalues_annotations[0][8])
                        # hgvsc
                        assert_equals(insertion[16][n][10].encode(), passvalues_annotations[0][9])
                        # hgvsp
                        assert_equals(insertion[16][n][11].encode(), passvalues_annotations[0][10])
                        # cdna, cds, protein pos and len, distance
                        for o in range(12,19):
                            assert_equals(insertion[16][n][o], passvalues_annotations[0][o - 1])
                        # info
                        assert_equals(insertion[16][n][19].encode(), passvalues_annotations[0][18])
                        
                        print("TADA!")
                        
                    
            # allele, annotation_list, impact[0], gene_name, gene_id, feature_type[0], feature_id, transcript_biotype[0], rank, total, hgvsc, hgvsp, cdna_pos, cdna_len, cds_pos, cds_len, protein_pos, protein_len, distance, error[0]
            
            
