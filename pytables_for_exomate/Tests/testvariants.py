from variants_snp import *
from tables import *
from nose.tools import *
from importvariant import *

__author__ = "Isabell Kullack"

def test_init_variants2():
    """init of variants values are equal"""
    
    with open_file("Tests/test_init_variants.h5", mode = 'w') as searched_file:
        init_structure(searched_file, variantsSNP_structure)
        retrieved_table = searched_file.get_node('/variants_group', "variantsSNP")
        for variants_row in retrieved_table:
            assert_equals(variants_row['is_known'], True)
            assert_equals(variants_row['is_transition'], False)
            assert_equals(variants_row['is_transversion'], True)
        
def test_variants_inserted():
    """test of inserted variants values are equal"""
    with open_file("Tests/test_init_variants.h5", mode = "w", title = "test_1") as searched_file:
        
        init_structure(searched_file, variantsSNP_structure)
        retrieved_table = searched_file.get_node('/variants_group', "variantsSNP")
        
        import_variant = ImportVariant(searched_file, 3, 10583, "G", "A", True, False, 0, retrieved_table)
        import_variant.insert_new_variant()
        for variants_row in retrieved_table:
            assert_equals(variants_row['chrom'], '03'.encode())
            assert_equals(variants_row['pos'], 10583)
            assert_equals(variants_row['alt'], 'G'.encode())

        

