from import_vcf import *
#from import_vcf import main as importvcfmain
from tables import *
from nose.tools import *
from nose.plugins.skip import Skip, SkipTest


__author__ = "Isabell Kullack"

def test_import_values_from_vcf():
    """test inserted values of vcf file of a simple single row in all tables"""
    hdf5_file = "Tests/vcf_test_one_row.h5"
    # reset of hdf5 file
    h5_file_reset = open(hdf5_file, 'w')
    h5_file_reset.close()
    
    with open_file(hdf5_file, mode = "w", title = "vcf_test_one_row") as searched_file:
        vcf_file = "Tests/test_vcf_one_line.vcf"
        defined_filter = None
        start_value = None
        limit = None
        checkoff = False
        is_vcf_in_snp_pytables = None
        existing_snp_variant_ids = None
        
        import_values_from_vcf(searched_file, vcf_file, defined_filter, start_value, limit,
                               checkoff, is_vcf_in_snp_pytables, existing_snp_variant_ids)
        
    with open_file(hdf5_file, mode = "r") as searched_file:
        retrieved_table = searched_file.get_node('/variants_group', "variantsSNP")
        for variants_row in retrieved_table:
            assert_equals(variants_row['variant_id'], 0)# TODO annotations_table_id? sucht man von den varianten aus die Annotations oder zu den annotations die Varianten?
            assert_equals(variants_row['chrom'], '1'.encode())
            assert_equals(variants_row['pos'], 14930)
            assert_equals(variants_row['ref'], 'A'.encode())
            assert_equals(variants_row['alt'], 'G'.encode())
            assert_equals(variants_row['is_transition'], 1)
            assert_equals(variants_row['is_transversion'], 0)
            
        retrieved_table = searched_file.get_node('/', "calls")
        for calls_row in retrieved_table:
            assert_equals(calls_row['sample_id'], 0)
            assert_equals(calls_row['variant_id'], 0)
            assert_equals(calls_row['variant_table_id'], 0)
            assert_equals(calls_row['qual'], 22.1)
            assert_equals(calls_row['is_heterozygous'], 0) # is info['AC'][0] == 1
            assert_equals(calls_row['read_depth'], 3)
            assert_equals(calls_row['ref_depth'], 1)
            assert_equals(calls_row['alt_depth'], 2)
            assert_equals(calls_row['strand_bias'], 0.000)
            assert_equals(calls_row['qual_by_depth'], 3.74)
            assert_equals(calls_row['mapping_qual'], 24.70)
            assert_equals(calls_row['haplotype_score'], 0.0000)
            assert_equals(calls_row['mapping_qual_bias'], 0.358)
            assert_equals(calls_row['read_pos_bias'], -1.231)
	
        retrieved_table = searched_file.get_node('/samples_group', "samples")
        for samples_row in retrieved_table:
            assert_equals(samples_row['sample_id'], 0)
            assert_equals(samples_row['accession'], 'RB_E_032'.encode())
            assert_equals(samples_row['tissue'], 'T'.encode())
            
        retrieved_table = searched_file.get_node('/annotations_group', "annotations")
        counter = 0
        annotations_searched_0 = {'annotation_id': 0, 'variant_id': 0, 'variant_table_id': 0,
                                  'transcript_id' : -999999999, 'allele':'G', 
                                  'annotation': 'downstream_gene_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'DDX11L1', 'gene_id':'DDX11L1',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NR_046018.2',
                                  'transcript_biotype': 'Noncoding',
                                  'rank' : -999999999999999999, 'total' : -999999999999999999, 
                                  'hgvsc' : 'n.*1652A>G', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': 521, 'info' : ''}
                                  
        annotations_searched_1 = {'annotation_id': 1, 'variant_id': 0, 
                                  'variant_table_id': 0,
                                  'transcript_id' : -999999999,
                                  'allele':'G', 
                                  'annotation': 'downstream_gene_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'MIR6859-1', 'gene_id':'MIR6859-1.3',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NR_106918.1.3',
                                  'transcript_biotype': 'Noncoding',
                                  'rank' : -999999999999999999, 'total' : -999999999999999999, 
                                  'hgvsc' : 'n.*68T>C', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': 2439, 'info' : ''}
                                  
        annotations_searched_2 = {'annotation_id': 2, 'variant_id': 0, 
                                  'variant_table_id': 0,
                                  'transcript_id' : -999999999,
                                  'allele':'G', 
                                  'annotation': 'downstream_gene_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'MIR6859-2', 'gene_id':'MIR6859-2.2',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NR_107062.1.2',
                                  'transcript_biotype': 'Noncoding',
                                  'rank' : -999999999999999999, 'total' : -999999999999999999, 
                                  'hgvsc' : 'n.*68T>C', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': 2439, 'info' : ''}
                                  
        annotations_searched_3 = {'annotation_id': 3, 'variant_id': 0, 
                                  'variant_table_id': 0,
                                  'transcript_id' : -999999999,
                                  'allele':'G', 
                                  'annotation': 'intron_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'WASH7P', 'gene_id':'WASH7P',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NR_024540.1',
                                  'transcript_biotype': 'Noncoding',
                                  'rank' : 10, 'total' : 10, 
                                  'hgvsc' : 'n.1301+40T>C', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': -999999999999999999, 'info' : ''}
                                  
        counter_ref = {0: annotations_searched_0, 1: annotations_searched_1, 
                       2: annotations_searched_2, 3: annotations_searched_3}
        for annotations_row in retrieved_table:
            if counter in counter_ref:
                for k, v in counter_ref[counter].items():
                    if isinstance(v, str):
                        v = v.encode()
                    print(k,v)
                    assert_equals(annotations_row[k], v)
            counter = counter + 1
        

