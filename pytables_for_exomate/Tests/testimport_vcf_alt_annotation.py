from import_vcf import *
from pytables_for_exomate import main as pytmain
from tables import *
from nose.tools import *
from nose.plugins.skip import Skip, SkipTest


__author__ = "Isabell Kullack"

def test_import_vcf_alt_depth_splitting():
    """test alt and alt depth splitting in combined variants of imported vcf files"""
    vcf_file = "Tests/test_vcf_three_line_alt_split_annotation.vcf"
    hdf5_file = "Tests/test_vcf_three_line_alt_split_annotation.h5"
    h5_file_reset = open(hdf5_file, 'w')
    h5_file_reset.close()
    
    defined_filter = None
    start_value = None
    limit = None
    checkoff = False
    is_vcf_in_pytables = None
    existing_variant_ids = None
    preprocessingoff = False
    init_all = True
    init_calls = True
    import_calls = None
    init_variants = True
    import_variants = None
    init_variants_nosnp = True 
    import_variants_nosnp, init_samples, import_samples = None, True, None
    init_annotations = True
    import_annotations = None
    enable_indexing = False
        
    skip_known_vcf = False
    pytmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
         init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
         init_annotations, import_annotations, vcf_file, defined_filter, start, limit, checkoff, 
         preprocessingoff, skip_known_vcf, enable_indexing)
    #importvcfmain(hdf5_file, vcf_file, defined_filter, start_value, limit, checkoff, 
                    #preprocessingoff, skip_known_vcf)
        
    #with open_file(hdf5_file, mode = "w") as searched_file:
        # should insert all 3 variants, second with alt spliting
        #import_values_from_vcf(searched_file, vcf_file, defined_filter, start_value, limit, 
        #                       checkoff, is_vcf_in_pytables, existing_variant_ids)
    
    with open_file(hdf5_file, mode = "a") as searched_file:
        retrieved_snp_table = searched_file.get_node('/variants_group', "variantsSNP")
        retrieved_nosnp_table = searched_file.get_node('/variants_group', "variantsNoSNP")
        
        snp_values = [[variants_row['variant_id'], variants_row['ref'], variants_row['alt']] 
                      for variants_row in retrieved_snp_table.where("""(pos == 22082967)""")]
        assert_equal(snp_values, [])
        
        nosnp_values = [[variants_row['variant_id'], variants_row['ref'], variants_row['alt']]
                         for variants_row in retrieved_nosnp_table.where("""(pos == 22082967)""")]
        
        assert_equal(nosnp_values[0], [0,b'CAA',b'C'])
        assert_equal(nosnp_values, [[0,b'CAA',b'C'],[1,b'CAA',b'CA']])


def test_import_vcf_annotation_relation_with_alt_splitting():
    """test inserted annotations of combined variants after splitting alt and alt depth 
    of imported vcf files"""
    with open_file("Tests/test_vcf_three_line_alt_split_annotation.h5", mode = "a") as searched_file:
        retrieved_table = searched_file.get_node('/annotations_group', "annotations")
        #print(retrieved_table)
        counter = 0
        
        # Annotations with allele C shall be imported first 
        # related to the first variant of table 1
        annotations_searched_4 = {'annotation_id': 4, 'variant_id': 0, 
                                  'variant_table_id': 1,
                                  'transcript_id' : -999999999,
                                  'allele':'C', 
                                  'annotation': 'intron_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'USP48', 'gene_id':'USP48',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NM_032236.5',
                                  'transcript_biotype': 'Coding',
                                  'rank' : 3, 'total' : 26, 
                                  'hgvsc' : 'c.412+70_412+71delTT', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': -999999999999999999, 'info' : ''}
                                  
        annotations_searched_5 = {'annotation_id': 5, 'variant_id': 0, 
                                  'variant_table_id': 1,
                                  'transcript_id' : -999999999,
                                  'allele':'C', 
                                  'annotation': 'intron_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'USP48', 'gene_id':'USP48',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NM_001032730.1',
                                  'transcript_biotype': 'Coding',
                                  'rank' : 3, 'total' : 10, 
                                  'hgvsc' : 'c.412+70_412+71delTT', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': -999999999999999999, 'info' : ''}
                         
        # Annotations with allele C shall be imported next related to second variant
        annotations_searched_6 = {'annotation_id': 6, 'variant_id': 1, 
                                  'variant_table_id': 1,
                                  'transcript_id' : -999999999,
                                  'allele':'CA', 
                                  'annotation': 'intron_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'USP48', 'gene_id':'USP48',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NM_032236.5',
                                  'transcript_biotype': 'Coding',
                                  'rank' : 3, 'total' : 26, 
                                  'hgvsc' : 'c.412+70delT', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': -999999999999999999, 'info' : ''}
                                  
        annotations_searched_7 = {'annotation_id': 7, 'variant_id': 1, 
                                  'variant_table_id': 1,
                                  'transcript_id' : -999999999,
                                  'allele':'CA', 
                                  'annotation': 'intron_variant', 
                                  'annotation2':'', 'annotation3':'',
                                  'putative_impact' : 'MODIFIER', 
                                  'gene_name':'USP48', 'gene_id':'USP48',
                                  'feature_type': 'transcript',
                                  'feature_id': 'NM_001032730.1',
                                  'transcript_biotype': 'Coding',
                                  'rank' : 3, 'total' : 10, 
                                  'hgvsc' : 'c.412+70delT', 'hgvsp' : '', 
                                  'cdna_position' : -999999999999999999, 
                                  'cdna_len' : -999999999999999999, 
                                  'cds_position' : -999999999999999999, 
                                  'cds_len' : -999999999999999999, 
                                  'protein_position' : -999999999999999999,
                                  'protein_len' : -999999999999999999, 
                                  'distance_to_feature': -999999999999999999, 'info' : ''}
                         
        counter_ref = {4: annotations_searched_4, 5: annotations_searched_5, 
                       6: annotations_searched_6, 7: annotations_searched_7}
        for annotations_row in retrieved_table:
            if counter in counter_ref:
                for k, v in counter_ref[counter].items():
                    if isinstance(v, str):
                        v = v.encode()
                    #print(k,v)
                    assert_equals(annotations_row[k], v)
            counter = counter + 1
        
        """
        annotation sequence given:
        C|intron_variant|MODIFIER|USP48|USP48|transcript|NM_032236.5|Coding|3/26|c.412+70_412+71delTT||||||,
        CA|intron_variant|MODIFIER|USP48|USP48|transcript|NM_032236.5|Coding|3/26|c.412+70delT||||||,
        C|intron_variant|MODIFIER|USP48|USP48|transcript|NM_001032730.1|Coding|3/10|c.412+70_412+71delTT||||||,
        CA|intron_variant|MODIFIER|USP48|USP48|transcript|NM_001032730.1|Coding|3/10|c.412+70delT||||||"""
