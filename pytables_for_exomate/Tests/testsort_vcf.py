from pytables_for_exomate import main as testmain
from tables import *
from nose.tools import *
#from nose.plugins.skip import Skip, SkipTest
#from sort_vcf import sort_vcf


__author__ = "Isabell Kullack"


def test_sort_vcf():
    """test sorting and import of vcf file"""
    
    hdf5_file = "Tests/test_vcf_sort.h5"

    h5_file_reset = open(hdf5_file, 'w')
    h5_file_reset.close()
        
    init_all = False
    init_calls = False
    import_calls = None
    init_variants = False
    import_variants = None
    init_variants_nosnp = False
    import_variants_nosnp = None
    init_samples = False
    import_samples = None
    init_annotations = False
    import_annotations = None
    
    vcf_file = "Tests/test_vcf_sort.vcf"
    defined_filter = None
    start = None
    limit = None
    checkoff = False
    is_vcf_in_pytables = None
    existing_variant_ids = None
    preprocessingoff = False
    
    # insert vcf entries
    testmain(hdf5_file, init_all, init_calls, import_calls, init_variants, import_variants, 
         init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
         init_annotations, import_annotations, vcf_file, defined_filter, start, limit, checkoff, 
         preprocessingoff)
    
    # test entries in hdf5 file                      
    with open_file(hdf5_file, mode = "r") as searched_file:
        retrieved_table = searched_file.get_node('/variants_group', "variantsSNP")
        
        # 1	14930	rs75454623	A	G
        variants_snps_searched_0 = {'variant_id': 0, 'chrom': '01', 'pos': 14930, 'ref' : 'A',
                                  'alt':'G', 'is_transition': 1,  'is_transversion' : 0}
        
        # 2	14930	rs75454623	A	G
        variants_snps_searched_1 = {'variant_id': 1, 'chrom': '02', 'pos': 14930, 'ref' : 'A',
                                  'alt':'G', 'is_transition': 1,  'is_transversion' : 0}
        
        # 11	14930	rs75454623	A	G
        variants_snps_searched_2 = {'variant_id': 2, 'chrom': '11', 'pos': 14930, 'ref' : 'A',
                                  'alt':'G', 'is_transition': 1,  'is_transversion' : 0}
        
        # GL000194.1	20144	rs143346096	G	A	25.52
        variants_snps_searched_3 = {'variant_id': 3, 'chrom': 'GL000194.1', 'pos': 20144, 'ref' : 'G',
                                  'alt':'A', 'is_transition': 1,  'is_transversion' : 0}

        # MT	20144	rs143346096	G	A	25.52	.	
        variants_snps_searched_4 = {'variant_id': 4, 'chrom': 'MT', 'pos': 20144, 'ref' : 'G',
                                  'alt':'A', 'is_transition': 1,  'is_transversion' : 0}
                                  
        # X	15211	rs781809	A	G	76.35	.	
        variants_snps_searched_5 = {'variant_id': 5, 'chrom': 'X', 'pos': 15211, 'ref' : 'A',
                                  'alt':'G',  'is_transition': 1,  'is_transversion' : 0}
                                  
        # X	15211	rs78601809	T	G	76.35	.	
        variants_snps_searched_6 = {'variant_id': 6, 'chrom': 'X', 'pos': 15211, 'ref' : 'T',
                                  'alt':'G', 'is_transition': 0,  'is_transversion' : 1}
        
        counter_ref = {0: variants_snps_searched_0, 1: variants_snps_searched_1, 
                       2: variants_snps_searched_2, 3: variants_snps_searched_3, 
                       4: variants_snps_searched_4, 5: variants_snps_searched_5, 
                       6: variants_snps_searched_6}
        counter = 0
        for variants_row in retrieved_table:
            if counter in counter_ref:
                for k, v in counter_ref[counter].items():
                    if isinstance(v, str):
                        v = v.encode()
                    print(k,v)
                    assert_equals(variants_row[k], v)
            counter = counter + 1
        
        retrieved_nosnp_table = searched_file.get_node('/variants_group', "variantsNoSNP")
        
        # X	15211	rs78601809	T	GA	6.35	
        variants_nosnps_searched_0 = {'variant_id': 0, 'chrom': 'X', 'pos': 15211, 'ref' : 'T',
                                       'alt':'GA',  'is_transition': 0,  'is_transversion' : 0}
       
        # X	15211	rs78601809	TA	G	76.5	.	
        variants_nosnps_searched_1 = {'variant_id': 1, 'chrom': 'X', 'pos': 15211, 'ref' : 'TA',
                                       'alt':'G', 'is_transition': 0,  'is_transversion' : 0}
        
        counter_ref = {0: variants_nosnps_searched_0, 1: variants_nosnps_searched_1}
        counter = 0
        for variants_row in retrieved_nosnp_table:
            if counter in counter_ref:
                for k, v in counter_ref[counter].items():
                    if isinstance(v, str):
                        v = v.encode()
                    #print(k,v)
                    assert_equals(variants_row[k], v)
            counter = counter + 1
        
        retrieved_calls_table = searched_file.get_node('/', "calls")
        
        # A	G 22.45	. 	AC=2;AF=1.00;AN=2;BaseQRankSum=-0.358;DB;DP=3;Dels=0.00;FS=0.000;
        #HaplotypeScore=0.0000;MQ=24.70;MQ0=1;MQRankSum=0.358;QD=3.74;ReadPosRankSum=-1.231
        calls_searched_0 = {'sample_id': 0, 'variant_id': 0, 'variant_table_id': 0, 'qual': 22.1, 
                            'is_heterozygous' : 0, 'read_depth':3, 'ref_depth': 1, 'alt_depth': 2,
                            'strand_bias': 0.000, 'qual_by_depth' : 3.74, 'mapping_qual' : 24.70, 
                            'haplotype_score' :  0.0000, 'mapping_qual_bias' : 0.358, 
                            'read_pos_bias' : -1.231}
       
        # test rest if id fit by qual
        
        calls_searched_1 = {'sample_id': 0, 'variant_id': 1, 'variant_table_id': 0, 
                            'qual': 22.2}

        calls_searched_2 = {'sample_id': 0, 'variant_id': 2, 'variant_table_id': 0, 
                            'qual': 22.11}

        calls_searched_3 = {'sample_id': 0, 'variant_id': 3, 'variant_table_id': 0, 
                            'qual': 225.52}

        calls_searched_4 = {'sample_id': 0, 'variant_id': 4, 'variant_table_id': 0, 
                            'qual': 25.52}
        
        calls_searched_5 = {'sample_id': 0, 'variant_id': 5, 'variant_table_id': 0, 
                            'qual': 76.335}

        calls_searched_6 = {'sample_id': 0, 'variant_id': 6, 'variant_table_id': 0, 
                            'qual': 76.35}

        calls_searched_7 = {'sample_id': 0, 'variant_id': 0, 'variant_table_id': 1, 
                            'qual': 6.35}

        calls_searched_8 = {'sample_id': 0, 'variant_id': 1, 'variant_table_id': 1, 
                            'qual': 76.5}

        counter_ref = {0: calls_searched_0, 1: calls_searched_1, 2: calls_searched_2, 
                       3: calls_searched_3, 4: calls_searched_4, 5: calls_searched_5, 
                       6: calls_searched_6, 7: calls_searched_7, 8: calls_searched_8}
        counter = 0
        for variants_row in retrieved_calls_table:
            if counter in counter_ref:
                for k, v in counter_ref[counter].items():
                    if isinstance(v, str):
                        v = v.encode()
                    print(k,v)
                    assert_equals(variants_row[k], v)
            counter = counter + 1

