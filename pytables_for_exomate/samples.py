from tables import *
from filehandler import *
from profiling import *
import argparse
import sys

__author__ = "Isabell Kullack"


class Samples(IsDescription):
    sample_id = Int64Col(dflt=-999999999999999999, pos=0)
    accession = StringCol(30, dflt='', pos=1)
    patient_id = Int64Col(dflt=-999999999999999999, pos=2)
    tissue = StringCol(15, dflt='', pos=3) 
    disease_id = Int64Col(dflt=-999999999999999999, pos=4)
    
samples_structure = {'name':'samples', 'class': Samples, 'group':'samples_group', 'group_title':'samples group', 'table_title': 'samples table', 'is_in_group':True}


def main(hdf5_file_obj, init, import_file):
    
    if init:
        samples_table = init_structure(hdf5_file_obj, samples_structure)
    else:
        try:
            samples_table = get_table_from_file(hdf5_file_obj, samples_structure['name'])
        except NoSuchNodeError:
            samples_table = init_structure(hdf5_file_obj, samples_structure)
    
    if import_file:
        import_samples_from_csv(hdf5_file_obj, import_file, samples_table)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("--init", type=bool, default=False, choices=(True, False),
                        help="boolean to init samples table -- Caution ALL SAMPLES LOST! [default: False]")
    parser.add_argument("-i", "--importsamplesfromcsv", type=str,
                        help="csv file name or path containing samples to be imported into samples table")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on sample tables operations enabled")
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    hdf5_file_obj = open_in_mode(args.hdf5file, 'a')
    sys.exit(main(hdf5_file_obj, args.init, args.importsamplesfromcsv))   
    hdf5_file_obj.close()
    if(args.enableprofiling):
        disable_profiling(profiling_object)
