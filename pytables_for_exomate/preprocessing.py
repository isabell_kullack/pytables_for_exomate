import numpy as np
import time #TODO later on not needed
import h5py
from profiling import *


__author__ = "Isabell Kullack"

def fields_view(arr, fields):
    """ creates view of extracted numpy array - author Christopher Schroeder """
    dtype2 = np.dtype({name:arr.dtype.fields[name] for name in fields})
    return np.ndarray(arr.shape, dtype2, arr, 0, arr.strides) 


#@profile_method
def vcf_data_insertion_check(hdf5_file, vcf_file_new):
    """
    main preprocessing extraction numpy arrays of vcf and pytables files and create
    intersection arrays including variants existing ids
    """
    if(hdf5_file[-3:] != ".h5"):
        hdf5_file = hdf5_file + ".h5"
        
    #try:
    # extract np array of pytables file with columns chrom, pos, ref, alt, variant_id   
    h5py_file = h5py.File(hdf5_file,'r')
    
    if vcf_file_new is not None:
        # extract np array of vcf file with columns chrom, pos, ref, alt   
        vcf_array = np.genfromtxt(vcf_file_new, delimiter = '\t', usecols=(0, 1, 3, 4),
                                 dtype={'names': ('chrom', 'pos', 'ref', 'alt'),
                                        'formats': ('S12', 'i8', 'S64', 'S64')})
        
        is_vcf_in_snp_pytables, is_vcf_in_nosnp_pytables, existing_snp_variants, existing_nosnp_variants, existing_snp_variants_in_vcf, existing_nosnp_variants_in_vcf, existing_snp_variant_ids, existing_nosnp_variant_ids = None, None, None, None, None, None, None, None
    
        if (h5py_file["/variants_group/variantsSNP"].shape[0] != 0):
            
            pytables_snp_array = np.array(h5py_file["/variants_group/variantsSNP"],
                                  dtype={'names': ('chrom', 'pos', 'ref', 'alt', 'variant_id'), 
                                         'formats': ('S12', 'i8', 'S64', 'S64', 'i8')})
            
            # sort pytables arrays to get correct ordered ids
            pytables_snp_array_sorted = np.sort(pytables_snp_array, order=('chrom', 'pos', 'ref', 'alt'))
            
            # create a four column view of the 5 column pytables extracted numpy array
            pytables_snp_view = fields_view(pytables_snp_array_sorted, ["chrom", "pos", "ref", "alt"])
            
            # boolean intersection array
            is_vcf_in_snp_pytables = np.in1d(vcf_array, pytables_snp_view)
            
            # existing variants
            existing_snp_variants_in_vcf = vcf_array[is_vcf_in_snp_pytables]
            
            # existing bools
            exist_snp_pytables_bools = np.in1d(pytables_snp_view, vcf_array)
            
            # existing ids
            existing_snp_variant_ids = pytables_snp_array_sorted["variant_id"][exist_snp_pytables_bools]
            
            # existing found variants
            existing_snp_variants = pytables_snp_array_sorted[exist_snp_pytables_bools]
            
            #np.set_printoptions(threshold=1000000)
            #print("vcf",vcf_array)
            #print('pyt',pytables_snp_view)
            #print(is_vcf_in_snp_pytables)
            #print("exist:", existing_snp_variants_in_vcf)
            
            #to insert values from vcf_Array
            #variants_not_in_pytables = vcf_array[np.logical_not(is_vcf_in_snp_pytables)]
            
            #print("variants_not_in_pytables:")
            #print(variants_not_in_pytables[0]) 
            #print(exist_snp_pytables_bools)
            #print("existing_snp_variant_ids:", existing_snp_variant_ids)
            #print(existing_snp_variant_ids[0])
            
            #print('Varianten ID SNP', pytables_snp_array["variant_id"])
            #print('Maximale Varianten ID SNP', max(pytables_snp_array["variant_id"]))

            
            
        if (h5py_file["/variants_group/variantsNoSNP"].shape[0] != 0):
            pytables_nosnp_array = np.array(h5py_file["/variants_group/variantsNoSNP"],
                                  dtype={'names': ('chrom', 'pos', 'ref', 'alt', 'variant_id'), 
                                         'formats': ('S12', 'i8', 'S64', 'S64', 'i8')})
            
            # sort pytables arrays to get correct ordered ids
            pytables_nosnp_array_sorted = np.sort(pytables_nosnp_array, order=('chrom', 'pos', 'ref', 'alt'))
            
            # create a four column view of the 5 column pytables extracted numpy array
            pytables_nosnp_view = fields_view(pytables_nosnp_array_sorted, ["chrom", "pos", "ref", "alt"])
            
            # boolean intersection array
            is_vcf_in_nosnp_pytables = np.in1d(vcf_array, pytables_nosnp_view)
            
            # existing variants
            existing_nosnp_variants_in_vcf = vcf_array[is_vcf_in_nosnp_pytables]
            
            # existing bools
            exist_nosnp_pytables_bools = np.in1d(pytables_nosnp_view, vcf_array)
            
            # existing ids
            existing_nosnp_variant_ids = pytables_nosnp_array_sorted["variant_id"][exist_nosnp_pytables_bools]
            
            # existing found variants
            existing_nosnp_variants = pytables_nosnp_array_sorted[exist_nosnp_pytables_bools]
            
            #print(exist_nosnp_pytables_bools)
            #print("existing_nosnp_variant_ids:", existing_nosnp_variant_ids)
            #print(existing_nosnp_variant_ids[0])
        
        
        return is_vcf_in_snp_pytables, is_vcf_in_nosnp_pytables, existing_snp_variants, existing_nosnp_variants, existing_snp_variants_in_vcf, existing_nosnp_variants_in_vcf, existing_snp_variant_ids, existing_nosnp_variant_ids, True, vcf_file_new
    
    else:
        return None, None, None, None, None, None, None, None, False, vcf_file_new
        
    #except:
    #    return None, None, None, None, None, None, None, None, False, vcf_file_new  
            

