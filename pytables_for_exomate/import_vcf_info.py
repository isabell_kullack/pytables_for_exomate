from importobjecthandler import *
from preprocessing import vcf_data_insertion_check
from tables import *
from filehandler import *
from importvariant import *
from importcall import *
from importsample import *
from profiling import *
from sort_vcf import *
#from indexHandler import create_tables_index
from extract_annotation_info import processing_annotation_extraction
import sys
import os
import argparse
import numpy as np
import time #TODO later on not required
from variants_utilities import normalized_indel, transition_transversion
from vcf import Reader as VCFReader

__author__ = "Isabell Kullack"


def import_values_from_vcf(hdf5_file, vcf_file, defined_filter, start_value, limit, checkoff,
                           is_vcf_in_snp_pytables = None, existing_snp_variant_ids = None, 
                           is_vcf_in_nosnp_pytables = None, existing_nosnp_variant_ids = None, 
                           preprocessingoff = False, skip_known_vcf = False):
    """Variants from vcf_file will be imported in a pytables hdf5_file."""
    temp_variant_id = -999999999999999999
    first_row = True
    is_variant_table = True
    counter_for_is_snp_variant_existing = 0
    counter_for_is_nosnp_variant_existing = 0
    is_new_sample_import = False
    is_known_variant = False
    counter = 0
    counter_for_annotations_of_sample = 0
    
    calls_table, variants_snp_table, variants_nosnp_table, samples_table, annotations_table, variant_snp_id, variant_nosnp_id, sample_id, annotation_id = get_import_objects_and_ids(hdf5_file)
    #print('hier!!!')
    #print(variants_snp_table, variants_nosnp_table)    
    vcf_obj = open(vcf_file, 'r')
    vcf_reader = VCFReader(vcf_obj)
    # start in enumerate takes about 0.10-0.20 second more, not of caching
    for counter, record in enumerate(vcf_reader):
        
        # limit 0 and no limit otherwise the same
        if (limit is not None) & (limit == 0):
            if counter > limit:
                break
        
        # if limit reached: finish        
        if limit:
            if limit >= 0:
                if counter > limit:
                    break
            else:
                print("Negative limit.")
                break
        
        # do nothing up to start_value
        if start_value:
            if counter < start_value:
                if is_vcf_in_snp_pytables is not None and is_vcf_in_snp_pytables[counter]:
                    counter_for_is_snp_variant_existing = counter_for_is_snp_variant_existing + 1
                if is_vcf_in_nosnp_pytables is not None and is_vcf_in_nosnp_pytables[counter]:
                    counter_for_is_nosnp_variant_existing = counter_for_is_nosnp_variant_existing + 1
                
                continue
            else:
                pass
        
                  
                    
        # also used for ref/alt depth
        sample_record = record.samples[0]
        # one sample in vcf extraction 
        if first_row:
            first_row = False
            try:
                sample_name = sample_record.sample
            except:
                sample_name = None
            if sample_name:
                accession = sample_name[:-2]
                tissue = sample_name[-1]
                import_sample = ImportSample(hdf5_file, accession, None, tissue, None, samples_table, sample_id)
                print('is_new_sample_import', import_sample.is_new_entry)
                is_new_sample_import = import_sample.is_new_entry
                if skip_known_vcf:
                    if not is_new_sample_import:
                        break 
                sample_id = import_sample.sample_id
                if samples_table.cols.sample_id.is_indexed: #TODO more generic, method down
                    samples_table.flush_rows_to_index()
                else:
                    samples_table.flush()
        
#        # filter extract
#        if record.FILTER:
#            print(record.Filter)
#            continue
#        else:
#            continue
        
        # filter added if given
        if record.FILTER is not None and defined_filter in {'q10', 's50'}:
            #print('counter ', counter)
            record_filter = remove_brackets_quotations(record.FILTER)
            #print(record_filter)
            if record_filter == defined_filter:
                print("Entry skipped by filter: {0}".format(record_filter))
                if is_vcf_in_snp_pytables is not None and is_vcf_in_snp_pytables[counter]:
                    counter_for_is_snp_variant_existing = counter_for_is_snp_variant_existing + 1
                if is_vcf_in_nosnp_pytables is not None and is_vcf_in_nosnp_pytables[counter]:
                    counter_for_is_nosnp_variant_existing = counter_for_is_nosnp_variant_existing + 1
                continue
    
        # multiple alt value separation
        alt = record.ALT     
        alt = remove_brackets_quotations(alt)
        alt_array = []
        alt_array.append(alt)
        alt_is_splitted = False
        
        # alt_depth extraction and computation by author Marcel Martin
        try:
            ref_depth = sample_record['AD'][0]
            alt_depths = sample_record['AD'][1:]     
        except AttributeError:
            ref_depth = None
            alt_depths = [None] * len(alt)
        if "," in alt:
            alt_array = alt.split(", ")
            alt_is_splitted = True
            
        
        # values from vcf reader record for variants
        chrom = record.CHROM
        pos = record.POS 
        ref = record.REF
        ref = str(ref)
        # is_transition = record.is_transition # this or function?
        is_transition, is_transversion = transition_transversion(ref, alt) # requires str not list
        
        
        # values from vcf reader record for calls
        info = record.INFO
        get = record.INFO.get
        is_heterozygous = None
        alt_depth = None
        strand_bias = None
        read_depth = None
        qual = record.QUAL
        if 'DP' in info:
            read_depth = info['DP'] # in Totel Depth of all samples in record
        if 'AC' in info:
            is_heterozygous = info['AC'][0] == 1 # allele count in genotypes
        if 'FS' in info:
            strand_bias = info['FS'] # strand bias at this position in exomate
        """if 'SB' in info:
            strand_bias = info['SB'] # strand bias in vcf specification 4.2"""
        
        haplotype_score = get('HaplotypeScore', None)
        mapping_qual = get('MQ', None)
        qual_by_depth = get('QD', None)
        mapping_qual_bias = get('MQRankSum', None)
        read_pos_bias = get('ReadPosRankSum', None)
        
        #print('alt_array', alt_array)
        #print('alt_depth', alt_depth)

        # multiple alt values in variants and referencing annotations inserted
        for alts, alt_depth in zip(alt_array, alt_depths):
            #print('alts', alts)
            
            if (len(alts) == 1) and (len(ref) == 1):
                # check if record ist SNP and set flag
                temp_variant_id = variant_snp_id
                temp_variants_table = variants_snp_table
                is_snp_table = True
                variant_table_id = variant_table['variant_snp_id']
            else:
                temp_variant_id = variant_nosnp_id
                temp_variants_table = variants_nosnp_table
                is_snp_table = False
                variant_table_id = variant_table['variant_nosnp_id']
            
            # activate or deactivate pytables-data-check
            is_variant_check, is_call_check = get_data_insertion_checks(checkoff, preprocessingoff, alt_is_splitted, is_new_sample_import, is_variant_table)
            #print(is_variant_check)
            #print(is_call_check)
                
            # if preprocessing activated recieved intersection arrays and 
            # boolean of intersection array for found variants including their annotations
            if (is_vcf_in_snp_pytables is not None) and (existing_snp_variant_ids is not None) and not alt_is_splitted and is_vcf_in_snp_pytables[counter]:
                #starttime = time.time()
                temp_variant_id = existing_snp_variant_ids[counter_for_is_snp_variant_existing]
                counter_for_is_snp_variant_existing = counter_for_is_snp_variant_existing + 1
                is_known_variant = True
                #print('variant id extract', time.time()-starttime)
                
            elif (is_vcf_in_nosnp_pytables is not None) and (existing_nosnp_variant_ids is not None) and not alt_is_splitted and is_vcf_in_nosnp_pytables[counter]:
                temp_variant_id = existing_nosnp_variant_ids[counter_for_is_nosnp_variant_existing]
                counter_for_is_nosnp_variant_existing = counter_for_is_nosnp_variant_existing + 1
                is_known_variant = True        
            else:  
                #starttime = time.time()
                #print('is_variant_check',is_variant_check)
                import_variant = ImportVariant(hdf5_file, chrom, pos, ref, alts, is_transition,
                                               is_transversion, temp_variant_id, 
                                               temp_variants_table, is_variant_check)
                # temporary variant_id for found variants because variant_snp/nosnp_id is counted up
                temp_variant_id = import_variant.variant_id
                    
                if not import_variant.is_new_entry:
                    is_known_variant = True
                
                #print('variant import', time.time()-starttime)
                #starttime = time.time()
                annotation_count = 0
                # extract and insert annotations                          
                annotation_id, annotation_count = processing_annotation_extraction(info['ANN'],  hdf5_file, 
                                                annotations_table, pos, ref, alts,
                                                temp_variant_id, variant_table_id, annotation_id)
                
                #print('annotation import', time.time()-starttime)
                counter_for_annotations_of_sample += annotation_count
            #print('Annotation eingetragen.')
            # insert call if sample id
            if sample_id != -999999999999999999:
                #starttime = time.time()
                import_call = ImportCall(hdf5_file, variant_table_id, qual, is_heterozygous, 
                                         read_depth, ref_depth, alt_depth, strand_bias, 
                                         qual_by_depth, mapping_qual, haplotype_score, 
                                         mapping_qual_bias, read_pos_bias, calls_table, sample_id,
                                         temp_variant_id, is_call_check)
                #print('call import', time.time()-starttime)
                
                #print('Call eingetragen.')
            #print('is_call_check',is_call_check)
            # counted up variant id and set temp_variant_id also for each splitted alt entry
            if not is_known_variant:
                if is_variant_table:
                    variant_snp_id = variant_snp_id + 1
                    temp_variant_id = variant_snp_id
                else:
                    variant_nosnp_id = variant_nosnp_id + 1
                    temp_variant_id = variant_nosnp_id
            else:
                is_known_variant = False
                if is_variant_table:
                    temp_variant_id = variant_snp_id
                else:
                    temp_variant_id = variant_nosnp_id

        # flushing of table in packets
        if (counter % 100000 == 0):
            flush_tables([variants_snp_table, variants_nosnp_table, calls_table, annotations_table])
            variant_snp_id = get_maximum_table_id(variants_snp_table, 'variant_id')
            variant_nosnp_id = get_maximum_table_id(variants_nosnp_table, 'variant_id')
            annotation_id = get_maximum_table_id(annotations_table, 'annotation_id')
            #print(annotation_id)
            
    #flushing all tables after appending all variants
    flush_tables([variants_snp_table, variants_nosnp_table, calls_table, annotations_table])
    #record.is_snp, record.is_indel, record.is_transition, record.is_deletion possible
    print('vcf vollständig')
    vcf_obj.close()
    print('hier auch')

    print('Records in VCF: ', counter)
    counter_new = counter - counter_for_is_snp_variant_existing - counter_for_is_nosnp_variant_existing
    print('neue Varianten: ', counter_new)
    print('Annotations für Sample', counter_for_annotations_of_sample) 
    print('Varianten in variantsSNP', variants_snp_table._v_attrs.NROWS)
    print('Varianten in variantsNoSNP', variants_nosnp_table._v_attrs.NROWS)
    print('Annotationen in annotations', annotations_table._v_attrs.NROWS)
    print('Calls in calls', calls_table._v_attrs.NROWS)
    return counter

def flush_tables(tables_dict):
    for table_ref in tables_dict:
        if table_ref.cols.variant_id.is_indexed: # TODO more generalize
            table_ref.flush_rows_to_index()
        else:
            table_ref.flush()
            
            
@profile_method
def main(hdf5_file, vcf_file, defined_filter, start_value, limit, checkoff, preprocessingoff, skip_known_vcf = False):
    is_vcf_in_snp_pytables, is_vcf_in_nosnp_pytables, existing_snp_variants, existing_nosnp_variants, existing_snp_variants_in_vcf, existing_nosnp_variants_in_vcf, existing_snp_variant_ids, existing_nosnp_variant_ids, is_preprocessing_successful, vcf_file_new = None, None, None, None, None, None, None, None, True, None 
    # sorting of vcf
    last_counter = 0
    vcf_file_new = sort_vcf(vcf_file)
    # preprocessing
    if not preprocessingoff:
        is_vcf_in_snp_pytables, is_vcf_in_nosnp_pytables, existing_snp_variants, existing_nosnp_variants, existing_snp_variants_in_vcf, existing_nosnp_variants_in_vcf, existing_snp_variant_ids, existing_nosnp_variant_ids, is_preprocessing_successful, vcf_file_new = vcf_data_insertion_check(hdf5_file, vcf_file_new)
        if not is_preprocessing_successful:
            print("Preprocessing quit before import {0}.".format(vcf_file))
            if os.path.isfile(vcf_file_new):
                try:
                    os.remove(vcf_file_new)
                except Exception as e:
                    print("Temporary sorted vcf file deletion was impossible: {0}".format(e))
            return True, last_counter
        print('Preprocesssing completed.')
    # import
    hdf5_file_obj = open_in_mode(hdf5_file, 'a') # open_file(hdf5_file, append=True, mode='a')
    last_counter = import_values_from_vcf(hdf5_file_obj, vcf_file_new, defined_filter, start_value, limit, 
                           checkoff, is_vcf_in_snp_pytables, existing_snp_variant_ids, 
                           is_vcf_in_nosnp_pytables, existing_nosnp_variant_ids, 
                           preprocessingoff, skip_known_vcf)
    hdf5_file_obj.close() 
    if os.path.isfile(vcf_file_new):
        try:
            os.remove(vcf_file_new)
        except Exception as e:
            print("Temporary sorted vcf file deletion was impossible: {0}".format(e))
    return False, last_counter
    
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("vcffile", help="vcf file to be imported")
    parser.add_argument("-f", "--filterminus", type=str, default='', choices=('', 'q10', 's50'),
                        help="filter left out given insert [Example: q10]")
    parser.add_argument("-s", "--start_value", type=int,
                        help="start_value of row number to be imported -- first row number is 0")
    parser.add_argument("-l", "--limit", type=int,
                        help="including limit of row number up to be imported")
    parser.add_argument("-c", "--checkoff", action="store_true", 
                        help="disables checking if variants and calls and samples in tables")
    parser.add_argument("--skipknownvcf", type=bool, default=False, choices=(True, False),
                        help="skips vcf import if sample is found - no calls check or missing variant import! [default: False]")
    parser.add_argument("-pre", "--preprocessingoff", action="store_true", 
                        help="disables preprocessed checking if variants in tables")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.vcffile, args.filterminus, args.start_value, args.limit, 
                  args.checkoff, args.preprocessingoff, args.skipknownvcf))   
    if(args.enableprofiling):
        disable_profiling(profiling_object)
