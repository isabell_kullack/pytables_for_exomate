from profiling import *
from tables import *
from datastructures import *
import time

__author__ = "Isabell Kullack"


#open_mode: 'r', 'r+', 'a' and 'w'
def open_in_mode(opened_file, open_mode):
    if(opened_file[-3:] != ".h5"):
        opened_file = opened_file + ".h5"
    return open_file(opened_file, mode = open_mode, title = opened_file, filters=filters)

def init_groups(hdf5_file_path):
    """
    Initialisation of groups to delete historic tables
    """
    hdf5_file_obj = open_in_mode(hdf5_file_path, 'w')
    for group_name in ['variants', 'annotations', 'samples']:
        group = group_name+'_group'
        try:
            hdf5_file_obj.get_node("/", group)._f_remove()
        except:
            pass
        hdf5_file_obj.create_group("/", group, group, filters=filters)
    hdf5_file_obj.close()
    
    
def init_structure(hdf5_file, class_info):
    """
    Creates groups and and table in hdf5_file by structure in class_info.
    """
    if class_info['is_in_group']:
        try:
            #hdf5_file.root.class_info['group'].class_info['name']._f_remove()  is deprecated
            hdf5_file.remove_node("/"+class_info['group'], name=class_info['name'], recursive=True)
        except:
            pass
        try:
            group = hdf5_file.create_group("/", class_info['group'], class_info['group_title'], filters=filters)
        except:
            group = hdf5_file.get_node("/", class_info['group'])
        return hdf5_file.create_table(group, class_info['name'], class_info['class'], class_info['table_title'], filters=filters, expectedrows=expectedrows, chunkshape=chunkshape)
    else:
        try:
            #hdf5_file.root.table._f_remove() is deprecated
            hdf5_file.remove_node("/", name=class_info['name'], recursive=True)
        except:
            pass
        return hdf5_file.create_table("/", class_info['name'], class_info['class'], class_info['table_title'], filters=filters, expectedrows=expectedrows, chunkshape=chunkshape)


def get_table_from_file(searched_file, table_name):
    """
    Returns table ImportObject. 
    """
    if "variants" in table_name:
        return searched_file.get_node('/variants_group', table_name)
    if "annotations" in table_name:
        return searched_file.get_node('/annotations_group', table_name)
    if table_name == "calls":
        return searched_file.root.calls
    if "statistics" in table_name:
        return searched_file.get_node('/', table_name)
    else:
        group_name = table_name + "_group"
        return searched_file.get_node('/'+group_name, table_name)

#@profile_method
def insert_new_table_rows(ImportObject):
    """
    insert validated values of ImportObject to table_row of PyTables table of ImportObject
    """
    try:
        for table_column in ImportObject.attribute_list:
            insertion_value = getattr(ImportObject, table_column) # ImportObject.__dict__.get(table_column)
            #print(table_column, '"', insertion_value, '"')
            if insertion_value is not None: #otherwise 0 is interpreted as None
                ImportObject.table_row[table_column[1:]] = insertion_value
        ImportObject.table_row.append()
    except Exception as e:
        print("Import not successful: {0}".format(e))

        
def flush_tables(tables_dict):
    """flush table if indexed with index"""
    for table_ref in tables_dict:
        """if table_ref.cols.variant_id.is_indexed:
            table_ref.flush_rows_to_index()
        #elif table_ref.cols.variant_table_id.is_indexed: # TODO more generalize
            #table_ref.flush_rows_to_index()
        else:
            table_ref.flush()"""
        table_ref.flush()


def replace_true_false_to_int(variable_name):
    if (variable_name in {0, False}):
        return 0
    else:
        return 1

            
def replace_to_int(variable_name):
    """replacement to int for insertion check requires -999999999999999999"""
    try:
        return int(variable_name)
    except:
        return -999999999999999999
        
def replace_to_int32(variable_name):
    """replacement to int for insertion check requires -999999999"""
    try:
        return int(variable_name)
    except:
        return -999999999
            
def replace_to_float(variable_name):
    try:
        return float(variable_name)
    except:
        return None
        
def remove_brackets_quotations(reduce_string):
    reduce_string = str(reduce_string)
    reduce_string = reduce_string.strip("[]''") #possible ("[]''(){}<>")
    return reduce_string
    
    
