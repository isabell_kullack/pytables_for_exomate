from profiling import *
from tables import *
from importobjecthandler import *
from datastructures import filters
import time

__author__ = "Isabell Kullack"




def create_tables_index(hdf5_file, hdf5_file2 = None):
    if hdf5_file2 is None:
        hdf5_file2 = hdf5_file
    calls_table, variants_snp_table, variants_nosnp_table, samples_table, annotations_table, statistics_snp_table, statistics_nosnp_table, variant_snp_id, variant_nosnp_id, sample_id, annotation_id, call_id = get_import_objects_and_ids(hdf5_file, hdf5_file2)
    
    
    refresh_index({'column':annotations_table.cols.variant_id})
    #refresh_index({'column':annotations_table.cols.variant_table_id})
    refresh_index({'column':samples_table.cols.sample_id})
    refresh_index({'column':variants_snp_table.cols.variant_id})
    refresh_index({'column':variants_nosnp_table.cols.variant_id})
    #refresh_index({'column':calls_table.cols.sample_id})
    refresh_index({'column':calls_table.cols.variant_id})
    #refresh_index({'column':calls_table.cols.variant_table_id})
    refresh_index({'column':statistics_nosnp_table.cols.variant_id})
    refresh_index({'column':statistics_snp_table.cols.variant_id})
    
    
    
def refresh_index(table_ref):
                
    if not table_ref['column'].is_indexed:
        table_ref['column'].create_index(optlevel=optlevel, kind=kind, filters=filters, tmp_dir='/tmp', _testmode=False, _verbose=True) 
        #table_ref['column'].create_csindex(filters=filters)
        #table_ref['column'].create_csindex(optlevel=optlevel, kind=kind, filters=filters, tmp_dir='/tmp', _testmode=False, _verbose=True)
        
        #, tmp_dir=None, _blocksizes=None, _testmode=False, _verbose=False) (optlevel=optlevel, kind=kind, filters=filters, tmp_dir='/tmp', _testmode=False, _verbose=True) # auch angebbar
    #  _blocksizes = blocksizes, throws exception 
    else:
        table_ref['column'].reindex()
                
    
    #create_index()  / reindex() / reindex_dirty() / remove_index()



        
    

