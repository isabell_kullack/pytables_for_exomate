from tables import *
from filehandler import *
import sys
from profiling import *


__author__ = "Isabell Kullack"

class ImportStatistic():
    def __init__(self, hdf5_file, call_id, variant_id, variant_table_id, annotation_start_id, annotation_count, statistics_table):
        self.__hdf5_file = hdf5_file
        self._call_id = call_id
        self._variant_id = variant_id
        self._variant_table_id = variant_table_id
        self._annotation_start_id = annotation_start_id
        self._annotation_count = annotation_count
        
        
        self.attribute_list = ['_call_id', '_variant_id', '_variant_table_id', 
                               '_annotation_start_id', '_annotation_count']
        
        self.table = statistics_table
        self.statistics_table = self.table
        self.table_row = self.table.row
        self.statistics_row = self.table_row
        self.validate_insertions()
        self.is_new_entry = True
        #no insertion check needed 
        insert_new_table_rows(self)
        
    def validate_insertions(self):
        self._call_id = int(self._call_id)
        self._variant_id = int(self._variant_id)
        self._variant_table_id = int(self._variant_table_id)
        #self._annotation_start_id = int(self._annotation_start_id)
        self._annotation_count = int(self._annotation_count)
    
    def is_insertion_a_new_entry_in_table(self):
        pass
    
    
