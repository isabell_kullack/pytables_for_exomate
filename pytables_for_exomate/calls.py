from tables import *
from filehandler import *
from profiling import *
import argparse
import sys
import numpy

__author__ = "Isabell Kullack"

             
class Calls(IsDescription):
    sample_id = Int64Col(dflt=-999999999999999999, pos=0)
    variant_id = Int64Col(dflt=-999999999999999999, pos=1)
    variant_table_id = Int8Col(dflt=-99, pos=2)
    qual = Float64Col(dflt=numpy.nan, pos=3)
    is_heterozygous = UInt8Col(dflt=0, pos=4) 
    read_depth = Int64Col(dflt=-999999999999999999, pos=5)
    ref_depth = Int64Col(dflt=-999999999999999999, pos=6)
    alt_depth = Int64Col(dflt=-999999999999999999, pos=7) 
    strand_bias = Float64Col(dflt=numpy.nan, pos=8)
    qual_by_depth = Float64Col(dflt=numpy.nan, pos=9)
    mapping_qual = Float64Col(dflt=numpy.nan, pos=10)
    haplotype_score = Float64Col(dflt=numpy.nan, pos=11)
    mapping_qual_bias = Float64Col(dflt=numpy.nan, pos=12)
    read_pos_bias = Float64Col(dflt=numpy.nan, pos=13)
    call_id = Int64Col(dflt=-999999999999999999, pos=14)
    

calls_structure = {'name':'calls', 'class': Calls, 'table_title': 'calls table', 'is_in_group':False}
    
def main(hdf5_file_obj, init, import_file):
    if init:
        calls_table = init_structure(hdf5_file_obj, calls_structure)
    else:
        try:
            calls_table = get_table_from_file(hdf5_file_obj, calls_structure['name'])
        except NoSuchNodeError:
            calls_table = init_structure(hdf5_file_obj, calls_structure)
    if import_file:
        import_calls_from_csv(hdf5_file_obj, import_file, calls_table)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("--init", type=bool, default=False, choices=(True, False),
                        help="boolean to init calls table -- Caution ALL CALLS LOST! [default: False]")
    parser.add_argument("-i", "--importcallsfromcsv", type=str,
                        help="csv file name or path containing calls to be imported into calls table")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on calls tables operations enabled")
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    hdf5_file_obj = open_in_mode(args.hdf5file, 'a')
    sys.exit(main(hdf5_file_obj, args.init, args.importcallsfromcsv))   
    hdf5_file_obj.close()
    if(args.enableprofiling):
        disable_profiling(profiling_object)
