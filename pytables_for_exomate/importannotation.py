from tables import *
from filehandler import *
import sys
from profiling import *


__author__ = "Isabell Kullack"

class ImportAnnotation():
    def __init__(self, hdf5_file, variant_id, variant_table_id, transcript_id, allele, 
                 annotation, annotation2, annotation3, annotation4, annotation5, annotation6, 
                 annotation7, putative_impact, gene_name, gene_id, feature_type, feature_id, 
                 transcript_biotype, rank, total, hgvsc, hgvsp, cdna_position, cdna_len, 
                 cds_position, cds_len, protein_position, protein_len, distance_to_feature, info, 
                 annotations_table, annotation_id):
        self.__hdf5_file = hdf5_file
        self._annotation_id = annotation_id
        self._variant_id = variant_id
        self._variant_table_id = variant_table_id
        self._transcript_id = transcript_id
        self._allele = allele
        self._annotation = annotation
        self._annotation2 = annotation2
        self._annotation3 = annotation3
        self._annotation4 = annotation4
        self._annotation5 = annotation5
        self._annotation6 = annotation6
        self._annotation7 = annotation7
        self._putative_impact = putative_impact
        self._gene_name = gene_name
        self._gene_id = gene_id
        self._feature_type = feature_type
        self._feature_id = feature_id
        self._transcript_biotype = transcript_biotype
        self._rank = rank
        self._total = total
        self._hgvsc = hgvsc
        self._hgvsp = hgvsp
        self._cdna_position = cdna_position
        self._cdna_len = cdna_len
        self._cds_position = cds_position
        self._cds_len = cds_len
        self._protein_position = protein_position
        self._protein_len = protein_len
        self._distance_to_feature = distance_to_feature
        self._info = info
        
        self.attribute_list = ['_annotation_id', '_variant_id', '_variant_table_id', 
                               '_transcript_id', '_allele', '_annotation', '_annotation2', 
                               '_annotation3', '_annotation4', '_annotation5', '_annotation6', 
                               '_annotation7', '_putative_impact',
                               '_gene_name', '_gene_id', '_feature_type', '_feature_id', 
                               '_transcript_biotype', '_rank', '_total', '_hgvsc', '_hgvsp', 
                               '_cdna_position', '_cdna_len', '_cds_position', '_cds_len', 
                               '_protein_position', '_protein_len', '_distance_to_feature', 
                               '_info']
        
        self.table = annotations_table
        self.annotations_table = self.table
        self.table_row = self.table.row
        self.annotations_row = self.table_row
        self.validate_insertions()
        self.is_new_entry = True
        #no insertion check needed - annotations belong to variants and don't vary   
        insert_new_table_rows(self)
        
    def validate_insertions(self):
        self._annotation_id = replace_to_int(self._annotation_id)
        self._variant_id = replace_to_int(self._variant_id)
        self._variant_table_id = replace_to_int(self._variant_table_id)
        self._transcript_id = replace_to_int32(self._transcript_id)
        self._rank = replace_to_int(self._rank)
        self._total = replace_to_int(self._total)
        self._cdna_position = replace_to_int(self._cdna_position)
        self._cdna_len = replace_to_int(self._cdna_len)
        self._cds_position = replace_to_int(self._cds_position)
        self._cds_len = replace_to_int(self._cds_len)
        self._protein_position = replace_to_int(self._protein_position)
        self._protein_len = replace_to_int(self._protein_len)
        self._distance_to_feature = replace_to_int(self._distance_to_feature)
        #print('distance',self._distance_to_feature,'"')
    
    def is_insertion_a_new_entry_in_table(self):
        pass
    
    
        
def import_annotations_from_csv(hdf5_file, csv_file, annotations_table):
    """annotations from csv_file will be imported in a pytables hdf5_file."""
    annotation_id = len(annotations_table)
    is_new_entry = False
    data_check = False
    import csv
    csv.register_dialect('unixpwd', delimiter=';', quoting=csv.QUOTE_NONE)
    with open(csv_file, newline='', encoding='utf-8') as csvfile:
        csv_printer = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in csv_printer:
            try:
                #row[0] is id from export table
                import_annotation = Importannotation(hdf5_file, row[1], row[2], row[3], row[4], 
                                                     row[5], row[6], row[7], row[8], row[9], 
                                                     row[10], row[11], row[12], row[13], row[14],
                                                     row[15], row[16], row[17], row[18], row[19],
                                                     row[20], row[21], row[22], row[23], row[24],
                                                     row[25], row[26], row[27], row[28], 
                                                     annotations_table, annotation_id, 
                                                     data_check)
                if import_annotation.is_new_entry:
                    is_new_entry = True
                    annotation_id = annotation_id + 1
                    if (annotation_id == 100000):
                        annotations_table.flush()
                        annotation_id = len(annotations_table)
            except TypeError as e:
                sys.exit('file {}, line {}: TypeError: {}'.format(csv_file+'.csv', 
                                                                  csv_printer.line_num, e))
        if is_new_entry:
            annotations_table.flush()
