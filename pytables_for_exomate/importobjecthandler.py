from tables import *
from filehandler import *
from variants_nosnp import variantsNoSNP_structure
from variants_snp import variantsSNP_structure
from calls import calls_structure
from samples import samples_structure
from annotations import annotations_structure
from statistics import statistics_structure0, statistics_structure1
from datastructures import *
import time
import numpy as np

__author__ = "Isabell Kullack"

def get_data_insertion_checks(checkoff, preprocessingoff, alt_is_splitted, is_new_sample_import, 
                              is_variant_table):
    is_variant_check = False
    is_call_check = True
    
    if preprocessingoff and not checkoff:
        is_variant_check = True
    
    # variants check with preprocessing
    if alt_is_splitted and not checkoff:
        is_variant_check = True
    
    # calls check always if not a new sample
    if is_new_sample_import or checkoff:
        is_call_check = False            

    return is_variant_check, is_call_check


def get_maximum_table_id(table_obj, id_column):
    """
    prints the number of existing rows in table_name
    """
    max_id = 0
    len_table = len(table_obj)
    if len_table > 0:
        row = table_obj.row
        passvalues = [ row[id_column] for row in table_obj.iterrows() ]
        if passvalues:
            max_id = max(passvalues)
            max_id = max(max_id, len_table) # otherwise zero id entry creates zero instead of 1
    return max_id


def get_import_objects_and_ids(hdf5_file, hdf5_file2 = None):
    """Variants from vcf_file will be imported in a pytables hdf5_file."""
    variant_snp_id = -999999999999999999
    variant_nosnp_id = -999999999999999999
    sample_id = -999999999999999999
    annotation_id = -999999999999999999
    call_id = -999999999999999999
    
    if hdf5_file2 is None:
        hdf5_file2 = hdf5_file      
    
    try:
        calls_table = get_table_from_file(hdf5_file, "calls")
    except NoSuchNodeError:
        calls_table = init_structure(hdf5_file, calls_structure)
    # extension
    try:
        call_id = get_maximum_table_id(calls_table, 'call_id')
    except:
        pass
    
    try:
        statistics_snp_table = get_table_from_file(hdf5_file2, "statisticsSNP")
    except NoSuchNodeError:
        statistics_snp_table = init_structure(hdf5_file2, statistics_structure0) 
        
    try:
        statistics_nosnp_table = get_table_from_file(hdf5_file2, "statisticsNoSNP")
    except NoSuchNodeError:
        statistics_nosnp_table = init_structure(hdf5_file2, statistics_structure1)    
    
    try:
        variants_snp_table = get_table_from_file(hdf5_file, "variantsSNP")
        variant_snp_id = get_maximum_table_id(variants_snp_table, 'variant_id')
        #print(variant_snp_id, 'variant_snp_id')
    except NoSuchNodeError:
        variants_snp_table = init_structure(hdf5_file, variantsSNP_structure)
        variant_snp_id = 0
    
    try:
        variants_nosnp_table = get_table_from_file(hdf5_file, "variantsNoSNP")
        variant_nosnp_id = get_maximum_table_id(variants_nosnp_table, 'variant_id')
    except NoSuchNodeError:
        variants_nosnp_table = init_structure(hdf5_file, variantsNoSNP_structure)
        variant_nosnp_id = 0
    
    try:
        samples_table = get_table_from_file(hdf5_file, "samples")
        sample_id = get_maximum_table_id(samples_table, 'sample_id')
    except NoSuchNodeError:
        samples_table = init_structure(hdf5_file, samples_structure)
        sample_id = 0
    
    try:
        annotations_table = get_table_from_file(hdf5_file, "annotations")
        annotation_id = get_maximum_table_id(annotations_table, 'annotation_id')
    except NoSuchNodeError:
        annotations_table = init_structure(hdf5_file, annotations_structure)
        annotation_id = 0
    
    return calls_table, variants_snp_table, variants_nosnp_table, samples_table, annotations_table, statistics_snp_table, statistics_nosnp_table, variant_snp_id, variant_nosnp_id, sample_id, annotation_id, call_id
