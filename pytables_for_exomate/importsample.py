from tables import *
from filehandler import *
import sys
__author__ = "Isabell Kullack"

class ImportSample():
    def __init__(self, hdf5_file, accession, patient_id, tissue, disease_id, samples_table, 
                 sample_id):
        self.__hdf5_file = hdf5_file
        self.__accession = accession
        self.__patient_id = patient_id
        self.__tissue = tissue
        self.__disease_id = disease_id
        self.validate_insertions()
        self.sample_id = sample_id
        self.samples_table = samples_table
        self.samples_row = self.samples_table.row
        self.is_new_entry = self.is_insertion_a_new_entry_in_table()
        if self.is_new_entry:
            self.insert_new_sample()
        else:
            pass

    def validate_insertions(self):
        self.__accession = str(self.__accession)
        self.__patient_id = replace_to_int(self.__patient_id)
        self.__tissue = str(self.__tissue)
        self.__disease_id = replace_to_int(self.__disease_id)
        
    
     
    def is_insertion_a_new_entry_in_table(self):
        """checks if new sample is in sample table and sets sample_id in case"""
        query_condition = "(accession == {0}) & (patient_id == {1}) & (tissue == {2}) & (disease_id == {3})".format(self.__accession.encode(), self.__patient_id, self.__tissue.encode(), self.__disease_id)
        passvalues = [self.samples_row['sample_id'] for self.samples_row in self.samples_table.where(query_condition)]
        if not passvalues:
            return True
        else:
            self.sample_id = passvalues[0]
            return False

    def insert_new_sample(self):
        """Insertion in pytables file"""
        self.samples_row['sample_id'] = self.sample_id
        self.samples_row['accession'] = self.__accession
        if self.__patient_id is not None:
            self.samples_row['patient_id'] = self.__patient_id
        self.samples_row['tissue'] = self.__tissue
        if self.__disease_id is not None:
            self.samples_row['disease_id'] = self.__disease_id
        # Insert a new samples record
        self.samples_row.append()
        
        
        
def import_samples_from_csv(hdf5_file, csv_file, samples_table):
    """samples from csv_file will be imported in a pytables hdf5_file."""
    import csv
    csv.register_dialect('unixpwd', delimiter=';', quoting=csv.QUOTE_NONE)
    with open(csv_file, newline='', encoding='utf-8') as csvfile:
        csv_printer = csv.reader(csvfile, delimiter=';', quotechar='"')
        sample_id = len(samples_table)
        is_new_entry = False
        for row in csv_printer:
            try:
                #row[0] is its id from export table
                import_sample = ImportSample(hdf5_file, row[1], row[2], row[3], row[4], 
                                             samples_table, sample_id)
                if import_sample.is_new_entry:
                    is_new_entry = True
                    sample_id = sample_id + 1
            except TypeError as e:
                sys.exit('file {}, line {}: TypeError: {}'.format(csv_file+'.csv',
                         csv_printer.line_num, e))
        if is_new_entry:
            samples_table.flush()
        


