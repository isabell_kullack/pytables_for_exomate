from tables import *
from filehandler import *
from importstatistic import *
from profiling import *
import argparse
import sys
import numpy

__author__ = "Isabell Kullack"

# variant_table_id 0: SNP
# variant_table_id 1: NOSNP

class Statistics(IsDescription):
    call_id = Int64Col(dflt=-999999999999999999, pos=1)
    variant_id = Int64Col(dflt=-999999999999999999, pos=2)
    variant_table_id = Int8Col(dflt=-99, pos=3) # might be useful for call verification 
    annotation_start_id = Int64Col(dflt=-999999999999999999, pos=4)
    annotation_count = Int8Col(dflt=-99, pos=5)
    
statistics_structure0 = {'name':'statisticsSNP', 'class': Statistics, 'table_title': 'statistics SNP table', 'is_in_group':False}

  
statistics_structure1 = {'name':'statisticsNoSNP', 'class': Statistics, 'table_title': 'statistics NoSNP table', 'is_in_group':False}

    
def main(hdf5_file_obj, init, is_snp):
    if is_snp:
        statistics_structure = statistics_structure0
    else:
        statistics_structure = statistics_structure1
    if init:
        statistics_table = init_structure(hdf5_file_obj, statistics_structure)
    else:
        try:
            statistics_table = get_table_from_file(hdf5_file_obj, statistics_structure['name'])
        except NoSuchNodeError:
            statistics_table = init_structure(hdf5_file_obj, statistics_structure)
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("--init", type=bool, default=False, choices=(True, False),
                        help="boolean to init statistics table -- Caution ALL statistics LOST! [default: False]")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on statistics tables operations enabled")
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    hdf5_file_obj = open_in_mode(args.hdf5file, 'a')
    sys.exit(main(hdf5_file_obj, args.init, True), main(hdf5_file_obj, args.init, False))
    hdf5_file_obj.close()
    if(args.enableprofiling):
        disable_profiling(profiling_object)
