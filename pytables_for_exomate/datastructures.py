import time
from tables import Filters

__author__ = "Isabell Kullack"


filters = Filters(complevel=0, complib='lzo', fletcher32=False) 
# -complib {zlib,lzo,bzip2,blosc,blosc:blosclz,blosc:lz4,blosc:lz4hc,blosc:snappy,blosc:zlib}

expectedrows = 10000000 # 0 is standard
optlevel = 1
kind='full'
blocksizes = (500000,500000,500000,500000)
chunkshape = 100000 # TODO or None?
    
variant_table = dict( {
    'variant_snp_id' : 0, 
    'variant_nosnp_id' : 1}
    )
    
    
def main():
    pass

if __name__ == '__main__':
    main()
