from importannotation import *

__author__ = "Isabell Kullack"


def processing_annotation_extraction(annotations_all, hdf5_file, annotations_table, 
                                     pos, ref, alt, temp_variant_id, variant_table_id, 
                                     annotation_id):
    """extract and insert annotations of VCF-like extracted annotations"""
    print(annotations_all)
    print(len(annotations_all))
    for counter, annotation_single in enumerate(annotations_all):
        
        ann = annotation_single.split("|")
        
        transcript_id = None
        allele = None
        annotation = None
        annotation2 = None
        annotation3 = None
        annotation4 = None
        annotation5 = None
        annotation6 = None
        annotation7 = None
        putative_impact = None
        gene_name = None
        gene_id = None
        feature_id = None
        feature_type = None 
        transcript_biotype = None
        rank = None
        total = None
        cdna_position = None
        cdna_len = None
        cds_position = None
        cds_len = None
        protein_position = None
        protein_len = None
        distance_to_feature = None
        info = None
        
        allele = ann[0]
        if '-' in allele:
            splitted_allele = allele.split('-')
            if alt not in splitted_allele:
                continue 
            else:
                allele = alt
        else:
            if allele != alt:
                continue   
        annotation = ann[1]
        annotate = {0: '', 1: '', 2: '', 3: '', 4: '', 5: '', 6: ''}
        annotate[0] = annotation
        if '&' in annotation:
            annotation_list = []
            annotation_list = annotation.split('&')
            count = 0
            for anno in annotation_list:
                if count < 7:
                    annotate[count] = anno
                    #print(annotate)
                    #print(annotate[0])
                else:
                    print('one more annotation!')
                    print(ann[1])
                count = count + 1
            
        putative_impact = ann[2]
        gene_name = ann[3]
        if len(gene_name) > 40:
            print("Gene_name is longer",len(gene_name))
        gene_id = ann[4]
        if len(gene_id) > 40:
            print("Gene_id is longer",len(gene_id))
        feature_type = ann[5]
        if len(feature_type) > 24:
            print("feature_type is longer",len(feature_id))
        feature_id = ann[6]
        if len(feature_id) > 40:
            print("feature_id is longer",len(feature_id))
        transcript_biotype = ann[7]
        if ann[8]:
            rank, total = ann[8].split("/")
        hgvsc = ann[9]
        hgvsp = ann[10]
        if ann[11]:
            cdna_position, cdna_len = ann[11].split("/")
        if ann[12]:
            cds_position, cds_len = ann[12].split("/")
        if ann[13]:
            protein_position, protein_len = ann[13].split("/")
        distance_to_feature = ann[14]
        if ann[15]:
            info = ann[15]

        #print(ann)
        """print('alt ', alt)
        print('Allele: {0}'.format(ann[0]))
        print('Annotation: {0}'.format(ann[1]))
        print('putative_Impact: {0}'.format(ann[2]))
        print('Gene_Name: {0}'.format(ann[3]))
        print('Gene_ID: {0}'.format(ann[4]))
        print('Feature_Type: {0}'.format(ann[5]))
        print('Feature_id: {0}'.format(ann[6]))
        print('Transcript_BioType: {0}'.format(ann[7]))
        print('rank / total: {0}'.format(ann[8]))
        print('HGVS.c: {0}'.format(ann[9]))
        print('HGVS.p: {0}'.format(ann[10]))
        print('cDNA.pos / cDNA.length: {0}'.format(ann[11]))
        print('CDS.pos / CDS.length: {0}'.format(ann[12]))
        print('protein position / protein length: {0}'.format(ann[13]))
        print('Distance: {0}'.format(ann[14]))
        print('ERRORS / WARNINGS / INFO: {0}'.format(ann[15]))"""
    
        ImportAnnotation(hdf5_file, temp_variant_id, variant_table_id, transcript_id, 
                            allele, annotate[0], annotate[1], annotate[2], annotate[3], 
                            annotate[4], annotate[5], annotate[6], putative_impact, gene_name,
                            gene_id, feature_type, feature_id, transcript_biotype, rank, 
                            total, hgvsc, hgvsp, cdna_position, cdna_len, cds_position, 
                            cds_len, protein_position, protein_len, distance_to_feature, info, 
                            annotations_table, annotation_id)
        annotation_id = annotation_id + 1
        
    
    return annotation_id, counter+1
