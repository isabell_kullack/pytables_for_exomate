'''
All common enums for exomate
'''

__author__ = "Marcel Martin"

def enum(**enums):
	return type('Enum', (), enums)


VARIANT = enum(
	THREE_PRIME_UTR = 0,
	FIVE_PRIME_UTR = 1,
	CODING_SEQUENCE = 2,
	DOWNSTREAM_GENE = 3,
	FEATURE_ELONGATION = 4,
	FEATURE_TRUNCATION = 5,
	FRAMESHIFT = 6,
	INCOMPLETE_TERMINAL_CODON = 7,
	INFRAME_DELETION = 8,
	INFRAME_INSERTION = 9,
	INITIATOR_CODON = 10,
	INTERGENIC = 11,
	INTRON = 12,
	MATURE_MIRNA = 13,
	MISSENSE = 14,
	NC_TRANSCRIPT = 15,
	NMD_TRANSCRIPT = 16,
	NON_CODING_EXON = 17,
	REGULATORY_REGION = 18,
	SPLICE_ACCEPTOR = 19,
	SPLICE_DONOR = 20,
	SPLICE_REGION = 21,
	STOP_GAINED = 22,
	STOP_LOST = 23,
	STOP_RETAINED = 24,
	SYNONYMOUS = 25,
	TF_BINDING_SITE = 26,
	TFBS_ABLATION = 27,
	UPSTREAM_GENE = 28
)

VARIANT_OUT_NAMES = {
	VARIANT.THREE_PRIME_UTR : "3'UTR",
	VARIANT.FIVE_PRIME_UTR : "5'UTR",
	VARIANT.CODING_SEQUENCE : "Coding",
	VARIANT.DOWNSTREAM_GENE : "Down",
	VARIANT.FEATURE_ELONGATION : "Elong",
	VARIANT.FEATURE_TRUNCATION : "Trunc",
	VARIANT.FRAMESHIFT : "F-Shift",
	VARIANT.INCOMPLETE_TERMINAL_CODON : "Incomp. Terminal",
	VARIANT.INFRAME_DELETION : "Del",
	VARIANT.INFRAME_INSERTION : "Ins",
	VARIANT.INITIATOR_CODON : "Init",
	VARIANT.INTERGENIC : "Inter",
	VARIANT.INTRON : "Intron",
	VARIANT.MATURE_MIRNA : "miRNA",
	VARIANT.MISSENSE : "Miss",
	VARIANT.NC_TRANSCRIPT : "NC-T",
	VARIANT.NMD_TRANSCRIPT : "NMD",
	VARIANT.NON_CODING_EXON : "NC-E",
	VARIANT.REGULATORY_REGION : "Reg",
	VARIANT.SPLICE_ACCEPTOR : "Acc",
	VARIANT.SPLICE_DONOR : "Donor",
	VARIANT.SPLICE_REGION : "Splice Region",
	VARIANT.STOP_GAINED : "Stop Gain",
	VARIANT.STOP_LOST : "Stop Lost",
	VARIANT.STOP_RETAINED : "Stop Ret",
	VARIANT.SYNONYMOUS : "Syn",
	VARIANT.TF_BINDING_SITE : "TFBS",
	VARIANT.TFBS_ABLATION : "TFBS-Abl",
	VARIANT.UPSTREAM_GENE : "Up"
}

#VARIANT_OUT_NAMES = {
#	VARIANT.THREE_PRIME_UTR : "3 Prime UTR",
#	VARIANT.FIVE_PRIME_UTR : "5 Prime UTR",
#	VARIANT.CODING_SEQUENCE : "Coding Sequence",
#	VARIANT.DOWNSTREAM_GENE : "Downstream Gene",
#	VARIANT.FEATURE_ELONGATION : "Feature Elongation",
#	VARIANT.FEATURE_TRUNCATION : "Feature Truncation",
#	VARIANT.FRAMESHIFT : "Frameshift",
#	VARIANT.INCOMPLETE_TERMINAL_CODON : "Incomplete Terminal Codon",
#	VARIANT.INFRAME_DELETION : "Inframe Deletion",
#	VARIANT.INFRAME_INSERTION : "Inframe Insertion",
#	VARIANT.INITIATOR_CODON : "Initiator Codon",
#	VARIANT.INTERGENIC : "Intergenic",
#	VARIANT.INTRON : "Intron",
#	VARIANT.MATURE_MIRNA : "Mature miRNA",
#	VARIANT.MISSENSE : "Missense",
#	VARIANT.NC_TRANSCRIPT : "NC Transcript",
#	VARIANT.NMD_TRANSCRIPT : "NMD Transcript",
#	VARIANT.NON_CODING_EXON : "Non Coding Exon",
#	VARIANT.REGULATORY_REGION : "Regulatory Region",
#	VARIANT.SPLICE_ACCEPTOR : "Splice Acceptor",
#	VARIANT.SPLICE_DONOR : "Splice Donor",
#	VARIANT.SPLICE_REGION : "Splice Region",
#	VARIANT.STOP_GAINED : "Stop Gained",
#	VARIANT.STOP_LOST : "Stop Lost",
#	VARIANT.STOP_RETAINED : "Stop Retained",
#	VARIANT.SYNONYMOUS : "Synonymous",
#	VARIANT.TF_BINDING_SITE : "TF Binding Site",
#	VARIANT.TFBS_ABLATION : "TF Binding Site Ablation",
#	VARIANT.UPSTREAM_GENE : "Upstream Gene"
#}


EXON_VARIANTS = [
	VARIANT.CODING_SEQUENCE,
	VARIANT.FRAMESHIFT,
	VARIANT.INCOMPLETE_TERMINAL_CODON,
	VARIANT.INFRAME_DELETION,
	VARIANT.INFRAME_INSERTION,
	VARIANT.INITIATOR_CODON,
	VARIANT.MISSENSE,
	VARIANT.SPLICE_ACCEPTOR,
	VARIANT.SPLICE_DONOR,
	VARIANT.SPLICE_REGION,
	VARIANT.STOP_GAINED,
	VARIANT.STOP_LOST,
	VARIANT.STOP_RETAINED,
	VARIANT.SYNONYMOUS
]

AG = ord('A') + ord('G')
CT = ord('C') + ord('T')


def is_indel(ref, alt):
	return not (len(ref) == len(alt) == 1)


def transition_transversion(ref, alt):
	if not (len(ref) == len(alt) == 1):
		return False, False
	s = ord(ref) + ord(alt)
	transition = s == AG or s == CT
	return transition, not transition


def normalized_indel(chromosome, pos, ref, alt):
	"""
	Normalize an indel by moving it to the left as far as possible.

	chromosome -- the chromosome sequence
	pos -- position of the indel on the chromosome (0-based coordinate)
	ref -- REF field from VCF
	alt -- ALT field from VCF
	"""
	assert chromosome[pos:pos+len(ref)] == ref
	assert (len(ref) == len(alt) and ref != alt) or \
			(len(ref) != len(alt) and ref[0] == alt[0])
	if len(ref) == len(alt) == 1:
		# no change necessary for SNP
		return (pos, ref, alt)
	if len(ref) == len(alt):
		while len(ref) > 1 and ref[0] == alt[0]:
			ref = ref[1:]
			alt = alt[1:]
			pos += 1
		return (pos, ref, alt)
	assert ref[0] == alt[0]
	assert len(ref) > 0 and len(alt) > 0

	while pos > 1 and ref[-1] == alt[-1]:
		# If the last characters are the same,
		# the indel can be moved one position to the left.
		pos -= 1
		ref = chromosome[pos:pos+1] + ref[:-1]
		alt = chromosome[pos:pos+1] + alt[:-1]

	while len(ref) > 1 and len(alt) > 1 and ref[0] == alt[0] and ref[1] == alt[1]:
		# when the first two characters are the same,
		# the first one can be removed.
		ref = ref[1:]
		alt = alt[1:]
		pos += 1

	assert (len(ref) == len(alt) and ref != alt) or \
			(len(ref) != len(alt) and ref[0] == alt[0])
	assert chromosome[pos:pos+1] == ref[0:1], "chromosome: %s pos: %d (%s != %s)" % (chromosome, pos, chromosome[pos:pos+1], ref[0:1])
	return (pos, ref, alt)
