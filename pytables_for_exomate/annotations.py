from tables import *
from filehandler import *
from importannotation import *
from profiling import *
import argparse
import sys
import numpy

__author__ = "Isabell Kullack"

# variant_table_id 0: SNP
# variant_table_id 1: NOSNP

class Annotations(IsDescription):
    annotation_id = Int64Col(dflt=-999999999999999999, pos=0)
    variant_id = Int64Col(dflt=-999999999999999999, pos=1)
    variant_table_id = Int8Col(dflt=-99, pos=2)
    transcript_id = Int32Col(dflt=-999999999, pos=3)
    allele = StringCol(30, dflt='', pos=4)
    annotation = StringCol(180, dflt='', pos=5)
    annotation2 = StringCol(25, dflt='', pos=6)
    annotation3 = StringCol(25, dflt='', pos=7)
    annotation4 = StringCol(25, dflt='', pos=8)
    annotation5 = StringCol(25, dflt='', pos=9)
    annotation6 = StringCol(25, dflt='', pos=10)
    annotation7 = StringCol(25, dflt='', pos=11)
    putative_impact = StringCol(8, dflt='', pos=12) # {HIGH, MODERATE, LOW, MODIFIER} 
    gene_name = StringCol(40, dflt='', pos=13) # 40 enough? TODO 
    gene_id = StringCol(40, dflt='', pos=14) # 40 enough? TODO
    feature_type = StringCol(24, dflt='', pos=15)
    feature_id = StringCol(40, dflt='', pos=16) # 40 enough? TODO
    transcript_biotype = StringCol(10, dflt='', pos=17)
    rank = Int64Col(dflt=-999999999999999999, pos=18)
    total = Int64Col(dflt=-999999999999999999, pos=19)
    hgvsc = StringCol(30, dflt='', pos=20)
    hgvsp = StringCol(30, dflt='', pos=21)
    cdna_position = Int64Col(dflt=-999999999999999999, pos=22)
    cdna_len = Int64Col(dflt=-999999999999999999, pos=23)
    cds_position = Int64Col(dflt=-999999999999999999, pos=24)
    cds_len = Int64Col(dflt=-999999999999999999, pos=24)
    protein_position = Int64Col(dflt=-999999999999999999, pos=25)
    protein_len = Int64Col(dflt=-999999999999999999, pos=26)
    distance_to_feature = Int64Col(dflt=-999999999999999999, pos=27)
    info = StringCol(30, dflt='', pos=28)
    
annotations_structure = {'name':'annotations', 'class': Annotations, 'group':'annotations_group', 'group_title':'annotations group', 'table_title': 'annotations table', 'is_in_group':True}

    
def main(hdf5_file_obj, init, import_file):
    
    if init:
        annotations_table = init_structure(hdf5_file_obj, annotations_structure)
    else:
        try:
            annotations_table = get_table_from_file(hdf5_file_obj, annotations_structure['name'])
        except NoSuchNodeError:
            annotations_table = init_structure(hdf5_file_obj, annotations_structure)
    
    if import_file:
        import_annotations_from_csv(hdf5_file_obj, import_file, annotations_table)   
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("--init", type=bool, default=False, choices=(True, False),
                        help="boolean to init annotations table -- Caution ALL annotations LOST! [default: False]")
    parser.add_argument("-i", "--importannotationsfromcsv", type=str,
                        help="csv file name or path containing annotations to be imported into annotations table")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on annotations tables operations enabled")
    args = parser.parse_args()
    
    if(args.enableprofiling):
        profiling_object = enable_profiling()
    hdf5_file_obj = open_in_mode(args.hdf5file, 'a')
    sys.exit(main(hdf5_file_obj, args.init, args.importannotationsfromcsv))
    hdf5_file_obj.close()
    if(args.enableprofiling):
        disable_profiling(profiling_object)
