from operator import itemgetter, attrgetter, methodcaller
from datastructures import *
from profiling import *
import io

__author__ = "Isabell Kullack"


#@profile_method
def sort_vcf(vcf_file):
    """sorts given vcf file to a new vcf file 2"""
    
    vcf_obj = open(vcf_file, 'r')
    
    vcf_file_new = vcf_file[:-4] + '_temp.vcf'
    vcf_obj_new = open(vcf_file_new, 'w')
    
    vcf_lines = list()
    samplename = ""
    
    for line in vcf_obj:
        if(line[0] == '#'):
            if(line[1] == '#'):
                print(line, file=vcf_obj_new, end='')
            else:
                x, name = line.rsplit('\t',1)
                samplename = name[:-1]
                print(line, file=vcf_obj_new, end='')
        else:       
            chrom, pos, vcf_id, ref, alt, qual, filter, info, format, sampledata = line.split('\t')#, maxsplit = 5)
            if len(chrom) == 1:
                if chrom != 'X' and chrom != 'Y':
                    chrom = '0' + chrom
            
            if "," in alt:
                alt_array = alt.split(",")
                keys = format.split(":")
                sdata = sampledata.split(":")
                #print(keys)
                #print(sdata)
                format_position = [i for i,x in enumerate(keys) if x == 'AD'][0]
                AD = sdata[format_position]
                #print(AD)
                counter = 1
                for alt in alt_array:
                    AD_splitted = AD.split(",")
                    #print(AD_splitted)
                    AD1 = AD_splitted[0]+','+AD_splitted[counter]
                    counter += 1
                    #print(AD1)
                    #print(sampledata)
                    #sampledata0 = sampledata.replace(AD, AD0, 1)
                    #print(sampledata0)
                    sampledata0 = sampledata.replace(AD, AD1, 1)
                    #print(sampledata0)
                    vcf_lines.append( (chrom, pos, vcf_id, ref, alt, qual, filter, info, format, sampledata0) )
                #second entry will be inserted automatically"""
            else:
                vcf_lines.append( (chrom, pos, vcf_id, ref, alt, qual, filter, info, format, sampledata) )
            
            
            
    # sorts chrom, pos, ref, alt - even though lasts are not splitted
    vcf_lines.sort(key=itemgetter(0,1,3,4,5)) 
    
    
    
    for (chrom, pos, vcf_id, ref, alt, qual, filter, info, format, sampledata) in vcf_lines: 
        print(chrom, pos, vcf_id, ref, alt, qual, filter, info, format, sampledata, sep='\t', file=vcf_obj_new, end='')
    
    vcf_obj.close()
    vcf_obj_new.close()
        
    return vcf_file_new, samplename


def main():
    vcf_file = "test_vcf_sort_alt2.vcf"
    vcf_file_new = sort_vcf(vcf_file)
        
        

if __name__ == '__main__':
    main()
    

