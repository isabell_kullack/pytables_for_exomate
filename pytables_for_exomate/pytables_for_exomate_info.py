from variants_snp import main as variantsmain
from variants_nosnp import main as variantsnosnpmain
from samples import main as samplesmain
from calls import main as callsmain
from annotations import main as annotationsmain
from import_vcf_info import main as vcfmain
from filehandler import init_groups, open_in_mode
from indexhandler import *
from profiling import *
import argparse
import sys
import os
#from interface import get_number_of_rows


__author__ = "Isabell Kullack"


@profile_method
def main(hdf5_file_path, init_all, init_calls, import_calls, init_variants, import_variants, 
         init_variants_nosnp, import_variants_nosnp, init_samples, import_samples, 
         init_annotations, import_annotations, vcf_file, defined_filter, start, limit, checkoff, 
         preprocessingoff, skip_known_vcf = False, enable_indexing = False):
    # manuel or auto init of tables
    if init_all or not os.path.isfile(hdf5_file_path):
        init_groups(hdf5_file_path)
        init_calls, init_variants, init_variants_nosnp, init_samples, init_annotations = True, True, True, True, True
    hdf5_file = open_in_mode(hdf5_file_path, 'a')
    variantsmain(hdf5_file, init_variants, import_variants)
    variantsnosnpmain(hdf5_file, init_variants_nosnp, import_variants_nosnp)
    samplesmain(hdf5_file, init_samples, import_samples)
    callsmain(hdf5_file, init_calls, import_calls)
    annotationsmain(hdf5_file, init_annotations, import_annotations)
    #hdf5_file.close()
    # init index if required
#    if os.path.isfile(hdf5_file_path) and enable_indexing:
#        print("Creating Index.")
#        create_tables_index(hdf5_file)
    hdf5_file.close()
    if vcf_file:
        vcf_list = []
        vcf_file_new = None
        if isinstance(vcf_file, str):
            vcf_list.append(vcf_file)
        else:
            vcf_list = vcf_file
        if vcf_file:
            previous_rows = 0
            for vcf in vcf_list:
                if 'temp' in vcf:
                    continue
                print(vcf)
		# preprocessing requires closed file for consistency
                is_import_unsuccessful, previous_rows  = vcfmain(hdf5_file_path, vcf, defined_filter, start, limit, checkoff, preprocessingoff, skip_known_vcf)
                #previous_rows = get_number_of_rows(hdf5_file, previous_rows)
                if is_import_unsuccessful:
                    print("This VCF file was not correctly imported: {0}".format(vcf))
                    continue
                else:
                    if enable_indexing:
                        print("Creating Index.")
                        hdf5_file = open_in_mode(hdf5_file_path, 'a')                      
                        create_tables_index(hdf5_file)
                        hdf5_file.close()
        else:
            print("Unsupported path.")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("hdf5file", help="hdf5 file to be used")
    parser.add_argument("--initall", type=bool, default=False, choices=(True, False),
                        help="boolean to init all tables -- Caution ALL DATA LOST! [default: False]")
    parser.add_argument("--initcalls", type=bool, default=False, choices=(True, False),
                        help="boolean to init calls table -- Caution ALL CALLS LOST! [default: False]")
    parser.add_argument("--initsamples", type=bool, default=False, choices=(True, False),
                        help="boolean to init samples table -- Caution ALL SAMPLES LOST! [default: False]")
    parser.add_argument("--initvariants", type=bool, default=False, choices=(True, False),
                        help="boolean to init variants table -- Caution ALL VARIANTS LOST! [default: False]")
    parser.add_argument("--initvariantsnosnp", type=bool, default=False, choices=(True, False),
                        help="boolean to init variants_nosnp table -- Caution ALL VARIANT nosnps LOST! [default: False]")
    parser.add_argument("--initannotations", type=bool, default=False, choices=(True, False),
                        help="boolean to init annotations table -- Caution ALL ANNOTATIONS LOST! [default: False]")
    parser.add_argument("--importcallsfromcsv", type=str,
                        help="csv file name or path containing calls to be imported into calls table")
    parser.add_argument("--importvariantsfromcsv", type=str,
                        help="csv file name or path containing variants to be imported into variants table")
    parser.add_argument("--importvariantsnosnpfromcsv", type=str,
                        help="csv file name or path containing variants_nosnp to be imported into variants_nosnp table")
    parser.add_argument("--importsamplesfromcsv", type=str,
                        help="csv file name or path containing samples to be imported into samples table")
    parser.add_argument("--importannotationsfromcsv", type=str,
                        help="csv file name or path containing annotations to be imported into annotations table")
    parser.add_argument("-vcf", nargs='+', help="vcf file to be imported")
    parser.add_argument("-f", "--filterminus", type=str, default='', choices=('', 'q10', 's50'),
                        help="vcf filter left out given insert [Example: q10]")
    parser.add_argument("-s", "--start", type=int,
                        help="vcf start of row number to be imported -- first row number is 0")
    parser.add_argument("-l", "--limit", type=int,
                        help="vcf including limit of row number up to be imported")
    parser.add_argument("-c", "--checkoff", action="store_true", 
                        help="disables checking if variants and calls and samples in tables")
    parser.add_argument("--skipknownvcf", type=bool, default=False, choices=(True, False),
                        help="skips vcf import if sample is found - no calls check or missing variant import! [default: False]")
    parser.add_argument("-i", "--enableindexing",  action="store_true",
                        help="enables try to reindex")
    parser.add_argument("-pre", "--preprocessingoff", action="store_true", 
                        help="disables preprocessed checking if variants in tables")
    parser.add_argument("-p", "--enableprofiling", action="store_true", 
                        help="profiling on all operations enabled")
    args = parser.parse_args()
    
    if args.enableprofiling:
        profiling_object = enable_profiling()
    sys.exit(main(args.hdf5file, args.initall, args.initcalls, args.importcallsfromcsv, 
                  args.initvariants, args.importvariantsfromcsv, args.initvariantsnosnp, 
                  args.importvariantsnosnpfromcsv, args.initsamples, args.importsamplesfromcsv, 
                  args.initannotations, args.importannotationsfromcsv, args.vcf, args.filterminus, 
                  args.start, args.limit, args.checkoff, args.preprocessingoff, args.skipknownvcf,
                  args.enableindexing))   
    if args.enableprofiling:
        disable_profiling(profiling_object)
