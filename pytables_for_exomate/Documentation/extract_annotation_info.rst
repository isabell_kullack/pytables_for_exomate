extract_annotation_info module
====================================

.. automodule:: extract_annotation_info
    :members:
    :undoc-members:
    :show-inheritance:
