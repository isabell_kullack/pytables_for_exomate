extract_annotation module
====================================

.. automodule:: extract_annotation
    :members:
    :undoc-members:
    :show-inheritance:
