.. . documentation master file, created by
   sphinx-quickstart on Wed Oct 28 14:38:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyTables for Exomate's documentation!
================================================

Contents:

.. toctree::
   :maxdepth: 4

   annotations
   calls
   importannotation
   importcall
   importsample
   import_vcf
   import_vcf_info
   importvariant
   importstatistic
   samples
   variants_noosnp
   variants_snp
   fileHandler
   importObjectHandler
   indexHandler
   preprocessingImportVCF
   processingAnnotationImportVCF
   profiling
   pytables_for_exomate
   pytables_for_exomate_info
   sort_vcf
   datastructures
   variants_utilities


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

